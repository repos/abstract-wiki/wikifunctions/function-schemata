test_information:
  name: canonicalize ZObject

test_objects:

  success:
    - name: regular quote
      object:
        Z1K1:
          Z1K1: Z9
          Z9K1: Z99
        Z99K1:
          Z1K1: Z6
          Z6K1: a string
      expected:
        Z1K1: Z99
        Z99K1:
          Z1K1: Z6
          Z6K1: a string

    - name: busted quote
      object:
        Z1K1:
          Z1K1: Z9
          Z9K1: Z99
        Z99K1:
          terriblekey: 4
      expected:
        Z1K1: Z99
        Z99K1:
          terriblekey: 4

    - name: not all Z7s are lists
      object:
        Z1K1: Z7
        Z7K1: Z10007
        K1:
          Z1K1: Z40
          Z40K1: Z41
        K2:
          Z1K1: Z18
          Z18K1:
            Z1K1: Z6
            Z6K1: Z31002K1
      expected:
        Z1K1: Z7
        Z7K1: Z10007
        K1:
          Z1K1: Z40
          Z40K1: Z41
        K2:
          Z1K1: Z18
          Z18K1: Z31002K1

    - name: string unordered
      object:
        Z1K1: Z6
        Z6K1: ba
      expected: ba

    - name: untrimmed string left
      object:
        Z1K1: Z6
        Z6K1: " a"
      expected: " a"

    - name: untrimmed string right
      object:
        Z1K1: Z6
        Z6K1: "a "
      expected: "a "

    - name: untrimmed string left two
      object:
        Z1K1: Z6
        Z6K1: "  a"
      expected: "  a"

    - name: untrimmed string both
      object:
        Z1K1: Z6
        Z6K1: " a "
      expected: " a "

    - name: empty record with reference type
      object:
        Z1K1:
          Z1K1: Z9
          Z9K1: Z2
      expected:
        Z1K1: Z2

    - name: simple record
      object:
        Z1K1:
          Z1K1: Z9
          Z9K1: Z60
        Z60K1:
          Z1K1: Z6
          Z6K1: a
      expected:
        Z1K1: Z60
        Z60K1: a

    - name: escaped string
      object:
        Z1K1: Z6
        Z6K1: Z6
      expected:
        Z1K1: Z6
        Z6K1: Z6

    - name: escaped string QID
      object:
        Z1K1: Z6
        Z6K1: Q42
      expected: Q42

    - name: unnecessarily escaped string key
      object:
        Z1K1: Z6
        Z6K1: Z1K1
      expected: Z1K1

    - name: unnecessarily escaped string key for Z0 (invalid ZID)
      object:
        Z1K1: Z6
        Z6K1: Z0
      expected: Z0

    - name: unnecessarily escaped string key for K3 (local keys are not References)
      object:
        Z1K1: Z6
        Z6K1: K3
      expected: K3

    - name: unnecessarily escaped string key with whitespace
      object:
        Z1K1: Z6
        Z6K1: " Z1"
      expected: " Z1"

    - name: object with escaped string
      object:
        Z1K1:
          Z1K1: Z9
          Z9K1: Z2
        Z2K2:
          Z1K1: Z6
          Z6K1: Z6
      expected:
        Z1K1: Z2
        Z2K2:
          Z1K1: Z6
          Z6K1: Z6

    - name: object with unnecessarily escaped string
      object:
        Z1K1:
          Z1K1: Z9
          Z9K1: Z2
        Z2K2:
          Z1K1: Z6
          Z6K1: Z
      expected:
        Z1K1: Z2
        Z2K2: Z

    - name: explicit reference
      object:
        Z1K1:
          Z1K1: Z9
          Z9K1: Z2
        Z2K2:
          Z1K1: Z9
          Z9K1: Z6
      expected:
        Z1K1: Z2
        Z2K2: Z6

    - name: implicit reference
      object:
        Z1K1:
          Z1K1: Z9
          Z9K1: Z2
        Z2K2:
          Z1K1: Z9
          Z9K1: Z6
      expected:
        Z1K1: Z2
        Z2K2: Z6

    - name: canonicalize Z5 error wrapping Z6
      object:
        Z1K1:
          Z1K1: Z9
          Z9K1: Z5
        Z5K1:
          Z1K1: Z9
          Z9K1: Z501
        Z5K2:
          Z1K1: Z6
          Z6K1: a string error message
      expected:
        Z1K1: Z5
        Z5K1: Z501
        Z5K2: a string error message

    - name: canonicalize Z5 error wrapping Z5 error
      object:
        Z1K1:
          Z1K1: Z9
          Z9K1: Z5
        Z5K1:
          Z1K1: Z9
          Z9K1: Z501
        Z5K2:
          Z1K1:
            Z1K1: Z9
            Z9K1: Z5
          Z5K1:
            Z1K1: Z9
            Z9K1: Z502
          Z5K2:
            Z1K1: Z6
            Z6K1: second error
      expected:
        Z1K1: Z5
        Z5K1: Z501
        Z5K2:
          Z1K1: Z5
          Z5K1: Z502
          Z5K2: second error

    - name: argument declaration
      object:
        Z1K1:
          Z1K1: Z9
          Z9K1: Z18
        Z18K1:
          Z1K1: Z6
          Z6K1: Z10000K1
      expected:
        Z1K1: Z18
        Z18K1: Z10000K1

    # LISTS

    - name: empty typed list
      object:
        Z1K1:
          Z1K1:
            Z1K1: Z9
            Z9K1: Z7
          Z7K1:
            Z1K1: Z9
            Z9K1: Z881
          Z881K1:
            Z1K1: Z9
            Z9K1: Z6
      expected:
        - Z6

    - name: typed list with empty string
      object:
        Z1K1:
          Z1K1:
            Z1K1: Z9
            Z9K1: Z7
          Z7K1:
            Z1K1: Z9
            Z9K1: Z881
          Z881K1:
            Z1K1: Z9
            Z9K1: Z6
        K1:
          Z1K1: Z6
          Z6K1: ""
        K2:
          Z1K1:
            Z1K1:
              Z1K1: Z9
              Z9K1: Z7
            Z7K1:
              Z1K1: Z9
              Z9K1: Z881
            Z881K1:
              Z1K1: Z9
              Z9K1: Z6
      expected:
        - Z6
        - ""

    - name: typed list with two empty strings
      object:
        Z1K1:
          Z1K1:
            Z1K1: Z9
            Z9K1: Z7
          Z7K1:
            Z1K1: Z9
            Z9K1: Z881
          Z881K1:
            Z1K1: Z9
            Z9K1: Z6
        K1:
          Z1K1: Z6
          Z6K1: ""
        K2:
          Z1K1:
            Z1K1:
              Z1K1: Z9
              Z9K1: Z7
            Z7K1:
              Z1K1: Z9
              Z9K1: Z881
            Z881K1:
              Z1K1: Z9
              Z9K1: Z6
          K1:
            Z1K1: Z6
            Z6K1: ""
          K2:
            Z1K1:
              Z1K1:
                Z1K1: Z9
                Z9K1: Z7
              Z7K1:
                Z1K1: Z9
                Z9K1: Z881
              Z881K1:
                Z1K1: Z9
                Z9K1: Z6
      expected:
        - Z6
        - ""
        - ""

    - name: typed list with ordered strings
      object:
        Z1K1:
          Z1K1:
            Z1K1: Z9
            Z9K1: Z7
          Z7K1:
            Z1K1: Z9
            Z9K1: Z881
          Z881K1:
            Z1K1: Z9
            Z9K1: Z6
        K1:
          Z1K1: Z6
          Z6K1: a
        K2:
          Z1K1:
            Z1K1:
              Z1K1: Z9
              Z9K1: Z7
            Z7K1:
              Z1K1: Z9
              Z9K1: Z881
            Z881K1:
              Z1K1: Z9
              Z9K1: Z6
          K1:
            Z1K1: Z6
            Z6K1: b
          K2:
            Z1K1:
              Z1K1:
                Z1K1: Z9
                Z9K1: Z7
              Z7K1:
                Z1K1: Z9
                Z9K1: Z881
              Z881K1:
                Z1K1: Z9
                Z9K1: Z6
      expected:
        - Z6
        - a
        - b

    - name: typed lists with unordered strings
      object:
        Z1K1:
          Z1K1:
            Z1K1: Z9
            Z9K1: Z7
          Z7K1:
            Z1K1: Z9
            Z9K1: Z881
          Z881K1:
            Z1K1: Z9
            Z9K1: Z6
        K1:
          Z1K1: Z6
          Z6K1: b
        K2:
          Z1K1:
            Z1K1:
              Z1K1: Z9
              Z9K1: Z7
            Z7K1:
              Z1K1: Z9
              Z9K1: Z881
            Z881K1:
              Z1K1: Z9
              Z9K1: Z6
          K1:
            Z1K1: Z6
            Z6K1: a
          K2:
            Z1K1:
              Z1K1:
                Z1K1: Z9
                Z9K1: Z7
              Z7K1:
                Z1K1: Z9
                Z9K1: Z881
              Z881K1:
                Z1K1: Z9
                Z9K1: Z6
      expected:
        - Z6
        - b
        - a

    - name: typed list with escaped string
      object:
        Z1K1:
          Z1K1:
            Z1K1: Z9
            Z9K1: Z7
          Z7K1:
            Z1K1: Z9
            Z9K1: Z881
          Z881K1:
            Z1K1: Z9
            Z9K1: Z6
        K1:
          Z1K1: Z6
          Z6K1: Z6
        K2:
          Z1K1:
            Z1K1:
              Z1K1: Z9
              Z9K1: Z7
            Z7K1:
              Z1K1: Z9
              Z9K1: Z881
            Z881K1:
              Z1K1: Z9
              Z9K1: Z6
          K1:
            Z1K1: Z6
            Z6K1: Z
          K2:
            Z1K1:
              Z1K1:
                Z1K1: Z9
                Z9K1: Z7
              Z7K1:
                Z1K1: Z9
                Z9K1: Z881
              Z881K1:
                Z1K1: Z9
                Z9K1: Z6
      expected:
        - Z6
        - Z1K1: Z6
          Z6K1: Z6
        - Z

    - name: typed list with different typed objects
      object:
        Z1K1:
          Z1K1:
            Z1K1: Z9
            Z9K1: Z7
          Z7K1:
            Z1K1: Z9
            Z9K1: Z881
          Z881K1:
            Z1K1: Z9
            Z9K1: Z1
        K1:
          Z1K1: Z6
          Z6K1: first item
        K2:
          Z1K1:
            Z1K1:
              Z1K1: Z9
              Z9K1: Z7
            Z7K1:
              Z1K1: Z9
              Z9K1: Z881
            Z881K1:
              Z1K1: Z9
              Z9K1: Z1
          K1:
            Z1K1:
              Z1K1: Z9
              Z9K1: Z11
            Z11K1:
              Z1K1: Z9
              Z9K1: Z1002
            Z11K2:
              Z1K1: Z6
              Z6K1: second item
          K2:
            Z1K1:
              Z1K1:
                Z1K1: Z9
                Z9K1: Z7
              Z7K1:
                Z1K1: Z9
                Z9K1: Z881
              Z881K1:
                Z1K1: Z9
                Z9K1: Z1
      expected:
        - Z1
        - first item
        - Z1K1: Z11
          Z11K1: Z1002
          Z11K2: second item

    - name: typed list of lists
      object:
        Z1K1:
          Z1K1:
            Z1K1: Z9
            Z9K1: Z7
          Z7K1:
            Z1K1: Z9
            Z9K1: Z881
          Z881K1:
            Z1K1:
              Z1K1: Z9
              Z9K1: Z7
            Z7K1:
              Z1K1: Z9
              Z9K1: Z881
            Z881K1:
              Z1K1: Z9
              Z9K1: Z1
        K1:
          Z1K1:
            Z1K1:
              Z1K1: Z9
              Z9K1: Z7
            Z7K1:
              Z1K1: Z9
              Z9K1: Z881
            Z881K1:
              Z1K1: Z9
              Z9K1: Z1
        K2:
          Z1K1:
            Z1K1:
              Z1K1: Z9
              Z9K1: Z7
            Z7K1:
              Z1K1: Z9
              Z9K1: Z881
            Z881K1:
              Z1K1:
                Z1K1: Z9
                Z9K1: Z7
              Z7K1:
                Z1K1: Z9
                Z9K1: Z881
              Z881K1:
                Z1K1: Z9
                Z9K1: Z1
      expected:
        - Z1K1: Z7
          Z7K1: Z881
          Z881K1: Z1
        - [ Z1 ]

    - name: typed list, type is literal
      object:
        Z1K1:
          Z1K1: Z4
          Z4K1:
            Z1K1: Z7
            Z7K1: Z881
            Z881K1: Z1
          Z4K2:
            Z1K1:
              Z1K1: Z7
              Z7K1: Z881
              Z881K1: Z3
            K1:
              Z1K1: Z3
              Z3K1: Z1
              Z3K2: K1
              Z3K3:
                Z1K1: Z12
                Z12K1:
                  Z1K1:
                    Z1K1: Z7
                    Z7K1: Z881
                    Z881K1: Z11
            K2:
              Z1K1:
                Z1K1: Z7
                Z7K1: Z881
                Z881K1: Z3
              K1:
                Z1K1: Z3
                Z3K1:
                  Z1K1: Z7
                  Z7K1: Z881
                  Z881K1: Z1
                Z3K2: K2
                Z3K3:
                  Z1K1: Z12
                  Z12K1:
                    Z1K1:
                      Z1K1: Z7
                      Z7K1: Z881
                      Z881K1: Z11
              K2:
                Z1K1:
                  Z1K1: Z7
                  Z7K1: Z881
                  Z881K1: Z3
          Z4K3: Z831
        K1:
          Z1K1: Z6
          Z6K1: first element
        K2:
          Z1K1:
            Z1K1: Z7
            Z7K1: Z881
            Z881K1: Z1
      expected:
        - Z1
        - first element

  throws:
    - name: string with wrong key
      object:
        Z1K1: Z6
        Z6K2: Z
      errors:
        - Z502
        - Z532

    - name: malformed normalized reference
      object:
        Z1K1: Z9
        Z9K1: tuber
      errors:
        - Z502

    - name: malformed key
      object:
        Z1K1: Z9
        tuber: Z7
      errors:
        - Z502
        - Z525

    - name: normalized terminal zobject
      object:
        Z1K1:
          Z1K1: Z9
          Z9K1: Z6
        Z6K1: error
      errors:
        - Z502
