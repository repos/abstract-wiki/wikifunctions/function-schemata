#[cfg(test)]
mod tests {

    use function_schemata::{AcceptedErrors, can_deserialize_as_request};
    use hex::FromHexError;
    use std::fs::read_to_string;

    #[derive(Debug)]
    enum TestErrors {
        HexError(()),
        LibError(()),
        StdError(()),
    }

    impl From<FromHexError> for TestErrors {
        fn from(error: FromHexError) -> TestErrors {
            println!("error is {:?}", error);
            TestErrors::HexError(())
        }
    }

    impl From<AcceptedErrors> for TestErrors {
        fn from(_error: AcceptedErrors) -> TestErrors {
            TestErrors::LibError(())
        }
    }

    impl From<std::io::Error> for TestErrors {
        fn from(_error: std::io::Error) -> TestErrors {
            TestErrors::StdError(())
        }
    }

    static BUFFER_FILE: &str = "../test_data/binary_format/hex.txt";

    #[test]
    fn test_deserialization() -> Result<(), TestErrors> {
        let mut buffer_string = read_to_string(BUFFER_FILE)?.to_string();
        buffer_string.pop(); // remove trailing newline
        println!("buffer_string is {:?}", buffer_string);
        let mut buffer = hex::decode(buffer_string)?;
        println!("buffer is {:?}", buffer);
        println!("buffer.len is {:?}", buffer.len());
        // let mut without_version: &[u8] = &buffer[6..];
        let result = can_deserialize_as_request(&mut buffer);
        assert!(result.is_ok());
        // assert!(true);
        Ok(())
    }

}
