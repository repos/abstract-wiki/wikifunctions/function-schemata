use apache_avro::{
    to_avro_datum,
    to_avro_datum_schemata,
    types::{Record}
};

use function_schemata::{
    AcceptedErrors,
    read_schema_files
};

fn main() -> Result<(), AcceptedErrors> {
    let schemata = read_schema_files(&[
        "../data/avro-v1.0.0/CODE.avsc",
        "../data/avro-v1.0.0/TYPE_CONVERTER.avsc",
        "../data/avro-v1.0.0/Z6.avsc",
        "../data/avro-v1.0.0/Z9.avsc",
        "../data/avro-v1.0.0/Z99.avsc",
        "../data/avro-v1.0.0/Z1.avsc",
        "../data/avro-v1.0.0/ARGUMENT.avsc",
        "../data/avro-v1.0.0/REQUEST.avsc"
    ])?;
    let mut ztypes_schemata = Vec::new();
    for schema in &schemata {
        ztypes_schemata.push(schema);
    }

    // Serialize a Z6/String.
    let mut the_string = Record::new(&schemata[2]).unwrap();
    the_string.put("Z6K1", "foo");
    let string_result = to_avro_datum(&schemata[2], the_string);
    println!("{:?}", string_result);

    // Serialize a CODE object.
    let mut the_code = Record::new(&schemata[0]).unwrap();
    the_code.put("codeString", "code");
    the_code.put("functionName", "function");
    let code_result = to_avro_datum(&schemata[0], the_code.clone());
    println!("{:?}", code_result);
   
    // Serialize a TYPE_CONVERTER object, demonstrating that references among
    // schemata still work. 
    let mut the_converter = Record::new(&schemata[1]).unwrap();
    the_converter.put("code", the_code.clone());
    the_converter.put("isList", false);
    let converter_result = to_avro_datum_schemata(&schemata[1], ztypes_schemata.clone(), the_converter);
    println!("{:?}", converter_result);

    Ok(())
}
