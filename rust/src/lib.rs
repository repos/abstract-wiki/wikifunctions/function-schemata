use apache_avro::{
    from_avro_datum_schemata,
    Schema
};
use std::{
    fs::File,
    io::Read
};

#[derive(Debug)]
pub enum AcceptedErrors {
    AvroError(()),
    StdError(()),
}

impl From<apache_avro::Error> for AcceptedErrors {
    fn from(error: apache_avro::Error) -> AcceptedErrors {
        println!("error is: {:?}", error);
        AcceptedErrors::AvroError(())
    }
}

impl From<std::io::Error> for AcceptedErrors {
    fn from(_error: std::io::Error) -> AcceptedErrors {
        AcceptedErrors::StdError(())
    }
}

/*
fn serde_as_number(value: serde_json::Value) -> Result<u8, AcceptedErrors> {
    let maybe_number = value.as_number();
    match maybe_number {
        Some(the_number) => Ok(the_number),
        _ => Err(JsonWasNotIntegerArray.into())
    }
}

fn serde_as_array(value: serde_json::Value) -> Result<Vec<serde_json::Value>, AcceptedErrors> {
    let maybe_array = value.as_array();
    match maybe_array {
        Some(the_array) => Ok(the_array?),
        _ => Err(JsonWasNotIntegerArray.into())
    }
}

pub fn read_buffer_from_txt(binary_file: &str) -> {
    let file_contents_result = read_to_string(binary_file);
    assert!(file_contents_result.is_ok());
    let file_contents = file_contents_result.unwrap();
    let deserialized_result = serde_json::from_str(file_contents.as_str());
    assert!(deserialized_result.is_ok());
    let deserialized: serde_json::Value = deserialized_result.unwrap();

    // let the_vec = serde_as_array(deserialized)?;
    let buffer: [u8; 5] = Default::default();
    for (i, number) in the_vec.into_iter().enumerate() {
        buffer[i] = serde_as_number(number)?;
    }
    Ok(buffer)
}
*/

pub fn read_schema_files(file_names: &[&str; 8]) -> Result<Vec<Schema>, AcceptedErrors> {
    let mut schema_strings: [String; 8] = Default::default();
    let mut schema_definitions: [&str; 8] = Default::default();
    for (index, file_name) in file_names.into_iter().enumerate() {
        // Read input file.
        let mut file = File::open(file_name)?;
        file.read_to_string(&mut schema_strings[index])?;
    }
    // Gather references.
    for index in 0..schema_strings.len() {
        // println!("contents are {}", &schema_strings[index]);
        schema_definitions[index] = &schema_strings[index];
    }

    // Define schemata.
    Ok(Schema::parse_list(&schema_definitions)?)
}

pub fn can_deserialize_as_request(mut buffer: &[u8]) -> Result<(), AcceptedErrors> {
    let schemata = read_schema_files(&[
        "../data/avro-v1.0.0/CODE.avsc",
        "../data/avro-v1.0.0/TYPE_CONVERTER.avsc",
        "../data/avro-v1.0.0/Z6.avsc",
        "../data/avro-v1.0.0/Z9.avsc",
        "../data/avro-v1.0.0/Z99.avsc",
        "../data/avro-v1.0.0/Z1.avsc",
        "../data/avro-v1.0.0/ARGUMENT.avsc",
        "../data/avro-v1.0.0/REQUEST.avsc"
    ])?;
    let mut ztypes_schemata = Vec::new();
    for schema in &schemata {
        ztypes_schemata.push(schema);
    }
    from_avro_datum_schemata(&schemata[7], ztypes_schemata, &mut buffer, None)?;
    Ok(())
}
