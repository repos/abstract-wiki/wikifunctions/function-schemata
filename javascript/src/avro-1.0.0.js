'use strict';

const avro = require( 'avro-js' );
const { dataDir, readJSON } = require( './fileUtils.js' );

const fnames = [
	'CODE.avsc',
	'TYPE_CONVERTER.avsc',
	'Z6.avsc',
	'Z9.avsc',
	'Z99.avsc',
	'Z1.avsc',
	'ARGUMENT.avsc',
	'REQUEST.avsc'
];
const schemaDefs = fnames.map(
	( fname ) => readJSON( dataDir( 'avro-v1.0.0', fname ) ) );
const allSchemata = avro.parse( schemaDefs );
const v100Schema = allSchemata._types[ 7 ];

module.exports = { v100Schema };
