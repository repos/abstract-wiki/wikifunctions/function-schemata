'use strict';

/* eslint no-use-before-define: ["error", { "functions": false }] */

const { convertZListToItemArray, isZObjectReference, isString, makeMappedResultEnvelope, wrapInQuote } = require( './utils.js' );
const { SchemaFactory } = require( './schema' );
const normalize = require( './normalize.js' );
const { getError } = require( './utils' );

const normalFactory = SchemaFactory.NORMAL();
const normalZ1Validator = normalFactory.create( 'Z1' );
const Z4Validator = normalFactory.create( 'Z4_literal' );
const Z5Validator = normalFactory.create( 'Z5_literal' );
const Z6Validator = normalFactory.create( 'Z6_literal' );
const Z7Validator = normalFactory.create( 'Z7_literal' );
const Z9Validator = normalFactory.create( 'Z9_literal' );
const Z99Validator = normalFactory.create( 'Z99_literal' );
const TypedListValidator = normalFactory.create( 'LIST_literal' );

/**
 * Canonicalizes a ZObject.
 *
 * @param {Object} zobject a ZObject
 * @return {Object} zobject that has been 'canonicalized'
 */
function canonicalizeObject( zobject ) {
	if ( Z99Validator.validate( zobject ) ) {
		return {
			Z1K1: canonicalize( zobject.Z1K1 ),
			Z99K1: zobject.Z99K1
		};
	}

	if ( Z9Validator.validate( zobject ) ) {
		zobject.Z9K1 = canonicalize( zobject.Z9K1 );

		// return as string if Z9K1 is a valid reference string
		if ( isString( zobject.Z9K1 ) && isZObjectReference( zobject.Z9K1 ) ) {
			return zobject.Z9K1;
		}
	}

	if ( Z6Validator.validate( zobject ) ) {
		zobject.Z6K1 = canonicalize( zobject.Z6K1 );

		// return as string if Z6/String doesn't need to be escaped, i.e., is not in Zxxxx format
		if ( isString( zobject.Z6K1 ) && !isZObjectReference( zobject.Z6K1 ) ) {
			return zobject.Z6K1;
		}
	}

	if ( TypedListValidator.validate( zobject ) ) {
		const itemList = convertZListToItemArray( zobject || [] ).map( ( e ) => canonicalize( e ) );

		let itemType;
		// FIXME: Should we search recursively for Z881?
		if (
			Z7Validator.validate( zobject.Z1K1 ) &&
			( canonicalize( zobject.Z1K1.Z7K1 ) === 'Z881' )
		) {
			// If type is a function call to Z881,
			// itemType is the canonical content of Z88K1.
			itemType = canonicalize( zobject.Z1K1.Z881K1 );
		} else if (
			Z4Validator.validate( zobject.Z1K1 ) &&
			Z7Validator.validate( zobject.Z1K1.Z4K1 ) &&
			( canonicalize( zobject.Z1K1.Z4K1.Z7K1 ) === 'Z881' )
		) {
			// If type is a literal and Z4K1 is function call to Z881,
			// itemType is the content of Z4K1.Z88K1
			itemType = canonicalize( zobject.Z1K1.Z4K1.Z881K1 );
		} else {
			// Else, itemType is the whole type ZObject transformed into canonical form.
			itemType = canonicalize( zobject.Z1K1 );
		}
		return [ itemType ].concat( itemList );
	}

	const keys = Object.keys( zobject );
	const result = {};

	for ( let i = 0; i < keys.length; i++ ) {
		result[ keys[ i ] ] = canonicalize( zobject[ keys[ i ] ] );
	}
	return result;
}

/**
 * Canonicalizes a ZObject unless it is a string.
 *
 * The input is assumed to be a well-formed ZObject, or else the behaviour is undefined
 *
 * @param {Object} zobject a ZObject
 * @return {Object|undefined} zobject that has been 'canonicalized' or undefined
 */
function canonicalize( zobject ) {
	if ( isString( zobject ) ) {
		return zobject;
	}
	return canonicalizeObject( zobject );
}

/**
 * Canonicalizes a normalized ZObject. Returns a Z22/'Evaluation result' containing the
 * canonicalized ZObject or a Z5/Error (in the metadata map of the Z22).
 *
 * @param {Object} zobject a ZObject
 * @return {Object} a Z22
 */
function canonicalizeExport( zobject ) {
	const errors = require( './error.js' );

	const normalized = normalize( zobject );

	const possibleError = getError( normalized );
	if ( ( Z5Validator.validateStatus( possibleError ) ).isValid() ) {
		return makeMappedResultEnvelope(
			null,
			errors.makeErrorInCanonicalForm(
				errors.error.unable_to_canonicalize,
				[ wrapInQuote( zobject ), possibleError ]
			)
		);
	}

	const status = normalZ1Validator.validateStatus( normalized );

	if ( status.isValid() ) {
		return makeMappedResultEnvelope( canonicalize( normalized.Z22K1 ), null );
	} else {
		return makeMappedResultEnvelope(
			null,
			errors.makeErrorInCanonicalForm(
				errors.error.unable_to_canonicalize,
				[ wrapInQuote( zobject ), status.getZ5() ]
			)
		);
	}
}

module.exports = canonicalizeExport;
