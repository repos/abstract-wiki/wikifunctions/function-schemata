'use strict';

const Ajv = require( 'ajv' ).default;

const fs = require( 'fs' );
const path = require( 'path' );
const { createZObjectKey, findTypeDefinition, findTypeIdentity, getError, isMemberOfDangerTrio, isVoid, isBuiltInType, convertZListToItemArray } = require( './utils.js' );
const { readYaml } = require( './fileUtils.js' );
const { ValidationStatus } = require( './validationStatus.js' );

const SCHEMA_NAME_REGEX = '(Z[1-9]\\d*(K[1-9]\\d*)?|LIST|RESOLVER|GENERIC)';

let Z1Validator, Z4Validator, Z5Validator, Z6Validator, Z7Validator,
	Z9Validator, Z18Validator, Z40Validator, Z99Validator;

function initializeValidators() {
	// eslint-disable-next-line no-use-before-define
	const defaultFactory = SchemaFactory.NORMAL();

	Z1Validator = defaultFactory.create( 'Z1' );
	Z4Validator = defaultFactory.create( 'Z4_literal' );
	Z5Validator = defaultFactory.create( 'Z5_literal' );
	Z6Validator = defaultFactory.create( 'Z6_literal' );
	Z7Validator = defaultFactory.create( 'Z7_literal' );
	Z9Validator = defaultFactory.create( 'Z9_literal' );
	Z18Validator = defaultFactory.create( 'Z18_literal' );
	Z40Validator = defaultFactory.create( 'Z40_literal' );
	Z99Validator = defaultFactory.create( 'Z99_literal' );
}

function newAjv() {
	return new Ajv( {
		allowMatchingProperties: true,
		verbose: true,
		strictTuples: false,
		strictTypes: false } );
}

// TODO (T296659): Migrate validatesAs* functions to utils. Somehow avoid
// incurring circular import problem in the process.

/**
 * Determines whether argument is a valid ZObject.
 *
 * @param {Object} Z1 object to be validated
 * @return {ValidationStatus} Status is only valid if Z1 validates as a Z1
 */
function validatesAsZObject( Z1 ) {
	return Z1Validator.validateStatus( Z1 );
}

/**
 * Determines whether argument is a Z4.
 *
 * @param {Object} Z1 a ZObject
 * @return {ValidationStatus} Status is only valid if Z1 validates as Z4
 */
function validatesAsType( Z1 ) {
	return Z4Validator.validateStatus( Z1 );
}

/**
 * Determines whether argument is a Z5.
 *
 * @param {Object} Z1 a ZObject
 * @return {ValidationStatus} Status is only valid if Z1 validates as Z5
 */
function validatesAsError( Z1 ) {
	return Z5Validator.validateStatus( Z1 );
}

/**
 * Determines whether argument is a Z6 or Z9. These two types' Z1K1s are
 * strings instead of Z9s, so some checks below need to special-case their
 * logic.
 *
 * @param {Object} Z1 a ZObject
 * @return {ValidationStatus} Status is only valid if Z1 validates as either Z6 or Z7
 */
function validatesAsString( Z1 ) {
	return Z6Validator.validateStatus( Z1 );
}

/**
 * Determines whether argument is a Z9.
 *
 * @param {Object} Z1 a ZObject
 * @return {ValidationStatus} Status is only valid if Z1 validates as Z9
 */
function validatesAsReference( Z1 ) {
	return Z9Validator.validateStatus( Z1 );
}

/**
 * Validates a ZObject against the Function Call schema.
 *
 * @param {Object} Z1 object to be validated
 * @return {ValidationStatus} whether Z1 can validated as a Function Call
 */
function validatesAsFunctionCall( Z1 ) {
	return Z7Validator.validateStatus( Z1 );
}

/**
 * Validates a ZObject against the Argument Reference schema.
 *
 * @param {Object} Z1 object to be validated
 * @return {ValidationStatus} whether Z1 can validated as an Argument Reference
 */
function validatesAsArgumentReference( Z1 ) {
	return Z18Validator.validateStatus( Z1 );
}

/**
 * Validates a ZObject against the Quote schema.
 *
 * @param {Object} Z1 object to be validated
 * @return {ValidationStatus} whether Z1 can validated as a Quote
 */
function validatesAsQuote( Z1 ) {
	return Z99Validator.validateStatus( Z1 );
}

/**
 * Validates a ZObject against the Boolean schema.
 *
 * @param {Object} Z1 object to be validated
 * @return {ValidationStatus} whether Z1 can validated as a Boolean
 */
function validatesAsBoolean( Z1 ) {
	return Z40Validator.validateStatus( Z1 );
}

class BaseSchema {

	constructor() {
		this.keyMap_ = new Map();
	}

	/**
	 * Validate a JSON object using validateStatus method; return only whether
	 * the result was valid without surfacing errors.
	 *
	 * @param {Object} maybeValid a JSON object
	 * @return {ValidationStatus} whether the object is valid
	 */
	validate( maybeValid ) {
		return this.validateStatus( maybeValid ).isValid();
	}

	/**
	 * @param {string} key
	 * @return {Schema} a schema for a sub part of the main schema.
	 */
	subValidator( key ) {
		return this.keyMap_.get( key );
	}

	/**
	 * @return {Array<string>} All the available subvalidators' keys.
	 */
	subValidatorKeys() {
		return Array.from( this.keyMap_.keys() );
	}
}

class Schema extends BaseSchema {
	constructor( validate, subValidators = null ) {
		super();
		this.validate_ = validate;
		if ( subValidators !== null ) {
			for ( const key of subValidators.keys() ) {
				this.keyMap_.set( key, new Schema( subValidators.get( key ) ) );
			}
		}
	}

	/**
	 * Try to validate a JSON object against the internal JSON schema validator.
	 * The results are used to instantiate a ValidationStatus object that is
	 * returned.
	 *
	 * @param {Object} maybeValid a JSON object
	 * @return {ValidationStatus} a validation status instance
	 */
	validateStatus( maybeValid ) {
		const result = this.validate_( maybeValid );
		const validationStatus = new ValidationStatus( this.validate_, result );
		return validationStatus;
	}
}

const noSchema_ = { not: {} };
const noAjv_ = new Ajv();
const justNo_ = new Schema( noAjv_.compile( noSchema_ ) );

class GenericSchema extends BaseSchema {
	constructor( keyMap ) {
		super();
		this.updateKeyMap( keyMap );
	}

	updateKeyMap( keyMap ) {
		this.keyMap_ = keyMap;
	}

	/**
	 * Try to validate a JSON object against the internal validators. For each
	 * key in maybeValid, the corresponding value will be validated against the
	 * appropriate validator in this.keyMap_.
	 *
	 * The results are used to instantiate a ValidationStatus object that is
	 * returned.
	 *
	 * @param {Object} maybeValid a JSON object
	 * @return {ValidationStatus} a validation status instance
	 */
	validateStatus( maybeValid ) {
		const allKeys = new Set( Object.keys( maybeValid ) );
		allKeys.delete( 'Z1K1' );
		for ( const key of this.keyMap_.keys() ) {
			const toValidate = maybeValid[ key ];
			// TODO (T290996): How to signal non-optional keys?
			if ( toValidate === undefined ) {
				continue;
			}
			allKeys.delete( key );

			// Allow unresolved Z7, Z9, or Z18 to pass validation. This is a stopgap
			// measure that will be phased out pending a massive validator overhaul.
			if ( isMemberOfDangerTrio( toValidate ) ) {
				continue;
			}

			// If key is not present, maybeValid[ key ] is undefined, which will
			// not validate well.
			const howsIt = this.keyMap_.get( key ).validateStatus( toValidate );
			if ( !howsIt.isValid() ) {
				// TODO (T296842): Somehow include key.
				// TODO (T296842): Consider conjunction of all errors?
				return howsIt;
			}
		}

		// TODO (T296842): Better errors for stray keys; allow non-local keys?
		if ( allKeys.size > 0 ) {
			return justNo_.validateStatus( maybeValid );
		}
		return new ValidationStatus( null, true );
	}
}

function dataDir( ...pathComponents ) {
	return path.join(
		path.dirname( path.dirname( path.dirname( __filename ) ) ),
		'data', ...pathComponents );
}

class SchemaFactory {

	constructor( ajv = null ) {
		if ( ajv === null ) {
			ajv = newAjv();
		}
		this.ajv_ = ajv;
	}

	/**
	 * Initializes a SchemaFactory linking schemata for canonical ZObjects.
	 *
	 * @return {SchemaFactory} factory with all canonical schemata included
	 */
	static CANONICAL() {
		// Add all schemata for normal ZObjects to ajv's parsing context.
		const ajv = newAjv();
		const directory = dataDir( 'CANONICAL' );
		// eslint-disable-next-line security/detect-unsafe-regex
		const fileRegex = /((Z[1-9]\d*(K[1-9]\d*)?)|(LIST)|(RESOLVER))\.yaml/;

		// eslint-disable-next-line security/detect-non-literal-fs-filename
		for ( const fileName of fs.readdirSync( directory ) ) {
			if ( fileName.match( fileRegex ) === null ) {
				throw new Error( "Schema not found: '" + fileName + "'" );
			}
			const fullFile = path.join( directory, fileName );
			ajv.addSchema( readYaml( fullFile ) );
		}
		return new SchemaFactory( ajv );
	}

	/**
	 * Initializes a SchemaFactory for mixed form Z1.
	 *
	 * @return {SchemaFactory} factory with lonely mixed form schema
	 */
	static MIXED() {
		const ajv = newAjv();
		const directory = dataDir( 'MIXED' );
		for ( const filename of [ 'Z1.yaml', 'Z99.yaml' ] ) {
			ajv.addSchema( readYaml( path.join( directory, filename ) ) );
		}
		return new SchemaFactory( ajv );
	}

	/**
	 * Initializes a SchemaFactory linking schemata for normal-form ZObjects.
	 *
	 * @return {SchemaFactory} factory with all normal-form schemata included
	 */
	static NORMAL() {
		// Add all schemata for normal ZObjects to ajv's parsing context.
		const ajv = newAjv();
		const directory = dataDir( 'NORMAL' );
		// eslint-disable-next-line security/detect-unsafe-regex
		const fileRegex = /((Z[1-9]\d*(K[1-9]\d*)?)|(GENERIC)|(LIST)|(RESOLVER))\.yaml/;

		// eslint-disable-next-line security/detect-non-literal-fs-filename
		for ( const fileName of fs.readdirSync( directory ) ) {
			if ( fileName.match( fileRegex ) === null ) {
				throw new Error( "Schema not found: '" + fileName + "'" );
			}
			const fullFile = path.join( directory, fileName );
			const schema = readYaml( fullFile );
			ajv.addSchema( schema );

			// Add literal schema too
			const id = schema.$id + '_literal';
			// Checks whether a literal definition exists
			if ( schema.definitions.objects[ id ] ) {
				const literal = {
					$id: id,
					$ref: schema.$ref + '_literal',
					definitions: schema.definitions
				};
				ajv.addSchema( literal );
			}
		}
		return new SchemaFactory( ajv );
	}

	/**
	 * Try to compile a schema. Use the factory's internal ajv_ in order to
	 * resolve references among multiple files.
	 *
	 * @param {Object} schema a JSON object containing a JSON Schema object
	 * @return {Schema} a Schema wrapping the resulting validator or null
	 */
	parse( schema ) {
		try {
			const validate = this.ajv_.compile( schema );
			return new Schema( validate );
		} catch ( err ) {
			throw new Error( 'Could not parse schema `' + err.message + '`, schema was: ' + JSON.stringify( schema ) );
		}
	}

	/**
	 * Gets the AJV schemas for *all* the sub components of a schema that is defined
	 * in the ZID_literal.properties section.
	 * The returned map looks like Z1K1: AjvValidator, Z8K1: AjvValidator...
	 * If such mapping cannot be found, an empty map will be returned.
	 *
	 * @param {string} schemaName the name of the schema. It can be ZID or ZID_literal.
	 * @return {Map<string, *>} A mapping of the subcomponent keys and their AJV schema.
	 */
	getSubSchemas_( schemaName ) {
		// For both ZID schema and ZID_literal schema, the schema definition is the same.
		// This behavior is defined by the schema factory above.
		let zid;
		if ( schemaName.match( `^${ SCHEMA_NAME_REGEX }$` ) ) {
			zid = schemaName;
		} else if ( schemaName.match( `^${ SCHEMA_NAME_REGEX }_literal$` ) ) {
			zid = schemaName.split( '_' )[ 0 ];
		} else {
			throw new Error(
				`Cannot process schema name to get sub-validators: ${ schemaName }. ` +
				'Accecptable format examples: Z42 and Z42_literal.'
			);
		}

		const overallSchema = this.ajv_.getSchema( schemaName ).schema;
		// If this schema doesn't have a zid_literal field or the field does not
		// contain properties (like Z1), we simply return an empty map.
		if ( !overallSchema.definitions.objects[ `${ zid }_literal` ] ||
			!overallSchema.definitions.objects[ `${ zid }_literal` ].properties ) {
			return new Map();
		}
		const keys = Object.keys( overallSchema.definitions.objects[ `${ zid }_literal` ].properties );
		const keyPathPrefix = `${ zid }#/definitions/objects/${ zid }_literal/properties/`;
		return new Map(
			keys.map( ( k ) => [ k, this.ajv_.getSchema( `${ keyPathPrefix }${ k }` ) ] ) );
	}

	/**
	 * Create a schema for the desired native type. A schema for normalized
	 * Z11s, for example, can be created as easily as
	 *
	 *  const factory = SchemaFactory.NORMAL();
	 *  const Z11Schema = factory.create("Z11");
	 *
	 * @param {string} schemaName the name of a supported schema
	 * @return {Schema} a fully-initialized Schema or null if unsupported
	 */
	create( schemaName ) {
		let type = schemaName;
		if ( schemaName === 'Z41' || schemaName === 'Z42' ) {
			type = 'Z40';
		}
		let validate = null;
		const message = null;
		validate = this.ajv_.getSchema( type );
		if ( validate === null || validate === undefined ) {
			throw new Error( 'Could not find schema: "' + schemaName + '" – ' + message );
		}
		const subValidators = this.getSubSchemas_( type );
		return new Schema( validate, subValidators );
	}

	/**
	 * Create a Map[ key -> BaseSchema] for a given Z4. Resultant Map indicates
	 * against which validators to test the elements of a ZObject with the
	 * corresponding keys.
	 *
	 * @param {Object} Z4 a Z4/Type
	 * @param {Map} typeCache mapping from typekeys (see createZObjectKey) to BaseSchemata
	 * @return {Map} mapping from type keys to BaseSchemata
	 */
	keyMapForUserDefined( Z4, typeCache ) {
		const keyMap = new Map();
		const Z3s = convertZListToItemArray( Z4.Z4K2 || [] );
		for ( const Z3 of Z3s ) {
			const propertyName = Z3.Z3K2.Z6K1;
			const propertyType = Z3.Z3K1;
			const identity = findTypeIdentity( propertyType ) || findTypeDefinition( propertyType );
			let subValidator;
			// TODO (T316787): Ensure that this works properly for nested user-
			// defined types.
			if ( validatesAsReference( identity ).isValid() &&
                isBuiltInType( identity.Z9K1 ) ) {
				subValidator = this.create( identity.Z9K1 );
			} else {
				const key = createZObjectKey( propertyType );
				if ( !( typeCache.has( key ) ) ) {
					typeCache.set(
						key,
						this.createUserDefined( [ propertyType ] ).get( key ) );
				}
				subValidator = typeCache.get( key );
			}
			keyMap.set( propertyName, subValidator );
		}
		return keyMap;
	}

	/**
	 * Create a schema for given user-defined type. The Z4 corresponding to the
	 * type must be provided.
	 *
	 * Currently only works for normal form.
	 *
	 * TODO (T296843): Maybe make this work for canonical forms, too.
	 *
	 * Usage:
	 *
	 *  // Z4 is a Z4 corresponding to a user-defined type
	 *  const factory = SchemaFactory.NORMAL();
	 *  const Z10001Schema = factory.createUserDefined([Z4]);
	 *
	 * @param {Object} Z4s the descriptor for the user-defined types
	 * @return {Schema} a fully-initialized Schema
	 */
	createUserDefined( Z4s ) {
		const typeCache = new Map();
		const normalize = require( './normalize.js' );

		const normalZ4s = [];
		for ( let errorIndex = 0; errorIndex < Z4s.length; ++errorIndex ) {
			const Z4 = Z4s[ errorIndex ];
			const normalizedEnvelope = normalize( Z4 );
			if ( !isVoid( getError( normalizedEnvelope ) ) ) {
				throw new Error( 'Failed to normalized Z4 at index: ' + errorIndex + '. Object: ' + JSON.stringify( Z4 ) );
			}
			normalZ4s.push( normalizedEnvelope.Z22K1 );
		}

		const zObjectKeys = [];

		// Create a GenericSchema for each of the requested Z4s.
		for ( const Z4 of normalZ4s ) {
			const key = createZObjectKey( Z4 );
			zObjectKeys.push( key );
			typeCache.set( key, new GenericSchema( new Map() ) );
		}

		// Populate all of the GenericSchemata with key maps.
		// We iterate twice here to avoid circular references and to ensure we
		// don't try to create schemata multiple times for any given Z4.
		for ( let i = 0; i < normalZ4s.length; ++i ) {
			const Z4 = normalZ4s[ i ];
			const key = zObjectKeys[ i ];
			typeCache.get( key ).updateKeyMap( this.keyMapForUserDefined( Z4, typeCache ) );
		}
		return typeCache;
	}

}

initializeValidators();

module.exports = {
	SchemaFactory,
	validatesAsZObject,
	validatesAsType,
	validatesAsError,
	validatesAsString,
	validatesAsFunctionCall,
	validatesAsReference,
	validatesAsQuote,
	validatesAsArgumentReference,
	validatesAsBoolean
};
