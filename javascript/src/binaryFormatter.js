'use strict';

const avro = require( 'avro-js' );
const semver = require( 'semver' );
const { dataDir, readJSON } = require( './fileUtils.js' );
const { validatesAsQuote, validatesAsReference, validatesAsString } = require( './schema.js' );
const {
	convertZListToItemArray,
	findTypeConverterToCodeIdentity,
	findFunctionIdentity,
	findTypeConverterFromCodeIdentity,
	findTypeIdentity,
	isObject,
	isZFunctionCall,
	isZReference,
	isZType
} = require( './utils.js' );
const { v100Schema } = require( './avro-1.0.0.js' );

const Z1_SCHEMA_ = readJSON( dataDir( 'avro', 'Z1.avsc' ) );
const Z6_SCHEMA_ = readJSON( dataDir( 'avro', 'Z6.avsc' ) );
const Z9_SCHEMA_ = readJSON( dataDir( 'avro', 'Z9.avsc' ) );
const Z99_SCHEMA_ = readJSON( dataDir( 'avro', 'Z99.avsc' ) );

// A ZObject is the union of arbitrary Z1s, Z6s, and Z9s.
// Note that Z1_SCHEMA_ must go last because it references the others by name.
const Z1_UNION_ = [ Z6_SCHEMA_, Z9_SCHEMA_, Z99_SCHEMA_, Z1_SCHEMA_ ];
// semver 0.0.1
const avroSchema = avro.parse( Z1_UNION_ );

// Requests in v0.0.2 also indicate whether to use reentrance.
const V002_REQUEST_SCHEMA_ = {
	type: 'record',
	namespace: 'ztypes',
	name: 'ZOBJECT',
	fields: [
		{
			name: 'reentrant',
			type: 'boolean'
		},
		{
			name: 'zobject',
			type: Z1_UNION_
		}
	]
};
// semver 0.0.2
const zobjectSchemaV002 = avro.parse( V002_REQUEST_SCHEMA_ );

// Requests in v0.0.3 rely on a minimal set of information instead of an entire
// Z7, allowing for further compression and facilitating later extension.
const V003_REQUEST_SCHEMA_ = {
	type: 'record',
	namespace: 'ztypes',
	name: 'ZOBJECT',
	fields: [
		{
			name: 'reentrant',
			type: 'boolean'
		},
		{
			name: 'codingLanguage',
			type: 'string'
		},
		{
			name: 'codeString',
			type: 'string'
		},
		{
			name: 'functionName',
			type: 'string'
		},
		{
			name: 'functionArguments',
			type: {
				type: 'map',
				values: Z1_UNION_
			}
		}
	]
};
// semver 0.0.3
const requestSchemaV003 = avro.parse( V003_REQUEST_SCHEMA_ );

// Requests in v0.0.4 rely on a minimal set of information instead of an entire
// Z7, allowing for further compression and facilitating later extension.
const V004_REQUEST_SCHEMA_ = {
	type: 'record',
	namespace: 'ztypes',
	name: 'ZOBJECT',
	fields: [
		{
			name: 'reentrant',
			type: 'boolean'
		},
		{
			name: 'remainingTime',
			type: 'double'
		},
		{
			name: 'codingLanguage',
			type: 'string'
		},
		{
			name: 'codeString',
			type: 'string'
		},
		{
			name: 'functionName',
			type: 'string'
		},
		{
			name: 'functionArguments',
			type: {
				type: 'map',
				values: Z1_UNION_
			}
		}
	]
};
// semver 0.0.4
const requestSchemaV004 = avro.parse( V004_REQUEST_SCHEMA_ );

// v0.0.5: like v0.0.4 but with a request ID!
const V005_REQUEST_SCHEMA_ = {
	type: 'record',
	namespace: 'ztypes',
	name: 'ZOBJECT',
	fields: [
		{
			name: 'reentrant',
			type: 'boolean'
		},
		{
			name: 'remainingTime',
			type: 'double'
		},
		{
			name: 'codingLanguage',
			type: 'string'
		},
		{
			name: 'codeString',
			type: 'string'
		},
		{
			name: 'functionName',
			type: 'string'
		},
		{
			name: 'requestId',
			type: 'string'
		},
		{
			name: 'functionArguments',
			type: {
				type: 'map',
				values: Z1_UNION_
			}
		}
	]
};
// semver 0.0.5
const requestSchemaV005 = avro.parse( V005_REQUEST_SCHEMA_ );

// v0.1.0: first new version after release.
// Like v0.0.5 but with type converters
// Also encapsulates functionName, codeString in a CODE record.
const CODE_SCHEMA_ = {
	type: 'record',
	namespace: 'ztypes',
	name: 'CODE',
	fields: [
		{
			name: 'codeString',
			type: 'string'
		},
		{
			name: 'functionName',
			type: 'string'
		}
	]
};
const ARGUMENT_SCHEMA_ = {
	type: 'record',
	namespace: 'ztypes',
	name: 'ARGUMENT',
	fields: [
		{
			name: 'deserializer',
			type: [ 'null', 'ztypes.CODE' ],
			default: null
		},
		{
			name: 'object',
			type: Z1_UNION_
		}
	]
};
const V010_REQUEST_SCHEMA_ = {
	type: 'record',
	namespace: 'ztypes',
	name: 'ZOBJECT',
	fields: [
		{
			name: 'reentrant',
			type: 'boolean'
		},
		{
			name: 'remainingTime',
			type: 'double'
		},
		{
			name: 'requestId',
			type: 'string'
		},
		{
			name: 'codingLanguage',
			type: 'string'
		},
		{
			name: 'function',
			type: CODE_SCHEMA_
		},
		{
			name: 'serializer',
			type: [ 'null', 'ztypes.CODE' ],
			default: null
		},
		{
			name: 'functionArguments',
			type: {
				type: 'map',
				values: ARGUMENT_SCHEMA_
			}
		}
	]
};
// semver 0.1.0
const requestSchemaV010 = avro.parse( V010_REQUEST_SCHEMA_ );

// v0.1.1: first new version after release.
// Like v0.1.0 but with an additional Boolean to support lists of types
const TYPE_CONVERTER_SCHEMA_ = {
	type: 'record',
	namespace: 'ztypes',
	name: 'TYPE_CONVERTER',
	fields: [
		{
			name: 'code',
			type: 'ztypes.CODE'
		},
		{
			name: 'isList',
			type: 'boolean',
			default: false
		}
	]
};
const V011_ARGUMENT_SCHEMA_ = {
	type: 'record',
	namespace: 'ztypes',
	name: 'ARGUMENT',
	fields: [
		{
			name: 'deserializer',
			type: [ 'null', 'ztypes.TYPE_CONVERTER' ],
			default: null
		},
		{
			name: 'object',
			type: Z1_UNION_
		}
	]
};
const V011_REQUEST_SCHEMA_ = {
	type: 'record',
	namespace: 'ztypes',
	name: 'ZOBJECT',
	fields: [
		{
			name: 'schemaVersion',
			type: 'string'
		},
		{
			name: 'reentrant',
			type: 'boolean'
		},
		{
			name: 'remainingTime',
			type: 'double'
		},
		{
			name: 'requestId',
			type: 'string'
		},
		{
			name: 'codingLanguage',
			type: 'string'
		},
		{
			name: 'function',
			type: CODE_SCHEMA_
		},
		{
			name: 'serializer',
			type: [ 'null', TYPE_CONVERTER_SCHEMA_ ],
			default: null
		},
		{
			name: 'functionArguments',
			type: {
				type: 'map',
				values: V011_ARGUMENT_SCHEMA_
			}
		}
	]
};
// semver 0.1.1
const requestSchemaV011 = avro.parse( V011_REQUEST_SCHEMA_ );

/**
 * Transform a ZObject into the form expected by the binary schema.
 *
 * @param {Object} ZObject a normal-form ZObject
 * @return {Object} an object in the intermediate binary format
 */
function convertNormalForBinaryFormat( ZObject ) {
	if ( !isObject( ZObject ) ) {
		throw new Error( `ZObject was not an Object: ${ JSON.stringify( ZObject ) }.` );
	}
	if ( validatesAsQuote( ZObject ).isValid() ) {
		return { 'ztypes.Z99': { Z99K1: JSON.stringify( ZObject.Z99K1 ) } };
	}
	if ( validatesAsReference( ZObject ).isValid() ) {
		return { 'ztypes.Z9': { Z9K1: ZObject.Z9K1 } };
	}
	if ( validatesAsString( ZObject ).isValid() ) {
		return { 'ztypes.Z6': { Z6K1: ZObject.Z6K1 } };
	}
	const valueMap = {};
	for ( const key of Object.keys( ZObject ) ) {
		valueMap[ key ] = convertNormalForBinaryFormat( ZObject[ key ] );
	}
	return { 'ztypes.Z1': { valueMap: valueMap } };
}

/**
 * Recover a ZObject from the form imposed by the binary schema.
 *
 * @param {Object} binaryFormatZObject a ZObject in the intermediate binary format
 * @return {Object} a normal-form ZObject
 */
function recoverNormalFromBinaryFormat( binaryFormatZObject ) {
	const Z1 = binaryFormatZObject[ 'ztypes.Z1' ];
	if ( Z1 !== undefined ) {
		const result = {};
		for ( const key of Object.keys( Z1.valueMap ) ) {
			result[ key ] = recoverNormalFromBinaryFormat( Z1.valueMap[ key ] );
		}
		return result;
	}
	const Z6 = binaryFormatZObject[ 'ztypes.Z6' ];
	if ( Z6 !== undefined ) {
		return { Z1K1: 'Z6', Z6K1: Z6.Z6K1 };
	}
	const Z9 = binaryFormatZObject[ 'ztypes.Z9' ];
	if ( Z9 !== undefined ) {
		return { Z1K1: 'Z9', Z9K1: Z9.Z9K1 };
	}
	const Z99 = binaryFormatZObject[ 'ztypes.Z99' ];
	if ( Z99 !== undefined ) {
		return {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z99'
			},
			Z99K1: JSON.parse( Z99.Z99K1 )
		};
	}
	throw new Error( 'Invalid binary form; must define one of ztypes.(Z1|Z6|Z9|Z99}' );
}

function programmingLanguagesMatch( desiredLanguage, retrievedLanguage ) {
	for ( const language of [ 'python', 'javascript' ] ) {
		if (
			desiredLanguage.startsWith( language ) &&
                retrievedLanguage.startsWith( language ) ) {
			return true;
		}
	}
	return false;
}

function findTypeConverterForProgrammingLanguage( desiredLanguage, typeConverterList, getCode ) {
	const typeConverterArray = (
		( typeConverterList === undefined ) ?
			[] : convertZListToItemArray( typeConverterList ) );
	for ( const typeConverter of typeConverterArray ) {
		const retrievedLanguage = getCode( typeConverter ).Z16K1.Z61K1.Z6K1;
		if ( programmingLanguagesMatch( desiredLanguage, retrievedLanguage ) ) {
			return typeConverter;
		}
	}
	return null;
}

/**
 * Given a programming language and a list of type converters, find the one whose
 * programming language is compatible with the desired language.
 *
 * @param {string} desiredLanguage the desired programming lanugage
 * @param {Object} typeConverterList a Z881/List of either Z46/Type converters to code or
 *   Z64/Type converters from code
 * @param {Function} getCode callback which retrieves the Code object from a given Z46 or Z64
 * @param {Function} getIdentity get Z9/Reference Identity for a Z46 or Z64
 * @return {Object|null} a matching type converter, or null if none is found
 */
function getCodeObjectForTypeConverterList(
	desiredLanguage, typeConverterList, getCode, getIdentity ) {
	const typeConverter = findTypeConverterForProgrammingLanguage(
		desiredLanguage,
		typeConverterList,
		getCode );
	if ( typeConverter === null ) {
		return null;
	}
	// The Type Converter's Identity must be a Z9/Reference or we can't proceed.
	const typeConverterIdentity = getIdentity( typeConverter );
	if ( typeConverterIdentity === null ) {
		return null;
	}
	const typeConverterCode = getCode( typeConverter );
	return {
		'ztypes.CODE': {
			codeString: typeConverterCode.Z16K2.Z6K1,
			functionName: typeConverterIdentity.Z9K1
		}
	};
}

function convertRequestToBinaryPostTypeConverters_(
	functionCallRequest, maybeSetSerializer, maybeSetDeserializer ) {
	const response = {
		reentrant: functionCallRequest.reentrant,
		remainingTime: functionCallRequest.remainingTime,
		requestId: functionCallRequest.requestId,
		function: {},
		functionArguments: {}
	};

	const functionCall = functionCallRequest.zobject;
	const theFunction = functionCall.Z7K1;

	// We assume that the orchestrator will have already selected a single implementation,
	// so the first implementation will be the only one.
	const firstImplementation = theFunction.Z8K4.K1;
	const functionCode = firstImplementation.Z14K3;

	const programmingLanguageObject = functionCode.Z16K1;
	let programmingLanguage;
	if ( isZReference( programmingLanguageObject ) ) {
		programmingLanguage = programmingLanguageObject.Z9K1;
	} else {
		programmingLanguage = programmingLanguageObject.Z61K1.Z6K1;
	}
	response.codingLanguage = programmingLanguage;
	response.function.codeString = functionCode.Z16K2.Z6K1;
	const functionIdentity = findFunctionIdentity( theFunction );
	if ( functionIdentity === null ) {
		throw new Error( 'Identity of Z8 was not a Z9: ' + JSON.stringify( theFunction ) );
	}
	response.function.functionName = functionIdentity.Z9K1;

	maybeSetSerializer( theFunction, programmingLanguage, response );

	for ( const key of Object.keys( functionCall ) ) {
		if ( key === 'Z1K1' || key === 'Z7K1' ) {
			continue;
		}
		const value = functionCall[ key ];
		const argument = {};
		const argumentType = value.Z1K1;

		maybeSetDeserializer( argumentType, programmingLanguage, argument );

		// No reason to type convert the whole dang type.
		let valueToTypeConvert = value;
		if ( isZType( argumentType ) ) {
			const typeIdentity = findTypeIdentity( argumentType );
			if ( typeIdentity ) {
				valueToTypeConvert = { ...value };
				if (
					argument.deserializer &&
                        argument.deserializer[ 'ztypes.TYPE_CONVERTER' ] &&
                        argument.deserializer[ 'ztypes.TYPE_CONVERTER' ].isList
				) {
					let tail = valueToTypeConvert;
					while ( tail ) {
						tail.Z1K1 = typeIdentity;
						tail = tail.K2;
					}
				} else {
					valueToTypeConvert.Z1K1 = typeIdentity;
				}
			}
		}
		argument.object = convertNormalForBinaryFormat( valueToTypeConvert );
		response.functionArguments[ key ] = argument;
	}
	return response;
}

function convertRequestToBinaryV011Plus(
	functionCallRequest, schemaVersion, enableTypeConverters ) {

	const setSerializer = ( theFunction, programmingLanguage, response ) => {
		// Add a type converter if the information is present in the function's return type.
		// If return type is not given as a Z4/Type, or if it lacks the Z4K8 key,
		// we cannot populate the type converter.
		let returnType = theFunction.Z8K2;
		let returnsList = false;
		const returnTypeIdentity = findTypeIdentity( returnType );
		if ( isZFunctionCall( returnTypeIdentity ) ) {
			const returnFunctionIdentity = findFunctionIdentity( returnTypeIdentity.Z7K1 );
			if ( returnFunctionIdentity && returnFunctionIdentity.Z9K1 === 'Z881' ) {
				returnsList = true;
				returnType = returnType.Z4K2.K1.Z3K1;
			}
		}
		let fromTypeConverterCode = null;
		if ( enableTypeConverters ) {
			fromTypeConverterCode = getCodeObjectForTypeConverterList(
				programmingLanguage,
				returnType.Z4K8,
				( Z64 ) => Z64.Z64K3,
				findTypeConverterFromCodeIdentity );
		}
		if ( fromTypeConverterCode !== null ) {
			fromTypeConverterCode = {
				'ztypes.TYPE_CONVERTER': {
					code: fromTypeConverterCode[ 'ztypes.CODE' ],
					isList: returnsList
				}
			};
			response.serializer = fromTypeConverterCode;
		}
	};

	const setDeserializer = ( argumentType, programmingLanguage, argument ) => {
		let toTypeConverterCode = null;
		let isList = false;
		const argumentIdentity = findTypeIdentity( argumentType );
		if ( isZFunctionCall( argumentIdentity ) ) {
			const argumentFunctionIdentity = findFunctionIdentity( argumentIdentity.Z7K1 );
			if ( argumentFunctionIdentity && argumentFunctionIdentity.Z9K1 === 'Z881' ) {
				isList = true;
				argumentType = argumentType.Z4K2.K1.Z3K1;
			}
		}
		if ( enableTypeConverters ) {
			toTypeConverterCode = getCodeObjectForTypeConverterList(
				programmingLanguage,
				argumentType.Z4K7,
				( Z46 ) => Z46.Z46K3,
				findTypeConverterToCodeIdentity );
		}
		if ( toTypeConverterCode !== null ) {
			toTypeConverterCode = {
				'ztypes.TYPE_CONVERTER': {
					code: toTypeConverterCode[ 'ztypes.CODE' ],
					isList: isList
				}
			};
			argument.deserializer = toTypeConverterCode;
		}
	};

	const result = convertRequestToBinaryPostTypeConverters_(
		functionCallRequest, setSerializer, setDeserializer );
	result.schemaVersion = schemaVersion;
	return result;
}

function convertRequestToBinaryV010Plus( functionCallRequest, enableTypeConverters = false ) {

	const setSerializer = ( theFunction, programmingLanguage, response ) => {
		// Add a type converter if the information is present in the function's return type.
		// If return type is not given as a Z4/Type, or if it lacks the Z4K8 key,
		// we cannot populate the type converter.
		const returnType = theFunction.Z8K2;
		let fromTypeConverterCode = null;
		if ( enableTypeConverters ) {
			fromTypeConverterCode = getCodeObjectForTypeConverterList(
				programmingLanguage,
				returnType.Z4K8,
				( Z64 ) => Z64.Z64K3,
				findTypeConverterFromCodeIdentity );
		}
		if ( fromTypeConverterCode !== null ) {
			response.serializer = fromTypeConverterCode;
		}
	};

	const setDeserializer = ( argumentType, programmingLanguage, argument ) => {
		let toTypeConverterCode = null;
		if ( enableTypeConverters ) {
			toTypeConverterCode = getCodeObjectForTypeConverterList(
				programmingLanguage,
				argumentType.Z4K7,
				( Z46 ) => Z46.Z46K3,
				findTypeConverterToCodeIdentity );
		}
		if ( toTypeConverterCode !== null ) {
			argument.deserializer = toTypeConverterCode;
		}
	};

	return convertRequestToBinaryPostTypeConverters_(
		functionCallRequest, setSerializer, setDeserializer );
}

function convertRequestToBinaryV003Plus( ZObject ) {
	const response = {
		reentrant: ZObject.reentrant,
		functionArguments: {}
	};

	// This is the only difference between semver v0.0.3 and v0.0.4.
	if ( ZObject.remainingTime !== null ) {
		response.remainingTime = ZObject.remainingTime;
	}
	// This is the only difference between semver v0.0.4 and v0.0.5.
	if ( ZObject.requestId !== null ) {
		response.requestId = ZObject.requestId;
	}

	const actualObject = ZObject.zobject;
	for ( const key of Object.keys( actualObject ) ) {
		if ( key === 'Z1K1' ) {
			continue;
		}
		const value = actualObject[ key ];
		if ( key === 'Z7K1' ) {
			response.functionName = value.Z8K5.Z9K1;
			const firstImplementation = value.Z8K4.K1;
			response.codingLanguage = firstImplementation.Z14K3.Z16K1.Z61K1.Z6K1;
			response.codeString = firstImplementation.Z14K3.Z16K2.Z6K1;
			continue;
		}
		response.functionArguments[ key ] = convertNormalForBinaryFormat( value );
	}
	return response;
}

function convertFormattedRequestToBinaryV011Plus( request ) {
	const result = { ...request };
	result.functionArguments = {};
	for ( const key of Object.keys( request.functionArguments ) ) {
		const argument = {};
		result.functionArguments[ key ] = argument;
		const original = request.functionArguments[ key ];
		argument.object = convertNormalForBinaryFormat( original.object );
		if ( original.deserializer ) {
			argument.deserializer = { 'ztypes.TYPE_CONVERTER': original.deserializer };
		}
	}
	if ( result.serializer ) {
		result.serializer = { 'ztypes.TYPE_CONVERTER': result.serializer };
	} else {
		delete result.serializer;
	}
	return result;
}

function convertFormattedRequestToBinaryV010Plus( request ) {
	const result = { ...request };
	result.functionArguments = {};
	for ( const key of Object.keys( request.functionArguments ) ) {
		const argument = {};
		result.functionArguments[ key ] = argument;
		const original = request.functionArguments[ key ];
		argument.object = convertNormalForBinaryFormat( original.object );
		if ( original.deserializer ) {
			argument.deserializer = { 'ztypes.CODE': original.deserializer };
		}
	}
	if ( result.serializer ) {
		result.serializer = { 'ztypes.CODE': result.serializer };
	} else {
		delete result.serializer;
	}
	return result;
}

function convertFormattedRequestToBinary( request, version ) {
	if ( semver.gte( version, '0.1.1' ) ) {
		const response = convertFormattedRequestToBinaryV011Plus( request );
		if ( semver.gte( version, '1.0.0' ) ) {
			return v100Schema.toBuffer( response );
		} else {
			return requestSchemaV011.toBuffer( response );
		}
	}
	if ( semver.gte( version, '0.1.0' ) ) {
		const response = convertFormattedRequestToBinaryV010Plus( request );
		return requestSchemaV010.toBuffer( response );
	}
	throw new Error( 'Formatted request -> binary operation only supported for semvers >= 0.1.0' );
}

function convertZObjectToBinary( ZObject, version = '0.1.0', enableTypeConverters = false ) {
	if ( semver.gte( version, '0.1.1' ) ) {
		const response = convertRequestToBinaryV011Plus( ZObject, version, enableTypeConverters );
		if ( semver.gte( version, '1.0.0' ) ) {
			return v100Schema.toBuffer( response );
		} else {
			return requestSchemaV011.toBuffer( response );
		}
	}
	if ( semver.gte( version, '0.1.0' ) ) {
		const response = convertRequestToBinaryV010Plus( ZObject, enableTypeConverters );
		return requestSchemaV010.toBuffer( response );
	}
	if ( semver.gte( version, '0.0.3' ) ) {
		const response = convertRequestToBinaryV003Plus( ZObject );
		if ( semver.gte( version, '0.0.5' ) ) {
			return requestSchemaV005.toBuffer( response );
		}
		if ( semver.gte( version, '0.0.4' ) ) {
			return requestSchemaV004.toBuffer( response );
		}
		return requestSchemaV003.toBuffer( response );
	}
	if ( semver.gte( version, '0.0.2' ) ) {
		return zobjectSchemaV002.toBuffer( {
			reentrant: ZObject.reentrant,
			zobject: convertNormalForBinaryFormat( ZObject.zobject )
		} );
	}
	return avroSchema.toBuffer( convertNormalForBinaryFormat( ZObject ) );
}

function recoverRequestFromAvroFormat011Plus( recovered ) {
	// Copy all members so that the result loses the ZOBJECT type annotation.
	const result = {
		reentrant: recovered.reentrant,
		remainingTime: recovered.remainingTime,
		requestId: recovered.requestId,
		codingLanguage: recovered.codingLanguage,
		function: {},
		functionArguments: {}
	};
	result.function.codeString = recovered.function.codeString;
	result.function.functionName = recovered.function.functionName;
	if ( recovered.serializer === null ) {
		result.serializer = null;
	} else {
		const rawFromTypeConverter = recovered.serializer[ 'ztypes.TYPE_CONVERTER' ];
		const fromTypeConverter = { code: {} };
		fromTypeConverter.code.codeString = rawFromTypeConverter.code.codeString;
		fromTypeConverter.code.functionName = rawFromTypeConverter.code.functionName;
		fromTypeConverter.isList = rawFromTypeConverter.isList;
		result.serializer = fromTypeConverter;
	}
	result.functionArguments = {};
	for ( const key of Object.keys( recovered.functionArguments ) ) {
		const argument = {};
		const original = recovered.functionArguments[ key ];
		argument.object = recoverNormalFromBinaryFormat( original.object );
		if ( original.deserializer === null ) {
			argument.deserializer = null;
		} else {
			const rawToTypeConverter = original.deserializer[ 'ztypes.TYPE_CONVERTER' ];
			const toTypeConverter = { code: {} };
			toTypeConverter.code.codeString = rawToTypeConverter.code.codeString;
			toTypeConverter.code.functionName = rawToTypeConverter.code.functionName;
			toTypeConverter.isList = rawToTypeConverter.isList;
			argument.deserializer = toTypeConverter;
		}
		result.functionArguments[ key ] = argument;
	}
	result.schemaVersion = recovered.schemaVersion;
	return result;
}

function recoverRequestFromAvroFormat010Plus( recovered ) {
	// Copy all members so that the result loses the ZOBJECT type annotation.
	const result = {
		reentrant: recovered.reentrant,
		remainingTime: recovered.remainingTime,
		requestId: recovered.requestId,
		codingLanguage: recovered.codingLanguage,
		function: {},
		functionArguments: {}
	};
	result.function.codeString = recovered.function.codeString;
	result.function.functionName = recovered.function.functionName;
	if ( recovered.serializer === null ) {
		result.serializer = null;
	} else {
		const rawFromTypeConverter = recovered.serializer[ 'ztypes.CODE' ];
		const fromTypeConverter = {};
		fromTypeConverter.codeString = rawFromTypeConverter.codeString;
		fromTypeConverter.functionName = rawFromTypeConverter.functionName;
		result.serializer = fromTypeConverter;
	}
	result.functionArguments = {};
	for ( const key of Object.keys( recovered.functionArguments ) ) {
		const argument = {};
		const original = recovered.functionArguments[ key ];
		argument.object = recoverNormalFromBinaryFormat( original.object );
		if ( original.deserializer === null ) {
			argument.deserializer = null;
		} else {
			const rawToTypeConverter = original.deserializer[ 'ztypes.CODE' ];
			const toTypeConverter = {};
			toTypeConverter.codeString = rawToTypeConverter.codeString;
			toTypeConverter.functionName = rawToTypeConverter.functionName;
			argument.deserializer = toTypeConverter;
		}
		result.functionArguments[ key ] = argument;
	}
	return result;
}

function recoverRequestFromAvroFormat3Plus( recovered ) {
	// Copy all members so that the result loses the ZOBJECT type annotation.
	const result = { ...recovered };
	for ( const key of Object.keys( recovered.functionArguments ) ) {
		const argument = recovered.functionArguments[ key ];
		result.functionArguments[ key ] = recoverNormalFromBinaryFormat( argument );
	}
	return result;
}

function getZObjectFromBinary( buffer, version = '1.0.0' ) {
	if ( semver.gte( version, '0.1.1' ) ) {
		let recovered;
		if ( semver.gte( version, '1.0.0' ) ) {
			recovered = v100Schema.fromBuffer( buffer );
		} else {
			recovered = requestSchemaV011.fromBuffer( buffer );
		}
		return recoverRequestFromAvroFormat011Plus( recovered );
	}
	if ( semver.gte( version, '0.1.0' ) ) {
		const recovered = requestSchemaV010.fromBuffer( buffer );
		return recoverRequestFromAvroFormat010Plus( recovered );
	}
	if ( semver.gte( version, '0.0.3' ) ) {
		let recovered;
		if ( semver.gte( version, '0.0.5' ) ) {
			recovered = requestSchemaV005.fromBuffer( buffer );
		} else if ( semver.gte( version, '0.0.4' ) ) {
			recovered = requestSchemaV004.fromBuffer( buffer );
		} else {
			recovered = requestSchemaV003.fromBuffer( buffer );
		}
		return recoverRequestFromAvroFormat3Plus( recovered );
	}
	if ( semver.gte( version, '0.0.2' ) ) {
		const recovered = zobjectSchemaV002.fromBuffer( buffer );
		return {
			reentrant: recovered.reentrant,
			zobject: recoverNormalFromBinaryFormat( recovered.zobject )
		};
	}
	return recoverNormalFromBinaryFormat( avroSchema.fromBuffer( buffer ) );
}

function addVersionToBuffer( formatted, version ) {
	const semverBuffer = Buffer.from( version );
	const lengthBuffer = Buffer.from( String.fromCharCode( semverBuffer.length ) );
	return Buffer.concat( [ lengthBuffer, semverBuffer, formatted ] );
}

function convertFormattedRequestToVersionedBinary( request, version ) {
	const response = convertFormattedRequestToBinary( request, version );
	return addVersionToBuffer( response, version );
}

function convertWrappedZObjectToVersionedBinary(
	wrappedZObject, version, enableTypeConverters = false ) {
	const response = convertZObjectToBinary( wrappedZObject, version, enableTypeConverters );
	return addVersionToBuffer( response, version );
}

function getWrappedZObjectFromVersionedBinary( buffer ) {
	const length = buffer.slice( 0, 1 );
	const lengthPivot = 1 + length.toString().charCodeAt( 0 );
	const version = buffer.slice( 1, lengthPivot ).toString();
	const avroFormatted = buffer.slice( lengthPivot, buffer.length );
	return getZObjectFromBinary( avroFormatted, version );
}

module.exports = {
	avroSchema,
	convertFormattedRequestToVersionedBinary,
	convertWrappedZObjectToVersionedBinary,
	convertZObjectToBinary,
	getWrappedZObjectFromVersionedBinary,
	getZObjectFromBinary,
	convertNormalForBinaryFormat,
	recoverNormalFromBinaryFormat,
	zobjectSchemaV002
};
