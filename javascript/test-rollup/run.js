'use strict';

const path = require( 'path' );

const { readFileLimited } = require( '../src/fileUtils.js' );

QUnit.module( 'rollup' );

{
	const temporaryPath = 'temporary-utils.js';
	const utilsPath = path.join( 'javascript', 'src', 'utils.js' );
	const temporary = readFileLimited( temporaryPath );
	const utils = readFileLimited( utilsPath );

	QUnit.test( 'rollup test', ( assert ) => {
		assert.deepEqual( utils, temporary, 'Output of rollup does not match utils.js; please run `npm run rollup`.' );
	} );
}
