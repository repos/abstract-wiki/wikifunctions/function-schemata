'use strict';

const { LoggerWrapper } = require( '../../../src/LoggerWrapper.js' );

QUnit.module( 'LoggerWrapper' );

class FakeLogger {

	constructor( args = null ) {
		this.level = null;
		this.data = null;
		this.args = args;
	}

	log( level, data ) {
		this.level = level;
		this.data = data;
	}

	child( args ) {
		return new FakeLogger( args );
	}

}

QUnit.test( 'log errors', ( assert ) => {
	const logger = new FakeLogger();
	const wrapper = new LoggerWrapper( logger );
	assert.throws( () => {
		wrapper.log( null, null );
	} );
} );

QUnit.test( 'trace', ( assert ) => {
	const logger = new FakeLogger();
	const wrapper = new LoggerWrapper( logger );
	const logMessage = 'i will arise and go now';
	wrapper.log( 'trace', logMessage );
	assert.strictEqual( logger.level, 'trace' );
	assert.true( typeof logger.data === 'object' );
} );

QUnit.test( 'debug', ( assert ) => {
	const logger = new FakeLogger();
	const wrapper = new LoggerWrapper( logger );
	wrapper.log( 'debug' );
	assert.strictEqual( logger.level, 'debug' );
	assert.true( typeof logger.data === 'object' );
} );

QUnit.test( 'info', ( assert ) => {
	const logger = new FakeLogger();
	const wrapper = new LoggerWrapper( logger );
	const logMessage = 'and a small cabin build there';
	const data = { message: logMessage };
	wrapper.log( 'info', data );
	assert.strictEqual( logger.level, 'info' );
	assert.strictEqual( logger.data, data );
} );

QUnit.test( 'warn', ( assert ) => {
	const logger = new FakeLogger();
	const wrapper = new LoggerWrapper( logger );
	const logMessage = 'of clay and wattles made';
	const data = { message: logMessage };
	wrapper.log( 'warn', data );
	assert.strictEqual( logger.level, 'warn' );
	assert.strictEqual( logger.data, data );
} );

QUnit.test( 'error', ( assert ) => {
	const logger = new FakeLogger();
	const wrapper = new LoggerWrapper( logger );
	const logMessage = 'nine bean-rows will I have there';
	const data = { message: logMessage };
	wrapper.log( 'error', data );
	assert.strictEqual( logger.level, 'error' );
	assert.strictEqual( logger.data, data );
} );

QUnit.test( 'fatal', ( assert ) => {
	const logger = new FakeLogger();
	const wrapper = new LoggerWrapper( logger );
	const logMessage = 'a hive for the honey-bee';
	const data = { message: logMessage };
	wrapper.log( 'fatal', data );
	assert.strictEqual( logger.level, 'fatal' );
	assert.strictEqual( logger.data, data );
} );

QUnit.test( 'child', ( assert ) => {
	const logger = new FakeLogger();
	const wrapper = new LoggerWrapper( logger );
	const logMessage = 'and live alone in the bee-loud glade';
	const args = { message: logMessage };
	const childWrapper = wrapper.child( args );
	assert.strictEqual( childWrapper._logger.args, args );
} );

QUnit.test( 'log from Executor with future timestamp', ( assert ) => {
	const logger = new FakeLogger();
	const wrapper = new LoggerWrapper( logger );
	const logMessage = 'hello from the other side';
	const data = { message: logMessage, time: '2055-06-25T12:34:56.789Z' };
	wrapper.log( 'info', data );
	const timeStamp = new Date();
	const loggedTime = Date.parse( logger.data.time );
	assert.true( Math.abs( timeStamp - loggedTime ) < 5 );

} );

QUnit.test( 'log from Executor with a timestamp that is not an ISO string type', ( assert ) => {
	const logger = new FakeLogger();
	const wrapper = new LoggerWrapper( logger );
	const logMessage = 'hello from the other side';
	const timeStamp = new Date();
	const data = { message: logMessage, time: timeStamp };
	wrapper.log( 'info', data );
	assert.strictEqual( logger.data.time, timeStamp.toISOString() );
} );

QUnit.test( 'log from Executor with valid timestamp', ( assert ) => {
	const logger = new FakeLogger();
	const wrapper = new LoggerWrapper( logger );
	const logMessage = 'hello from the other side';
	const timeStampISO = new Date().toISOString();
	const data = { message: logMessage, time: timeStampISO };
	wrapper.log( 'info', data );
	assert.strictEqual( logger.data.time, timeStampISO );
} );

QUnit.test( 'local-debug too', ( assert ) => {
	// This is to "cover" the local-debug log step.
	process.env.WIKIFUNCTIONS_DEBUG_LOCAL = 'yes';

	const logger = new FakeLogger();
	const wrapper = new LoggerWrapper( logger );
	const logMessage = 'Look at me, a locally-logged message!';
	const data = { message: logMessage };
	wrapper.log( 'info', data );
	assert.strictEqual( logger.level, 'info' );
	assert.strictEqual( logger.data, data );
} );

QUnit.test( 'requestId re-write pass-through', ( assert ) => {
	const logger = new FakeLogger();
	const wrapper = new LoggerWrapper( logger );
	const logMessage = 'Look at me, a locally-logged message!';
	const data = { message: logMessage, requestId: 'fish' };
	wrapper.log( 'info', data );
	assert.strictEqual( logger.level, 'info' );
	assert.strictEqual( logger.data[ 'x-request-id' ], 'fish' );
} );
