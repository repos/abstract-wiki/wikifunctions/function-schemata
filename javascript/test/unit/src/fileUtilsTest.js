'use strict';

const path = require( 'path' );

const {
	dataDir,
	readFileLimited,
	readJSON,
	readYaml
} = require( '../../../src/fileUtils.js' );

QUnit.module( 'fileUtils.js' );

QUnit.test( 'readFileLimited', ( assert ) => {

	const bigFilePath = path.join( 'test_data', 'function_call', 'too_big.json' );

	const bigFileLoad = readFileLimited( bigFilePath );
	assert.true( typeof bigFileLoad === 'string' );

	let limitedBigFileLoad;

	try {
		limitedBigFileLoad = readFileLimited( bigFilePath, 10000 );
	} catch ( error ) {
		// These are implementation details and not guaranteed-stable
		assert.strictEqual( error.name, 'Error' );
		assert.strictEqual( error.message, 'File test_data/function_call/too_big.json is larger than the maximum permitted size, 10000 bytes' );
	}
	// If we've set a response then something went wrong in going wrong.
	assert.deepEqual( limitedBigFileLoad, undefined );
} );

QUnit.test( 'dataDir', ( assert ) => {

	const naturalLanguages = dataDir( 'definitions/naturalLanguages.json' );
	assert.true( typeof naturalLanguages === 'string' );

} );

QUnit.test( 'readJSON', ( assert ) => {

	const naturalLanguagesPath = dataDir( 'definitions/naturalLanguages.json' );
	const naturalLanguages = readJSON( naturalLanguagesPath );
	assert.true( typeof naturalLanguages === 'object' );
	assert.true( naturalLanguages.en === 'Z1002' );
} );

QUnit.test( 'readYaml', ( assert ) => {

	// JSON files parse as YAML, so check with the above test
	const naturalLanguagesPath = dataDir( 'definitions/naturalLanguages.json' );
	const naturalLanguages = readYaml( naturalLanguagesPath );
	assert.true( typeof naturalLanguages === 'object' );
	assert.true( naturalLanguages.en === 'Z1002' );

	// … but also check with a real YAML file, too
	const canonicalZ2SchemaPath = path.join( 'test_data', 'canonical_zobject', 'Z2.yaml' );
	const canonicalZ2Schema = readYaml( canonicalZ2SchemaPath );

	assert.true( typeof canonicalZ2Schema === 'object' );
	assert.true( canonicalZ2Schema.test_information.name === 'canonical Z2' );
	assert.true( canonicalZ2Schema.test_objects.success[ 0 ].name === 'arbitrary Z2' );
} );
