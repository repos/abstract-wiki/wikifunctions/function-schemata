'use strict';

const { ZWrapperBase } = require( '../../../src/utils.js' );
const { error, makeErrorInNormalForm, makeErrorInCanonicalForm } = require( '../../../src/error.js' );

QUnit.module( 'error' );

QUnit.test( 'makeErrorInNormalForm: Undefined throws', ( assert ) => {
	assert.throws(
		() => {
			makeErrorInNormalForm();
		},
		( err ) => err.toString().includes( 'Missing error type' )
	);
} );

QUnit.test( 'makeErrorInCanonicalForm: Undefined throws', ( assert ) => {
	assert.throws(
		() => {
			makeErrorInCanonicalForm();
		},
		( err ) => err.toString().includes( 'Missing error type' )
	);
} );

QUnit.test( 'makeErrorInNormalForm: Non-ZID throws', ( assert ) => {
	assert.throws(
		() => {
			makeErrorInNormalForm( 'Hello' );
		},
		( err ) => err.toString().includes( 'Invalid error type' )
	);
} );

QUnit.test( 'makeErrorInCanonicalForm: Non-ZID throws', ( assert ) => {
	assert.throws(
		() => {
			makeErrorInCanonicalForm( 'Hello' );
		},
		( err ) => err.toString().includes( 'Invalid error type' )
	);
} );

QUnit.test( 'makeErrorInNormalForm: Z500 error, with no args', ( assert ) => {
	const testedError = makeErrorInNormalForm( error.unknown_error );
	assert.deepEqual(
		testedError,
		{
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z5' },
			Z5K1: { Z1K1: 'Z9', Z9K1: error.unknown_error },
			Z5K2: {
				Z1K1: {
					Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
					Z7K1: { Z1K1: 'Z9', Z9K1: 'Z885' },
					Z885K1: { Z1K1: 'Z9', Z9K1: error.unknown_error }
				}
			}
		}
	);
} );

QUnit.test( 'makeErrorInCanonicalForm: Z500 error, with no args', ( assert ) => {
	const testedError = makeErrorInCanonicalForm( error.unknown_error );
	assert.deepEqual(
		testedError,
		{
			Z1K1: 'Z5',
			Z5K1: error.unknown_error,
			Z5K2: {
				Z1K1: {
					Z1K1: 'Z7',
					Z7K1: 'Z885',
					Z885K1: error.unknown_error
				}
			}
		}
	);
} );

QUnit.test( 'makeErrorInNormalForm: Z500 error, with one arg', ( assert ) => {
	const testedError = makeErrorInNormalForm( error.unknown_error, [ 'hello' ] );
	assert.deepEqual(
		testedError,
		{
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z5' },
			Z5K1: { Z1K1: 'Z9', Z9K1: error.unknown_error },
			Z5K2: {
				Z1K1: {
					Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
					Z7K1: { Z1K1: 'Z9', Z9K1: 'Z885' },
					Z885K1: { Z1K1: 'Z9', Z9K1: error.unknown_error }
				},
				Z500K1: {
					Z1K1: 'Z6',
					Z6K1: 'hello'
				}
			}
		}
	);
} );

QUnit.test( 'makeErrorInCanonicalForm: Z500 error, with one arg', ( assert ) => {
	const testedError = makeErrorInCanonicalForm( error.unknown_error, [ 'hello' ] );
	assert.deepEqual(
		testedError,
		{
			Z1K1: 'Z5',
			Z5K1: error.unknown_error,
			Z5K2: {
				Z1K1: {
					Z1K1: 'Z7',
					Z7K1: 'Z885',
					Z885K1: error.unknown_error
				},
				Z500K1: 'hello'
			}
		}
	);
} );

QUnit.test( 'makeErrorInNormalForm: Z500 error, with two args', ( assert ) => {
	const testedError = makeErrorInNormalForm( error.unknown_error, [ 'hello', { Z1K1: 'Z9', Z9K1: 'Z41' } ] );
	assert.deepEqual(
		testedError,
		{
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z5' },
			Z5K1: { Z1K1: 'Z9', Z9K1: error.unknown_error },
			Z5K2: {
				Z1K1: {
					Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
					Z7K1: { Z1K1: 'Z9', Z9K1: 'Z885' },
					Z885K1: { Z1K1: 'Z9', Z9K1: error.unknown_error }
				},
				Z500K1: {
					Z1K1: 'Z6',
					Z6K1: 'hello'
				},
				Z500K2: {
					Z1K1: 'Z9',
					Z9K1: 'Z41'
				}
			}
		}
	);
} );

QUnit.test( 'makeErrorInCanonicalForm: Z500 error, with two args', ( assert ) => {
	const testedError = makeErrorInCanonicalForm( error.unknown_error, [ 'hello', 'Z41' ] );
	assert.deepEqual(
		testedError,
		{
			Z1K1: 'Z5',
			Z5K1: error.unknown_error,
			Z5K2: {
				Z1K1: {
					Z1K1: 'Z7',
					Z7K1: 'Z885',
					Z885K1: error.unknown_error
				},
				Z500K1: 'hello',
				Z500K2: 'Z41'
			}
		}
	);
} );

QUnit.test( 'makeErrorInCanonicalForm: Z506 error, with two args', ( assert ) => {
	const testedError = makeErrorInCanonicalForm( error.argument_type_mismatch, [ 'Z6', 'Z40', { Z1K1: 'Z39', Z39K1: 'K3' }, 'Z6' ] );
	assert.deepEqual(
		testedError,
		{
			Z1K1: 'Z5',
			Z5K1: error.argument_type_mismatch,
			Z5K2: {
				Z1K1: {
					Z1K1: 'Z7',
					Z7K1: 'Z885',
					Z885K1: error.argument_type_mismatch
				},
				Z506K1: 'Z6',
				Z506K2: 'Z40',
				Z506K3: { Z1K1: 'Z39', Z39K1: 'K3' },
				Z506K4: 'Z6'
			}
		}
	);
} );

class ZWrapperForTest extends ZWrapperBase {

	asJSON() {
		return { Z1K1: 'Z6', Z6K1: 'error' };
	}

}

QUnit.test( 'makeErrorInNormalForm: with ZWrapperBase', ( assert ) => {
	const testedError = makeErrorInNormalForm( error.unknown_error, [ new ZWrapperForTest() ] );
	assert.deepEqual(
		testedError,
		{
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z5' },
			Z5K1: { Z1K1: 'Z9', Z9K1: error.unknown_error },
			Z5K2: {
				Z1K1: {
					Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
					Z7K1: { Z1K1: 'Z9', Z9K1: 'Z885' },
					Z885K1: { Z1K1: 'Z9', Z9K1: error.unknown_error }
				},
				Z500K1: {
					Z1K1: 'Z6',
					Z6K1: 'error'
				}
			}
		}
	);

} );
