'use strict';

const {
	builtInTypes,
	convertItemArrayToZList,
	convertBenjaminArrayToZList,
	convertArrayToKnownTypedList,
	convertZListToItemArray,
	createZObjectKey,
	findBooleanIdentity,
	findFirstBadPath,
	findTypeDefinition,
	findTypeIdentity,
	findTypeConverterFromCodeIdentity,
	findTypeConverterToCodeIdentity,
	isMemberOfDangerTrio,
	inferItemType,
	isZArgumentReference,
	isZBoolean,
	isZEnvelope,
	isZFunction,
	isZFunctionCall,
	isZReference,
	isZString,
	isZType,
	isString,
	isArray,
	isObject,
	isKey,
	isZid,
	isItemId,
	isPropertyId,
	isLexemeId,
	isLexemeFormId,
	isLexemeSenseId,
	isEntityId,
	isReference,
	isZObject,
	isZObjectReference,
	isGlobalKey,
	deepEqual,
	deepCopy,
	// getHead,
	// getTail,
	getTypedListType,
	inferType,
	isBuiltInType,
	// isEmptyZList,
	isUserDefined,
	kidFromGlobalKey,
	makeFalse,
	makeMappedResultEnvelope,
	makeTrue,
	makeVoid,
	Mutex,
	isVoid,
	// wrapInKeyReference,
	// wrapInQuote,
	// wrapInZ6,
	// wrapInZ9,
	makeEmptyZMap,
	makeEmptyZResponseEnvelopeMap,
	isZMap,
	setZMapValue,
	getZMapValue,
	getError,
	setMetadataValue,
	setMetadataValues,
	stableStringify,
	ZWrapperBase
} = require( '../../../src/utils.js' );
const canonicalize = require( '../../../src/canonicalize.js' );
const normalize = require( '../../../src/normalize.js' );

QUnit.module( 'utils.js' );

QUnit.test( 'is*', ( assert ) => {
	assert.strictEqual( isString(), false );
	assert.strictEqual( isArray(), false );
	assert.strictEqual( isObject(), false );

	assert.strictEqual( isString( null ), false );
	assert.strictEqual( isArray( null ), false );
	assert.strictEqual( isObject( null ), false );

	assert.strictEqual( isString( '' ), true );
	assert.strictEqual( isArray( '' ), false );
	assert.strictEqual( isObject( '' ), false );

	assert.strictEqual( isString( [] ), false );
	assert.strictEqual( isArray( [] ), true );
	assert.strictEqual( isObject( [] ), false );

	assert.strictEqual( isString( {} ), false );
	assert.strictEqual( isArray( {} ), false );
	assert.strictEqual( isObject( {} ), true );
} );

QUnit.test( 'built-in types', ( assert ) => {
	for ( const ZID of builtInTypes() ) {
		assert.true( isBuiltInType( ZID ) );
		assert.false( isUserDefined( ZID ) );
	}
} );

QUnit.test( 'convertBenjaminArrayToZList with empty array, canonical', ( assert ) => {
	const array = [ 'Z1' ];
	const expected = {
		Z1K1: {
			Z1K1: 'Z7',
			Z7K1: 'Z881',
			Z881K1: 'Z1'
		}
	};
	assert.deepEqual( convertBenjaminArrayToZList( array, /* canonical= */true ), expected );
} );

QUnit.test( 'convertBenjaminArrayToZList with empty array, normal', ( assert ) => {
	const array = [ { Z1K1: 'Z9', Z9K1: 'Z1' } ];
	const expected = {
		Z1K1: {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z7'
			},
			Z7K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z881'
			},
			Z881K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z1'
			}
		}
	};
	assert.deepEqual( convertBenjaminArrayToZList( array ), expected );
} );

QUnit.test( 'convertItemArrayToZList with empty array, normal', ( assert ) => {
	const array = [];
	const expected = {
		Z1K1: {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z7'
			},
			Z7K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z881'
			},
			Z881K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z1'
			}
		}
	};
	assert.deepEqual( convertItemArrayToZList( array ), expected );
} );

QUnit.test( 'convertBenjaminArrayToZList with multiple types', ( assert ) => {
	const array = [
		{
			Z1K1: 'Z9',
			Z9K1: 'Z1'
		},
		{
			Z1K1: 'Z6',
			Z6K1: 'strang'
		},
		{
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z39'
			},
			Z39K1: {
				Z1K1: 'Z6',
				Z6K1: 'stronk'
			}
		}
	];
	const listZ1K1 = {
		Z1K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z7'
		},
		Z7K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z881'
		},
		Z881K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z1'
		}
	};
	const expected = {
		Z1K1: listZ1K1,
		K1: array[ 1 ],
		K2: {
			Z1K1: listZ1K1,
			K1: array[ 2 ],
			K2: {
				Z1K1: listZ1K1
			}
		}
	};
	assert.deepEqual( convertBenjaminArrayToZList( array ), expected );
} );

QUnit.test( 'convertItemArrayToZList with multiple types', ( assert ) => {
	const array = [
		{
			Z1K1: 'Z6',
			Z6K1: 'strang'
		},
		{
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z39'
			},
			Z39K1: {
				Z1K1: 'Z6',
				Z6K1: 'stronk'
			}
		}
	];
	const listZ1K1 = {
		Z1K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z7'
		},
		Z7K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z881'
		},
		Z881K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z1'
		}
	};
	const expected = {
		Z1K1: listZ1K1,
		K1: array[ 0 ],
		K2: {
			Z1K1: listZ1K1,
			K1: array[ 1 ],
			K2: {
				Z1K1: listZ1K1
			}
		}
	};
	assert.deepEqual( convertItemArrayToZList( array ), expected );
} );

QUnit.test( 'convertBenjaminArrayToZList with single type', ( assert ) => {
	const array = [
		{
			Z1K1: 'Z9',
			Z9K1: 'Z6'
		},
		{
			Z1K1: 'Z6',
			Z6K1: 'stink'
		},
		{
			Z1K1: 'Z6',
			Z6K1: 'stonk'
		}
	];
	const listZ1K1 = {
		Z1K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z7'
		},
		Z7K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z881'
		},
		Z881K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z6'
		}
	};
	const expected = {
		Z1K1: listZ1K1,
		K1: array[ 1 ],
		K2: {
			Z1K1: listZ1K1,
			K1: array[ 2 ],
			K2: {
				Z1K1: listZ1K1
			}
		}
	};
	assert.deepEqual( convertBenjaminArrayToZList( array ), expected );
} );

QUnit.test( 'convertItemArrayToZList with single type', ( assert ) => {
	const array = [
		{
			Z1K1: 'Z6',
			Z6K1: 'stink'
		},
		{
			Z1K1: 'Z6',
			Z6K1: 'stonk'
		}
	];
	const listZ1K1 = {
		Z1K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z7'
		},
		Z7K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z881'
		},
		Z881K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z6'
		}
	};
	const expected = {
		Z1K1: listZ1K1,
		K1: array[ 0 ],
		K2: {
			Z1K1: listZ1K1,
			K1: array[ 1 ],
			K2: {
				Z1K1: listZ1K1
			}
		}
	};
	assert.deepEqual( convertItemArrayToZList( array ), expected );
} );

QUnit.test( 'convertBenjaminArrayToZList with function call types', ( assert ) => {
	const array = [
		{
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z7'
			},
			Z7K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z8888'
			}
		},
		{
			Z1K1: {
				Z1K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z7'
				},
				Z7K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z8888'
				}
			},
			K1: 'strink'
		},
		{
			Z1K1: {
				Z1K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z7'
				},
				Z7K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z8888'
				}
			},
			K1: 'stronk'
		}
	];
	const listZ1K1 = {
		Z1K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z7'
		},
		Z7K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z881'
		},
		Z881K1: {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z7'
			},
			Z7K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z8888'
			}
		}
	};
	const expected = {
		Z1K1: listZ1K1,
		K1: array[ 1 ],
		K2: {
			Z1K1: listZ1K1,
			K1: array[ 2 ],
			K2: {
				Z1K1: listZ1K1
			}
		}
	};
	assert.deepEqual( convertBenjaminArrayToZList( array ), expected );
} );

QUnit.test( 'convertItemArrayToZList with function call types', ( assert ) => {
	const array = [
		{
			Z1K1: {
				Z1K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z7'
				},
				Z7K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z8888'
				}
			},
			K1: 'strink'
		},
		{
			Z1K1: {
				Z1K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z7'
				},
				Z7K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z8888'
				}
			},
			K1: 'stronk'
		}
	];
	const listZ1K1 = {
		Z1K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z7'
		},
		Z7K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z881'
		},
		Z881K1: {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z7'
			},
			Z7K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z8888'
			}
		}
	};
	const expected = {
		Z1K1: listZ1K1,
		K1: array[ 0 ],
		K2: {
			Z1K1: listZ1K1,
			K1: array[ 1 ],
			K2: {
				Z1K1: listZ1K1
			}
		}
	};
	assert.deepEqual( convertItemArrayToZList( array ), expected );
} );

QUnit.test( 'convertBenjaminArrayToZList with canonical function call types', ( assert ) => {
	const array = [
		{
			Z1K1: 'Z7',
			Z7K1: 'Z8888',
			Z8888K1: 'some arg'
		},
		{
			Z1K1: {
				Z1K1: 'Z7',
				Z7K1: 'Z8888',
				Z8888K1: 'some arg'
			},
			K1: 'strink'
		},
		{
			Z1K1: {
				Z1K1: 'Z7',
				Z7K1: 'Z8888',
				Z8888K1: 'some arg'
			},
			K1: 'stronk'
		}
	];
	const listZ1K1 = {
		Z1K1: 'Z7',
		Z7K1: 'Z881',
		Z881K1: {
			Z1K1: 'Z7',
			Z7K1: 'Z8888',
			Z8888K1: 'some arg'
		}
	};
	const expected = {
		Z1K1: listZ1K1,
		K1: array[ 1 ],
		K2: {
			Z1K1: listZ1K1,
			K1: array[ 2 ],
			K2: {
				Z1K1: listZ1K1
			}
		}
	};
	assert.deepEqual( convertBenjaminArrayToZList( array, /* canonical */true ), expected );
} );

QUnit.test( 'convertArrayToKnownTypedLists canonical with string type', ( assert ) => {
	const array = [ 'list of', 'strings' ];
	const listZ1K1 = {
		Z1K1: 'Z7',
		Z7K1: 'Z881',
		Z881K1: 'Z6'
	};
	const expected = {
		Z1K1: listZ1K1,
		K1: array[ 0 ],
		K2: {
			Z1K1: listZ1K1,
			K1: array[ 1 ],
			K2: {
				Z1K1: listZ1K1
			}
		}
	};
	assert.deepEqual( convertArrayToKnownTypedList( array, 'Z6', /* canonical */true ), expected );
} );

QUnit.test( 'convertArrayToKnownTypedLists normal with string type', ( assert ) => {
	const array = [ { Z1K1: 'Z6', Z6K1: 'list of' }, { Z1K1: 'Z6', Z6K1: 'strings' } ];
	const listZ1K1 = {
		Z1K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z7'
		},
		Z7K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z881'
		},
		Z881K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z6'
		}
	};
	const expected = {
		Z1K1: listZ1K1,
		K1: array[ 0 ],
		K2: {
			Z1K1: listZ1K1,
			K1: array[ 1 ],
			K2: {
				Z1K1: listZ1K1
			}
		}
	};
	assert.deepEqual( convertArrayToKnownTypedList( array, 'Z6' ), expected );
} );

QUnit.test( 'getTypedListType return canonical with canonical reference', ( assert ) => {
	const elementType = 'Z6';
	const expected = {
		Z1K1: 'Z7',
		Z7K1: 'Z881',
		Z881K1: 'Z6'
	};
	assert.deepEqual( getTypedListType( elementType, true ), expected );
} );

QUnit.test( 'getTypedListType return canonical with normal reference', ( assert ) => {
	const elementType = {
		Z1K1: 'Z9',
		Z9K1: 'Z6'
	};
	const expected = {
		Z1K1: 'Z7',
		Z7K1: 'Z881',
		Z881K1: 'Z6'
	};
	assert.deepEqual( getTypedListType( elementType, true ), expected );
} );

QUnit.test( 'getTypedListType return normal with canonical reference', ( assert ) => {
	const elementType = 'Z6';
	const expected = {
		Z1K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z7'
		},
		Z7K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z881'
		},
		Z881K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z6'
		}
	};
	assert.deepEqual( getTypedListType( elementType ), expected );
} );

QUnit.test( 'getTypedListType return normal with normal reference', ( assert ) => {
	const elementType = {
		Z1K1: 'Z9',
		Z9K1: 'Z6'
	};
	const expected = {
		Z1K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z7'
		},
		Z7K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z881'
		},
		Z881K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z6'
		}
	};
	assert.deepEqual( getTypedListType( elementType ), expected );
} );

QUnit.test( 'getTypedListType with canonical function call', ( assert ) => {
	const elementType = {
		Z1K1: 'Z7',
		Z7K1: 'Z885',
		Z885K1: 'Z500'
	};
	const expected = {
		Z1K1: 'Z7',
		Z7K1: 'Z881',
		Z881K1: elementType
	};
	assert.deepEqual( getTypedListType( elementType, true ), expected );
} );

QUnit.test( 'getTypedListType with normal function call', ( assert ) => {
	const elementType = {
		Z1K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z7'
		},
		Z7K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z8888'
		}
	};
	const expected = {
		Z1K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z7'
		},
		Z7K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z881'
		},
		Z881K1: {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z7'
			},
			Z7K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z8888'
			}
		}
	};
	assert.deepEqual( getTypedListType( elementType ), expected );
} );

QUnit.test( 'convertZListToItemArray with Typed List', ( assert ) => {
	const expected = [
		{
			Z1K1: 'Z6',
			Z6K1: 'stink'
		},
		{
			Z1K1: 'Z6',
			Z6K1: 'stonk'
		}
	];
	const listZ1K1 = {
		Z1K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z7'
		},
		Z7K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z881'
		},
		Z881K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z6'
		}
	};
	const ZList = {
		Z1K1: listZ1K1,
		K1: expected[ 0 ],
		K2: {
			Z1K1: listZ1K1,
			K1: expected[ 1 ],
			K2: {
				Z1K1: listZ1K1
			}
		}
	};
	assert.deepEqual( convertZListToItemArray( ZList ), expected );
} );

QUnit.test( 'convertZListToItemArray with undefined', ( assert ) => {
	assert.throws( () => convertZListToItemArray( undefined ) );
} );

QUnit.test( 'isZid', ( assert ) => {
	const testValues = [
		{ value: '', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Empty string' },

		{ value: 'Z1', isKey: false, isZid: true, isGlobalKey: false, isReference: true, isZObjectReference: true, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Trivial ZID' },
		{ value: 'Z123', isKey: false, isZid: true, isGlobalKey: false, isReference: true, isZObjectReference: true, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Simple ZID' },
		{ value: 'Z01', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Zero-padded ZID' },
		{ value: 'Z1234567890', isKey: false, isZid: true, isGlobalKey: false, isReference: true, isZObjectReference: true, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Long ZID' },
		{ value: ' \tZ1  \n ', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Whitespace-beset ZID' },
		{ value: 'Z', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Partial ZID' },

		{ value: 'Z1K1', isKey: true, isZid: false, isGlobalKey: true, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Trivial global key' },
		{ value: 'Z123K1', isKey: true, isZid: false, isGlobalKey: true, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Simple global key' },
		{ value: 'Z1234567890K1234567890', isKey: true, isZid: false, isGlobalKey: true, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Long global key' },
		{ value: 'Z01K1', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Zero-padded global key' },
		{ value: ' \tZ1K1  \n ', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Whitespace-beset global key' },
		{ value: 'ZK1', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Partial ZID global key' },
		{ value: 'Z1K', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Partial key global key' },

		{ value: 'A1', isKey: false, isZid: false, isGlobalKey: false, isReference: true, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Trivial non-Wikifunctions ID' },
		{ value: 'A123', isKey: false, isZid: false, isGlobalKey: false, isReference: true, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Simple non-Wikifunctions ID' },
		{ value: 'A1234567890', isKey: false, isZid: false, isGlobalKey: false, isReference: true, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Long non-Wikifunctions ID' },
		{ value: 'A01', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Zero-padded non-Wikifunctions ID' },
		{ value: ' \tA1  \n ', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Whitespace-beset non-Wikifunctions ID' },
		{ value: 'A', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Partial non-Wikifunctions ID' },

		{ value: 'K1', isKey: true, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Trivial local key' },
		{ value: 'K123', isKey: true, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Simple local key' },
		{ value: 'K1234567890', isKey: true, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Long local key' },
		{ value: 'K01', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Zero-padded local key' },
		{ value: ' \tK1  \n ', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Whitespace-beset local key' },
		{ value: 'K', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Partial local key' },

		{ value: 'Q1', isKey: false, isZid: false, isGlobalKey: false, isReference: true, isZObjectReference: false, isItemId: true, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: true, message: 'Trivial QID' },
		{ value: 'Q123', isKey: false, isZid: false, isGlobalKey: false, isReference: true, isZObjectReference: false, isItemId: true, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: true, message: 'Simple QID' },
		{ value: 'Q01', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Zero-padded QID' },
		{ value: 'Q1234567890', isKey: false, isZid: false, isGlobalKey: false, isReference: true, isZObjectReference: false, isItemId: true, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: true, message: 'Long QID' },
		{ value: ' \tQ1  \n ', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Whitespace-beset QID' },
		{ value: 'Q', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Partial QID' },

		{ value: 'P1', isKey: false, isZid: false, isGlobalKey: false, isReference: true, isZObjectReference: false, isItemId: false, isPropertyId: true,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: true, message: 'Trivial PID' },
		{ value: 'P123', isKey: false, isZid: false, isGlobalKey: false, isReference: true, isZObjectReference: false, isItemId: false, isPropertyId: true,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: true, message: 'Simple PID' },
		{ value: 'P01', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Zero-padded PID' },
		{ value: 'P1234567890', isKey: false, isZid: false, isGlobalKey: false, isReference: true, isZObjectReference: false, isItemId: false, isPropertyId: true,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: true, message: 'Long PID' },
		{ value: ' \tP1  \n ', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Whitespace-beset PID' },
		{ value: 'P', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Partial PID' },

		{ value: 'L1', isKey: false, isZid: false, isGlobalKey: false, isReference: true, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: true, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: true, message: 'Trivial LID' },
		{ value: 'L123', isKey: false, isZid: false, isGlobalKey: false, isReference: true, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: true, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: true, message: 'Simple LID' },
		{ value: 'L01', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Zero-padded LID' },
		{ value: 'L1234567890', isKey: false, isZid: false, isGlobalKey: false, isReference: true, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: true, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: true, message: 'Long LID' },
		{ value: ' \tL1  \n ', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Whitespace-beset LID' },
		{ value: 'L', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Partial LID' },

		{ value: 'L1-F1', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: true, isLexemeSenseId: false, isEntityId: true, message: 'Trivial LFID' },
		{ value: 'L123-F123', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: true, isLexemeSenseId: false, isEntityId: true, message: 'Simple LFID' },
		{ value: 'L1-F01', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Zero-padded LFID' },
		{ value: 'L1234567890-F1234567890', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: true, isLexemeSenseId: false, isEntityId: true, message: 'Long LFID' },
		{ value: ' \tL1-F1  \n ', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Whitespace-beset LFID' },
		{ value: 'L1-F', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Partial LFID' },
		{ value: 'L1-G1', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'No-F LFID' },

		{ value: 'L1-S1', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: true, isEntityId: true, message: 'Trivial LSID' },
		{ value: 'L123-S123', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: true, isEntityId: true, message: 'Simple LSID' },
		{ value: 'L1-S01', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Zero-padded LSID' },
		{ value: 'L1234567890-S1234567890', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: true, isEntityId: true, message: 'Long LSID' },
		{ value: ' \tL1-S1  \n ', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Whitespace-beset LSID' },
		{ value: 'L1-S', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'Partial LSID' },
		{ value: 'L1-G1', isKey: false, isZid: false, isGlobalKey: false, isReference: false, isZObjectReference: false, isItemId: false, isPropertyId: false,
			isLexemeId: false, isLexemeFormId: false, isLexemeSenseId: false, isEntityId: false, message: 'No-S LSID' }
	];

	testValues.forEach( ( testRun ) => {
		assert.strictEqual( isKey( testRun.value ), testRun.isKey, testRun.message + ': isKey' );
		assert.strictEqual( isZid( testRun.value ), testRun.isZid, testRun.message + ': isZid' );
		assert.strictEqual( isGlobalKey( testRun.value ), testRun.isGlobalKey, testRun.message + ': isGlobalKey' );
		assert.strictEqual( isReference( testRun.value ), testRun.isReference, testRun.message + ': isReference' );
		assert.strictEqual( isZObjectReference( testRun.value ), testRun.isZObjectReference, testRun.message + ': isZObjectReference' );
		assert.strictEqual( isItemId( testRun.value ), testRun.isItemId, testRun.message + ': isItemId' );
		assert.strictEqual( isPropertyId( testRun.value ), testRun.isPropertyId, testRun.message + ': isPropertyId' );
		assert.strictEqual( isLexemeId( testRun.value ), testRun.isLexemeId, testRun.message + ': isLexemeId' );
		assert.strictEqual( isLexemeFormId( testRun.value ), testRun.isLexemeFormId, testRun.message + ': isLexemeFormId' );
		assert.strictEqual( isLexemeSenseId( testRun.value ), testRun.isLexemeSenseId, testRun.message + ': isLexemeSenseId' );
		assert.strictEqual( isEntityId( testRun.value ), testRun.isEntityId, testRun.message + ': isEntityId' );
	} );
} );

QUnit.test( 'deepCopy', ( assert ) => {
	assert.strictEqual( deepEqual( deepCopy( [ null ] ), [ null ] ), true );
} );

QUnit.test( 'deepEqual', ( assert ) => {
	assert.strictEqual( deepEqual( { a: 1, b: 2 }, { a: 1, b: 2 } ), true );
	assert.strictEqual( deepEqual( { a: 1, b: 2 }, { a: 1, b: { c: 3 } } ), false );
	assert.strictEqual( deepEqual( { a: 1, b: 2 }, undefined ), false );
	assert.strictEqual( deepEqual( [ null ], [ undefined ] ), false );
	assert.strictEqual(
		deepEqual( { a: [ { a: [ null ] } ] }, { a: [ { a: [ undefined ] } ] } ),
		false );
	assert.strictEqual( deepEqual( undefined, null ), false );
	assert.strictEqual( deepEqual( undefined, undefined ), true );
} );

const mapType1 = {
	Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
	Z7K1: { Z1K1: 'Z9', Z9K1: 'Z883' },
	Z883K1: { Z1K1: 'Z9', Z9K1: 'Z6' },
	Z883K2: { Z1K1: 'Z9', Z9K1: 'Z1' }
};
const resolvedMapType = {
	Z1K1: { Z1K1: 'Z9', Z9K1: 'Z4' },
	Z4K1: mapType1,
	Z4K2: {
		Z1K1: {
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
			Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' },
			Z881K1: { Z1K1: 'Z9', Z9K1: 'Z3' }
		}
	},
	Z4K3: { Z1K1: 'Z9', Z9K1: 'Z831' }
};
const listType1 = {
	Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
	Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' },
	Z881K1: {
		Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
		Z7K1: { Z1K1: 'Z9', Z9K1: 'Z882' },
		Z882K1: { Z1K1: 'Z9', Z9K1: 'Z6' },
		Z882K2: { Z1K1: 'Z9', Z9K1: 'Z1' }
	}
};
const pairType1 = {
	Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
	Z7K1: { Z1K1: 'Z9', Z9K1: 'Z882' },
	Z882K1: { Z1K1: 'Z9', Z9K1: 'Z6' },
	Z882K2: { Z1K1: 'Z9', Z9K1: 'Z1' }
};
const error1 = {
	Z1K1: { Z1K1: 'Z9', Z9K1: 'Z5' },
	Z5K1: {
		Z1K1: { Z1K1: 'Z9', Z9K1: 'Z507' },
		Z507K1: { Z1K1: 'Z6', Z6K1: 'Could not dereference Z7K1' }
	}
};
const bogusMapType = {
	Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
	Z7K1: { Z1K1: 'Z9', Z9K1: 'Z882' },
	Z883K1: { Z1K1: 'Z9', Z9K1: 'Z6' },
	Z883K2: { Z1K1: 'Z9', Z9K1: 'Z1' }
};

QUnit.test( 'isZMap', ( assert ) => {
	const emptyZMap = { Z1K1: mapType1, K1: { Z1K1: listType1 } };
	const singletonZMap = { Z1K1: mapType1,
		K1: { Z1K1: listType1,
			K1: { Z1K1: pairType1, K1: { Z1K1: 'Z6', Z6K1: 'warnings' }, K2: { Z1K1: 'Z6', Z6K1: 'Be warned!' } },
			K2: { Z1K1: listType1 } } };
	const doubletonZMap = { Z1K1: mapType1,
		K1: { Z1K1: listType1,
			K1: { Z1K1: pairType1, K1: { Z1K1: 'Z6', Z6K1: 'warnings' }, K2: { Z1K1: 'Z6', Z6K1: 'Be warned!' } },
			K2: { Z1K1: listType1,
				K1: { Z1K1: pairType1, K1: { Z1K1: 'Z6', Z6K1: 'errors' }, K2: error1 },
				K2: { Z1K1: listType1 } } } };
	const ZMapWithResolvedType = {
		Z1K1: resolvedMapType,
		K1: {
			Z1K1: listType1
		}
	};
	const bogusZMap = { Z1K1: bogusMapType, K1: { Z1K1: listType1 } };
	assert.strictEqual( isZMap( emptyZMap ), true );
	assert.strictEqual( isZMap( singletonZMap ), true );
	assert.strictEqual( isZMap( doubletonZMap ), true );
	assert.strictEqual( isZMap( ZMapWithResolvedType ), true );
	assert.strictEqual( isZMap( bogusZMap ), false );
} );

QUnit.test( 'getZMapValue', ( assert ) => {
	const emptyZMap = { Z1K1: mapType1, K1: { Z1K1: listType1 } };
	const singletonZMap = { Z1K1: mapType1,
		K1: { Z1K1: listType1,
			K1: { Z1K1: pairType1, K1: { Z1K1: 'Z6', Z6K1: 'warnings' }, K2: { Z1K1: 'Z6', Z6K1: 'Be warned!' } },
			K2: { Z1K1: listType1 } } };
	const doubletonZMap = { Z1K1: mapType1,
		K1: { Z1K1: listType1,
			K1: { Z1K1: pairType1, K1: { Z1K1: 'Z6', Z6K1: 'warnings' }, K2: { Z1K1: 'Z6', Z6K1: 'Be warned!' } },
			K2: { Z1K1: listType1,
				K1: { Z1K1: pairType1, K1: { Z1K1: 'Z6', Z6K1: 'errors' }, K2: error1 },
				K2: { Z1K1: listType1 } } } };
	assert.strictEqual( getZMapValue( emptyZMap, { Z1K1: 'Z6', Z6K1: 'warnings' } ), undefined );
	assert.strictEqual( getZMapValue( singletonZMap, { Z1K1: 'Z6', Z6K1: 'not there' } ), undefined );
	assert.deepEqual( getZMapValue( singletonZMap, { Z1K1: 'Z6', Z6K1: 'warnings' } ), { Z1K1: 'Z6', Z6K1: 'Be warned!' } );
	assert.strictEqual( getZMapValue( doubletonZMap, { Z1K1: 'Z6', Z6K1: 'not there' } ), undefined );
	assert.deepEqual( getZMapValue( doubletonZMap, { Z1K1: 'Z6', Z6K1: 'warnings' } ), { Z1K1: 'Z6', Z6K1: 'Be warned!' } );
	assert.deepEqual( getZMapValue( doubletonZMap, { Z1K1: 'Z6', Z6K1: 'errors' } ), error1 );

	// Double-check that trying to get a ZMapValue on undefined errors.
	assert.throws(
		() => getZMapValue( undefined, { Z1K1: 'Z6', Z6K1: 'warnings' } )
	);
} );

QUnit.test( 'getZMapValue with (Benjamin) canonical form', ( assert ) => {
	const emptyZMap =
		canonicalize( { Z1K1: mapType1, K1: { Z1K1: listType1 } }, false, true ).Z22K1;
	const singletonZMap = canonicalize( { Z1K1: mapType1,
		K1: { Z1K1: listType1,
			K1: { Z1K1: pairType1, K1: { Z1K1: 'Z6', Z6K1: 'warnings' }, K2: { Z1K1: 'Z6', Z6K1: 'Be warned!' } },
			K2: { Z1K1: listType1 } } }, false, true ).Z22K1;
	const doubletonZMap = canonicalize( { Z1K1: mapType1,
		K1: { Z1K1: listType1,
			K1: { Z1K1: pairType1, K1: { Z1K1: 'Z6', Z6K1: 'warnings' }, K2: { Z1K1: 'Z6', Z6K1: 'Be warned!' } },
			K2: { Z1K1: listType1,
				K1: { Z1K1: pairType1, K1: { Z1K1: 'Z6', Z6K1: 'errors' }, K2: error1 },
				K2: { Z1K1: listType1 } } } }, false, true ).Z22K1;
	assert.strictEqual( getZMapValue( emptyZMap, 'warnings', true ), undefined );
	assert.strictEqual( getZMapValue( singletonZMap, 'not there', true ), undefined );
	assert.deepEqual( getZMapValue( singletonZMap, 'warnings', true ), 'Be warned!' );
	assert.strictEqual( getZMapValue( doubletonZMap, 'not there', true ), undefined );
	assert.deepEqual( getZMapValue( doubletonZMap, 'warnings', true ), 'Be warned!' );
	assert.deepEqual( getZMapValue( doubletonZMap, 'errors', true ),
		canonicalize( error1, false, true ).Z22K1 );

	// Double-check that trying to get a ZMapValue on undefined errors.
	assert.throws(
		() => getZMapValue( undefined, 'warnings', true )
	);
} );

QUnit.test( 'setZMapValue', ( assert ) => {
	const emptyZMap = { Z1K1: mapType1, K1: { Z1K1: listType1 } };
	const singletonZMap = { Z1K1: mapType1,
		K1: { Z1K1: listType1,
			K1: { Z1K1: pairType1, K1: { Z1K1: 'Z6', Z6K1: 'warnings' }, K2: { Z1K1: 'Z6', Z6K1: 'Be warned!' } },
			K2: { Z1K1: listType1 } } };
	const modifiedSingletonZMap = { Z1K1: mapType1,
		K1: { Z1K1: listType1,
			K1: { Z1K1: pairType1,
				K1: { Z1K1: 'Z6', Z6K1: 'warnings' },
				K2: { Z1K1: 'Z6', Z6K1: 'Relax, but this is still a warning' } },
			K2: { Z1K1: listType1 } } };

	setZMapValue( emptyZMap, { Z1K1: 'Z6', Z6K1: 'warnings' }, { Z1K1: 'Z6', Z6K1: 'Be warned!' } );
	assert.deepEqual( emptyZMap, singletonZMap );

	setZMapValue( singletonZMap, { Z1K1: 'Z6', Z6K1: 'warnings' }, { Z1K1: 'Z6', Z6K1: 'Relax, but this is still a warning' } );
	assert.deepEqual( singletonZMap, modifiedSingletonZMap );

	// Double-check that trying to set a ZMapValue on undefined errors.
	assert.throws(
		() => setZMapValue( undefined, { Z1K1: 'Z6', Z6K1: 'warnings' }, { Z1K1: 'Z6', Z6K1: 'Be warned!' } )
	);
} );

QUnit.test( 'setZMapValue with resolved type', ( assert ) => {
	// Ensure that type inference works correctly when the ZMap's type has been resolved.
	const ZMapWithResolvedType = {
		Z1K1: resolvedMapType,
		K1: {
			Z1K1: listType1
		}
	};
	const expectedZMap = {
		Z1K1: resolvedMapType,
		K1: {
			Z1K1: listType1,
			K1: {
				Z1K1: pairType1,
				K1: { Z1K1: 'Z6', Z6K1: 'warnings' },
				K2: { Z1K1: 'Z6', Z6K1: 'Be warned!' }
			},
			K2: { Z1K1: listType1 }
		}
	};
	setZMapValue(
		ZMapWithResolvedType,
		{ Z1K1: 'Z6', Z6K1: 'warnings' },
		{ Z1K1: 'Z6', Z6K1: 'Be warned!' }
	);
	assert.deepEqual( ZMapWithResolvedType, expectedZMap );
} );

QUnit.test( 'setZMapValue with callback', ( assert ) => {
	const emptyZMap = { Z1K1: mapType1, K1: { Z1K1: listType1 } };
	const beWarned = 'Be warned!';
	const dontBeWarned = 'nm, don\'t be warned';

	// Assert that callback should be called with the new tail.
	const expectedTail = {
		Z1K1: listType1,
		K1: {
			Z1K1: pairType1,
			K1: {
				Z1K1: 'Z6',
				Z6K1: 'warnings'
			},
			K2: {
				Z1K1: 'Z6',
				Z6K1: beWarned
			}
		},
		K2: {
			Z1K1: listType1
		}
	};
	const callback = function ( newTail ) {
		assert.deepEqual( newTail, expectedTail );
		const newerTail = { ...newTail };
		newerTail.K1.K2.Z6K1 = dontBeWarned;
		return newerTail;
	};

	// Assert that result is the expected map.
	const expectedZMap = {
		Z1K1: mapType1,
		K1: {
			Z1K1: listType1,
			K1: {
				Z1K1: pairType1,
				K1: {
					Z1K1: 'Z6',
					Z6K1: 'warnings'
				},
				K2: {
					Z1K1: 'Z6',
					Z6K1: dontBeWarned
				}
			},
			K2: {
				Z1K1: listType1
			}
		}
	};
	setZMapValue(
		emptyZMap,
		{ Z1K1: 'Z6', Z6K1: 'warnings' },
		{ Z1K1: 'Z6', Z6K1: beWarned },
		callback );
	assert.deepEqual( emptyZMap, expectedZMap );
} );

QUnit.test( 'setZMapValue appends to a non-empty list', ( assert ) => {
	function Z6_( someString ) {
		return {
			Z1K1: 'Z6',
			Z6K1: someString
		};
	}
	const emptyZMap = { Z1K1: mapType1, K1: { Z1K1: listType1 } };
	const warningKey = Z6_( 'warnings' );
	const reassuranceKey = Z6_( 'reassurances' );
	const beWarned = Z6_( 'Be warned!' );
	const dontBeWarned = Z6_( 'nm, don\'t be warned' );

	// Assert that result is the expected map.
	const firstExpectedZMap = {
		Z1K1: mapType1,
		K1: {
			Z1K1: listType1,
			K1: {
				Z1K1: pairType1,
				K1: warningKey,
				K2: beWarned
			},
			K2: {
				Z1K1: listType1
			}
		}
	};
	setZMapValue( emptyZMap, warningKey, beWarned );
	assert.deepEqual( emptyZMap, firstExpectedZMap );

	// Do it again.
	const secondExpectedZMap = {
		Z1K1: mapType1,
		K1: {
			Z1K1: listType1,
			K1: {
				Z1K1: pairType1,
				K1: warningKey,
				K2: beWarned
			},
			K2: {
				Z1K1: listType1,
				K1: {
					Z1K1: pairType1,
					K1: reassuranceKey,
					K2: dontBeWarned
				},
				K2: {
					Z1K1: listType1
				}
			}
		}
	};
	setZMapValue( emptyZMap, reassuranceKey, dontBeWarned );
	assert.deepEqual( emptyZMap, secondExpectedZMap );
} );

QUnit.test( 'setZMapValue/getZMapValue with Z39/Key reference as the map key', ( assert ) => {
	const keyRefKey = { Z1K1: 'Z39', Z39K1: { Z1K1: 'Z6', Z6K1: 'fake key' } };

	const initialZMap = {
		Z1K1: mapType1,
		K1: {
			Z1K1: listType1,
			K1: { Z1K1: pairType1, K1: keyRefKey, K2: { Z1K1: 'Z6', Z6K1: 'reference' } },
			K2: { Z1K1: listType1 } }
	};

	assert.deepEqual( getZMapValue( initialZMap, keyRefKey ), { Z1K1: 'Z6', Z6K1: 'reference' } );

	setZMapValue( initialZMap, keyRefKey, { Z1K1: 'Z6', Z6K1: 'new reference' } );
	assert.deepEqual(
		initialZMap,
		{
			Z1K1: mapType1,
			K1: { Z1K1: listType1,
				K1: { Z1K1: pairType1, K1: keyRefKey, K2: { Z1K1: 'Z6', Z6K1: 'new reference' } },
				K2: { Z1K1: listType1 } } }
	);

	assert.deepEqual( getZMapValue( initialZMap, keyRefKey ), { Z1K1: 'Z6', Z6K1: 'new reference' } );

} );

QUnit.test( 'setMetadataValue', ( assert ) => {
	const emptyZMap = { Z1K1: mapType1, K1: { Z1K1: listType1 } };
	const singletonZMap = { Z1K1: mapType1,
		K1: { Z1K1: listType1,
			K1: { Z1K1: pairType1, K1: { Z1K1: 'Z6', Z6K1: 'warnings' }, K2: { Z1K1: 'Z6', Z6K1: 'Be warned!' } },
			K2: { Z1K1: listType1 } } };
	const modifiedSingletonZMap = { Z1K1: mapType1,
		K1: { Z1K1: listType1,
			K1: { Z1K1: pairType1,
				K1: { Z1K1: 'Z6', Z6K1: 'warnings' },
				K2: { Z1K1: 'Z6', Z6K1: 'Relax, but this is still a warning' } },
			K2: { Z1K1: listType1 } } };
	const noEnvelope = { Z1K1: 'Z22', Z22K1: makeVoid(), Z22K2: undefined };
	const emptyEnvelope = { Z1K1: 'Z22', Z22K1: makeVoid(), Z22K2: emptyZMap };
	const singletonEnvelope = { Z1K1: 'Z22', Z22K1: makeVoid(), Z22K2: singletonZMap };
	const modifiedSingletonEnvelope = { Z1K1: 'Z22', Z22K1: makeVoid(), Z22K2: modifiedSingletonZMap };

	setMetadataValue(
		noEnvelope,
		{ Z1K1: 'Z6', Z6K1: 'warnings' }, { Z1K1: 'Z6', Z6K1: 'Be warned!' }
	);
	assert.deepEqual( noEnvelope, singletonEnvelope );

	setMetadataValue(
		emptyEnvelope,
		{ Z1K1: 'Z6', Z6K1: 'warnings' }, { Z1K1: 'Z6', Z6K1: 'Be warned!' }
	);
	assert.deepEqual( emptyEnvelope, singletonEnvelope );

	setMetadataValue(
		singletonEnvelope,
		{ Z1K1: 'Z6', Z6K1: 'warnings' }, { Z1K1: 'Z6', Z6K1: 'Relax, but this is still a warning' }
	);
	assert.deepEqual( singletonEnvelope, modifiedSingletonEnvelope );
} );

class ZWrapperForTest extends ZWrapperBase {

	constructor() {
		super();
		this.metadata = new Map();
	}

	setMetadata( key, value ) {
		this.metadata.set( key, value );
	}

}

QUnit.test( 'setMetadataValue with ZWrapper', ( assert ) => {
	const theWrapper = new ZWrapperForTest();
	setMetadataValue(
		theWrapper,
		{ Z1K1: 'Z6', Z6K1: 'key' },
		{ Z1K1: 'Z6', Z6K1: 'value' }
	);
	assert.deepEqual( theWrapper.metadata, new Map( [
		[ 'key', { Z1K1: 'Z6', Z6K1: 'value' } ]
	] ) );
} );

QUnit.test( 'setMetadataValues with ZWrapper', ( assert ) => {
	const theWrapper = new ZWrapperForTest();
	setMetadataValues( theWrapper, [
		[ { Z1K1: 'Z6', Z6K1: 'key1' }, { Z1K1: 'Z6', Z6K1: 'value1' } ],
		[ { Z1K1: 'Z6', Z6K1: 'key3' }, { Z1K1: 'Z6', Z6K1: 'value3' } ]
	] );
	assert.deepEqual( theWrapper.metadata, new Map( [
		[ 'key1', { Z1K1: 'Z6', Z6K1: 'value1' } ],
		[ 'key3', { Z1K1: 'Z6', Z6K1: 'value3' } ]
	] ) );
} );

QUnit.test( 'make* functions', ( assert ) => {
	const singletonZMapErrorsOnly = { Z1K1: mapType1,
		K1: { Z1K1: listType1,
			K1: { Z1K1: pairType1, K1: { Z1K1: 'Z6', Z6K1: 'errors' }, K2: error1 },
			K2: { Z1K1: listType1 } } };
	const mappedResultEnvelopeErrorsOnly = { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z22' }, Z22K1: { Z1K1: 'Z9', Z9K1: 'Z24' }, Z22K2: singletonZMapErrorsOnly };
	assert.deepEqual( makeTrue(), { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z40' }, Z40K1: { Z1K1: 'Z9', Z9K1: 'Z41' } } );
	assert.deepEqual( makeFalse(), { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z40' }, Z40K1: { Z1K1: 'Z9', Z9K1: 'Z42' } } );

	assert.deepEqual( makeVoid(), { Z1K1: 'Z9', Z9K1: 'Z24' } );
	assert.deepEqual( makeVoid( false ), { Z1K1: 'Z9', Z9K1: 'Z24' } );
	assert.deepEqual( makeVoid( true ), 'Z24' );

	assert.deepEqual( makeMappedResultEnvelope( null, null, true ), { Z1K1: 'Z22', Z22K1: 'Z24', Z22K2: 'Z24' } );
	assert.deepEqual( makeMappedResultEnvelope( null, null, false ), { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z22' }, Z22K1: { Z1K1: 'Z9', Z9K1: 'Z24' }, Z22K2: { Z1K1: 'Z9', Z9K1: 'Z24' } } );
	assert.deepEqual( makeMappedResultEnvelope( null, null ), { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z22' }, Z22K1: { Z1K1: 'Z9', Z9K1: 'Z24' }, Z22K2: { Z1K1: 'Z9', Z9K1: 'Z24' } } );
	assert.deepEqual( makeMappedResultEnvelope( null, singletonZMapErrorsOnly ),
		mappedResultEnvelopeErrorsOnly );
	assert.deepEqual( makeMappedResultEnvelope( null, error1 ), mappedResultEnvelopeErrorsOnly );

	assert.deepEqual( makeEmptyZMap( { Z1K1: 'Z9', Z9K1: 'Z6' }, { Z1K1: 'Z9', Z9K1: 'Z1' } ), {
		Z1K1: { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' }, Z7K1: { Z1K1: 'Z9', Z9K1: 'Z883' }, Z883K1: { Z1K1: 'Z9', Z9K1: 'Z6' }, Z883K2: { Z1K1: 'Z9', Z9K1: 'Z1' } },
		K1: { Z1K1: { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' }, Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' }, Z881K1: { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' }, Z7K1: { Z1K1: 'Z9', Z9K1: 'Z882' }, Z882K1: { Z1K1: 'Z9', Z9K1: 'Z6' }, Z882K2: { Z1K1: 'Z9', Z9K1: 'Z1' } } } }
	} );
	assert.deepEqual( makeEmptyZMap( { Z1K1: 'Z9', Z9K1: 'Z39' }, { Z1K1: 'Z9', Z9K1: 'Z5' } ), {
		Z1K1: { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' }, Z7K1: { Z1K1: 'Z9', Z9K1: 'Z883' }, Z883K1: { Z1K1: 'Z9', Z9K1: 'Z39' }, Z883K2: { Z1K1: 'Z9', Z9K1: 'Z5' } },
		K1: { Z1K1: { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' }, Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' }, Z881K1: { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' }, Z7K1: { Z1K1: 'Z9', Z9K1: 'Z882' }, Z882K1: { Z1K1: 'Z9', Z9K1: 'Z39' }, Z882K2: { Z1K1: 'Z9', Z9K1: 'Z5' } } } }
	} );
	assert.deepEqual( makeEmptyZMap( { Z1K1: 'Z9', Z9K1: 'Z6' }, listType1 ), {
		Z1K1: { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' }, Z7K1: { Z1K1: 'Z9', Z9K1: 'Z883' }, Z883K1: { Z1K1: 'Z9', Z9K1: 'Z6' }, Z883K2: listType1 },
		K1: { Z1K1: { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' }, Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' }, Z881K1: { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' }, Z7K1: { Z1K1: 'Z9', Z9K1: 'Z882' }, Z882K1: { Z1K1: 'Z9', Z9K1: 'Z6' }, Z882K2: listType1 } } }
	} );
	assert.deepEqual( makeEmptyZMap( { Z1K1: 'Z9', Z9K1: 'Z6' }, mapType1 ), {
		Z1K1: { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' }, Z7K1: { Z1K1: 'Z9', Z9K1: 'Z883' }, Z883K1: { Z1K1: 'Z9', Z9K1: 'Z6' }, Z883K2: mapType1 },
		K1: { Z1K1: { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' }, Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' }, Z881K1: { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' }, Z7K1: { Z1K1: 'Z9', Z9K1: 'Z882' }, Z882K1: { Z1K1: 'Z9', Z9K1: 'Z6' }, Z882K2: mapType1 } } }
	} );

	// Double-check that trying to make a Map from an unsupported key type errors
	assert.throws(
		() => makeEmptyZMap( { Z1K1: 'Z9', Z9K1: 'Z41' }, { Z1K1: 'Z9', Z9K1: 'Z1' } )
	);

	assert.deepEqual(
		makeEmptyZResponseEnvelopeMap(),
		{
			Z1K1: { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' }, Z7K1: { Z1K1: 'Z9', Z9K1: 'Z883' }, Z883K1: { Z1K1: 'Z9', Z9K1: 'Z6' }, Z883K2: { Z1K1: 'Z9', Z9K1: 'Z1' } },
			K1: { Z1K1: { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' }, Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' }, Z881K1: { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' }, Z7K1: { Z1K1: 'Z9', Z9K1: 'Z882' }, Z882K1: { Z1K1: 'Z9', Z9K1: 'Z6' }, Z882K2: { Z1K1: 'Z9', Z9K1: 'Z1' } } } }
		}
	);
} );

QUnit.test( 'Mutex', async ( assert ) => {
	const mutex = new Mutex();
	const eventQueue = [];

	// This function emulates a caller attempting to acquire the mutex, doing
	// some work, then releasing the mutex.
	async function f( callerId ) {
		// Wait a small, random number of ms.
		let toWait = 10 * Math.random();
		await new Promise( ( resolve ) => {
			setTimeout( () => {
				resolve();
			}, toWait );
		} );

		// Queue to acquire the lock.
		eventQueue.push( { eventType: 'QUEUE', callerId } );
		const lock = await mutex.acquire();

		// Get the lock and identify caller.
		eventQueue.push( { eventType: 'ACQUIRE_LOCK', callerId } );
		eventQueue.push( { eventType: 'IDENTITY', callerId } );

		// Wait a small, random number of ms before releasing the lock.
		toWait = 10 * Math.random();
		await new Promise( ( resolve ) => {
			setTimeout(
				() => {
					eventQueue.push( { eventType: 'RELEASE_LOCK', callerId } );
					lock.release();
					resolve();
				}, toWait
			);
		} );
	}

	const promises = [];
	for ( let i = 0; i < 100; ++i ) {
		promises.push( f( i ) );
	}
	await Promise.all( promises );
	const acquisitionOrder = [];
	let nextExpected = {};
	let expectedCaller;
	for ( const queuedEvent of eventQueue ) {
		const { eventType, callerId } = queuedEvent;
		switch ( eventType ) {
			case 'QUEUE':
				acquisitionOrder.push( callerId );
				break;
			case 'ACQUIRE_LOCK':
				expectedCaller = acquisitionOrder.shift();
				assert.deepEqual( callerId, expectedCaller );
				nextExpected = { eventType: 'IDENTITY', callerId };
				break;
			case 'IDENTITY':
				assert.deepEqual( queuedEvent, nextExpected );
				nextExpected = { eventType: 'RELEASE_LOCK', callerId };
				break;
			case 'RELEASE_LOCK':
				assert.deepEqual( queuedEvent, nextExpected );
				nextExpected = {};
				break;
		}
	}

	// All acquired locks should also have been released.
	assert.deepEqual( acquisitionOrder, [] );
} );

QUnit.test( 'isVoid', ( assert ) => {
	assert.strictEqual( isVoid( [] ), false );
	assert.strictEqual( isVoid( '' ), false );
	assert.strictEqual( isVoid( 'z24' ), false );
	assert.strictEqual( isVoid( 'Z24' ), true );
	assert.strictEqual( isVoid( {} ), false );
	assert.strictEqual( isVoid( { Z1K1: '' } ), false );
	assert.strictEqual( isVoid( { Z1K1: 'Z9' } ), false );
	assert.strictEqual( isVoid( { Z1K1: 'Z9', Z9K1: '' } ), false );
	assert.strictEqual( isVoid( { Z1K1: 'Z9', Z9K1: 'z24' } ), false );
	assert.strictEqual( isVoid( { Z1K1: 'Z9', Z9K1: 'Z24' } ), true );
	assert.strictEqual( isVoid( { Z1K1: 'Z21' } ), true );
	assert.strictEqual( isVoid( { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z21' } } ), true );
} );

QUnit.test( 'getError', ( assert ) => {
	const singletonZMapErrorsOnly = { Z1K1: mapType1,
		K1: { Z1K1: listType1,
			K1: { Z1K1: pairType1, K1: { Z1K1: 'Z6', Z6K1: 'errors' }, K2: error1 },
			K2: { Z1K1: listType1 } } };
	const mappedResultEnvelopeErrorsOnly = { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z22' }, Z22K1: { Z1K1: 'Z9', Z9K1: 'Z24' }, Z22K2: singletonZMapErrorsOnly };
	const emptyZMap = { Z1K1: mapType1, K1: { Z1K1: listType1 } };
	const mappedResultEnvelopeNoEntries = { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z22' }, Z22K1: { Z1K1: 'Z9', Z9K1: 'Z24' }, Z22K2: emptyZMap };
	const singletonZMap = { Z1K1: mapType1,
		K1: { Z1K1: listType1,
			K1: { Z1K1: pairType1, K1: { Z1K1: 'Z6', Z6K1: 'warnings' }, K2: { Z1K1: 'Z6', Z6K1: 'Be warned!' } },
			K2: { Z1K1: listType1 } } };
	const mappedResultEnvelopeWithWarnings = { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z22' }, Z22K1: { Z1K1: 'Z9', Z9K1: 'Z24' }, Z22K2: singletonZMap };
	const doubletonZMap = { Z1K1: mapType1,
		K1: { Z1K1: listType1,
			K1: { Z1K1: pairType1, K1: { Z1K1: 'Z6', Z6K1: 'warnings' }, K2: { Z1K1: 'Z6', Z6K1: 'Be warned!' } },
			K2: { Z1K1: listType1,
				K1: { Z1K1: pairType1, K1: { Z1K1: 'Z6', Z6K1: 'errors' }, K2: error1 },
				K2: { Z1K1: listType1 } } } };
	const mappedResultEnvelope2Entries = { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z22' }, Z22K1: { Z1K1: 'Z9', Z9K1: 'Z24' }, Z22K2: doubletonZMap };
	assert.strictEqual( getError( mappedResultEnvelopeErrorsOnly ), error1 );
	assert.strictEqual( getError( mappedResultEnvelope2Entries ), error1 );
	assert.deepEqual( getError( mappedResultEnvelopeNoEntries ), { Z1K1: 'Z9', Z9K1: 'Z24' } );
	assert.deepEqual( getError( mappedResultEnvelopeWithWarnings ), { Z1K1: 'Z9', Z9K1: 'Z24' } );
} );

QUnit.test( 'getError with canonical items', ( assert ) => {
	let map = makeEmptyZResponseEnvelopeMap();
	setZMapValue( map, 'errors', error1 );
	map = canonicalize( map ).Z22K1;

	const mappedResultEnvelopeCanonicalErrorsOnly = makeMappedResultEnvelope( null, map, true );

	const canonicalError = canonicalize( error1 ).Z22K1;

	assert.deepEqual(
		getError( mappedResultEnvelopeCanonicalErrorsOnly ), canonicalError );

} );

QUnit.test( 'inferType', ( assert ) => {
	assert.deepEqual( inferType( '' ), 'Z6' );
	assert.deepEqual( inferType( 'Z' ), 'Z6' );
	assert.deepEqual( inferType( 'Z0' ), 'Z6' );
	assert.deepEqual( inferType( 'Z1K1' ), 'Z6' );
	assert.deepEqual( inferType( 'Z1' ), 'Z9' );
	assert.deepEqual( inferType( [ 'Z1' ] ), 'LIST' );
	assert.deepEqual( inferType( { Z1K1: 'Z1' } ), 'Z1' );
} );

QUnit.test( 'kidFromGlobalKey', ( assert ) => {
	assert.strictEqual( kidFromGlobalKey( 'Z1K1' ), 'K1' );
	assert.strictEqual( kidFromGlobalKey( 'Z1K123' ), 'K123' );
	assert.strictEqual( kidFromGlobalKey( 'Z1K1234567890' ), 'K1234567890' );
	assert.strictEqual( kidFromGlobalKey( 'Z123K1' ), 'K1' );
	assert.strictEqual( kidFromGlobalKey( 'Z123K123' ), 'K123' );
	assert.strictEqual( kidFromGlobalKey( 'Z123K1234567890' ), 'K1234567890' );
	assert.strictEqual( kidFromGlobalKey( 'Z1234567890K1' ), 'K1' );
	assert.strictEqual( kidFromGlobalKey( 'Z1234567890K123' ), 'K123' );
	assert.strictEqual( kidFromGlobalKey( 'Z1234567890K1234567890' ), 'K1234567890' );
} );

QUnit.test( 'inferItemType', ( assert ) => {
	const singletonZ1Array = [
		{
			Z1K1: 'Z9',
			Z9K1: 'Z1'
		}
	];
	assert.deepEqual(
		inferItemType( singletonZ1Array ),
		{ Z1K1: 'Z9', Z9K1: 'Z1' },
		'Basic Z1 list is identified as such in normal form too'
	);

	assert.deepEqual(
		inferItemType( [ { Z1K1: 'Z6', Z6K1: 'Test' } ] ),
		{ Z1K1: 'Z9', Z9K1: 'Z6' },
		'Basic Z6 list is identified as such'
	);

	assert.deepEqual(
		inferItemType( [ { Z1K1: 'Z6', Z6K1: 'Test' }, { Z1K1: 'Z6', Z6K1: 'Second test' } ] ),
		{ Z1K1: 'Z9', Z9K1: 'Z6' },
		'Z6 list of length two is identified as such'
	);

	assert.deepEqual(
		inferItemType( [
			{ Z1K1: 'Z6', Z6K1: 'Test' },
			{ Z1K1: 'Z6', Z6K1: 'Second test' },
			{
				Z1K1: { Z1K1: 'Z9', Z9K1: 'Z40' },
				Z40K1: { Z1K1: 'Z9', Z9K1: 'Z42' }
			}
		] ),
		{ Z1K1: 'Z9', Z9K1: 'Z1' },
		'List with mixed types falls back to Z1'
	);

	assert.deepEqual(
		inferItemType( [ { Z1K1: 'Z9', Z9K1: 'Z12345' } ] ),
		{ Z1K1: 'Z9', Z9K1: 'Z1' },
		'Basic Z9 list falls back as a Z1 as it is a resolver type'
	);

} );

function Z9_( ZID ) {
	return { Z1K1: 'Z9', Z9K1: ZID };
}

const someArgumentReference = {
	Z1K1: Z9_( 'Z18' ),
	Z18K1: Z9_( 'Z30000K1' )
};

const someFalsehood = {
	Z1K1: Z9_( 'Z40' ),
	Z40K1: Z9_( 'Z42' )
};
const someTruth = {
	Z1K1: Z9_( 'Z40' ),
	Z40K1: Z9_( 'Z41' )
};

const someReference = Z9_( 'Z30001' );
const badReference = Z9_( 'Z100' );
badReference.Z1K1 = 'Z 9';
const builtInReference = Z9_( 'Z39' );

const someFunctionCall = {
	Z1K1: Z9_( 'Z7' ),
	Z7K1: Z9_( 'Z30002' )
};

const someEnvelope = {
	Z1K1: Z9_( 'Z22' ),
	Z22K1: Z9_( 'Z24' ),
	Z22K2: Z9_( 'Z24' )
};

const typeWithFunctionCallIdentity = {
	Z1K1: Z9_( 'Z4' ),
	Z4K1: someFunctionCall,
	Z4K2: convertArrayToKnownTypedList( [], Z9_( 'Z3' ) ),
	Z4K3: Z9_( 'Z30003' )
};

const typeWithBuiltInReferenceIdentity = { ...typeWithFunctionCallIdentity };
typeWithBuiltInReferenceIdentity.Z4K1 = builtInReference;

const typeWithReferenceIdentity = { ...typeWithFunctionCallIdentity };
typeWithReferenceIdentity.Z4K1 = someReference;

const typeWithTypeWithReferenceIdentityIdentity = { ...typeWithBuiltInReferenceIdentity };
typeWithTypeWithReferenceIdentityIdentity.Z4K1 = typeWithReferenceIdentity;

QUnit.test( 'isZArgumentReference', ( assert ) => {
	assert.true( isZArgumentReference( someArgumentReference ) );
	assert.false( isZArgumentReference( someFunctionCall ) );
	assert.false( isZArgumentReference( someReference ) );
	assert.false( isZArgumentReference( typeWithFunctionCallIdentity ) );
} );

QUnit.test( 'isZBoolean', ( assert ) => {
	assert.true( isZBoolean( someFalsehood ) );
	assert.true( isZBoolean( someTruth ) );
	assert.true( isZBoolean( someFalsehood.Z40K1 ) );
	assert.true( isZBoolean( someTruth.Z40K1 ) );
	assert.false( isZBoolean( someFunctionCall ) );
	assert.false( isZBoolean( someReference ) );
	assert.false( isZBoolean( typeWithFunctionCallIdentity ) );
} );

QUnit.test( 'isZEnvelope', ( assert ) => {
	assert.true( isZEnvelope( someEnvelope ) );
	assert.false( isZEnvelope( someFunctionCall ) );
	assert.false( isZEnvelope( someReference ) );
	assert.false( isZEnvelope( typeWithFunctionCallIdentity ) );
} );

QUnit.test( 'isZFunction', ( assert ) => {
	assert.false( isZFunction( someArgumentReference ) );
	assert.false( isZFunction( someFunctionCall ) );
	assert.false( isZFunction( someReference ) );
	assert.false( isZFunction( typeWithFunctionCallIdentity ) );
	const someFunction = {
		Z1K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z8'
		},
		Z8K1: {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z40'
			},
			Z40K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z41'
			}
		},
		Z8K2: {
			Z1K1: 'Z9',
			Z9K1: 'Z10000'
		},
		Z8K3: {
			Z1K1: 'Z9',
			Z9K1: 'Z10000'
		},
		Z8K4: {
			Z1K1: 'Z9',
			Z9K1: 'Z10000'
		},
		Z8K5: {
			Z1K1: 'Z9',
			Z9K1: 'Z10000'
		}
	};
	assert.true( isZFunction( someFunction ) );
} );

QUnit.test( 'isZFunctionCall', ( assert ) => {
	assert.false( isZFunctionCall( someArgumentReference ) );
	assert.true( isZFunctionCall( someFunctionCall ) );
	assert.false( isZFunctionCall( someReference ) );
	assert.false( isZFunctionCall( typeWithFunctionCallIdentity ) );
} );

QUnit.test( 'isZReference', ( assert ) => {
	assert.false( isZReference( someArgumentReference ) );
	assert.false( isZReference( someFunctionCall ) );
	assert.true( isZReference( someReference ) );
	assert.false( isZReference( badReference ) );
	assert.false( isZReference( typeWithFunctionCallIdentity ) );
} );

QUnit.test( 'isZString', ( assert ) => {
	const Z6_ = ( someString ) => ( {
		Z1K1: 'Z6',
		Z6K1: someString
	} );
	const goodString = Z6_( 'good' );
	const badString = Z6_( 'good' );
	badString.Z1K1 = 'Z 6';
	assert.false( isZString( someArgumentReference ) );
	assert.false( isZString( someFunctionCall ) );
	assert.false( isZString( someReference ) );
	assert.false( isZString( badString ) );
	assert.false( isZString( badString.Z6K1 ) );
	assert.true( isZString( goodString ) );
	assert.false( isZString( goodString.Z6K1 ) );
} );

QUnit.test( 'isZType', ( assert ) => {
	assert.false( isZType( someArgumentReference ) );
	assert.false( isZType( someFunctionCall ) );
	assert.false( isZType( someReference ) );
	assert.true( isZType( typeWithFunctionCallIdentity ) );
} );

QUnit.test( 'isMemberOfDangerTrio', ( assert ) => {
	assert.true( isMemberOfDangerTrio( someArgumentReference ) );
	assert.true( isMemberOfDangerTrio( someReference ) );
	assert.true( isMemberOfDangerTrio( someFunctionCall ) );
	assert.false( isMemberOfDangerTrio( typeWithFunctionCallIdentity ) );
	assert.false( isMemberOfDangerTrio( typeWithReferenceIdentity ) );
} );

QUnit.test( 'findBooleanIdentity', ( assert ) => {
	assert.strictEqual( findBooleanIdentity( someFalsehood ), someFalsehood.Z40K1 );
	assert.strictEqual(
		findBooleanIdentity( {
			Z1K1: Z9_( 'Z40' ),
			Z40K1: someFalsehood
		} ),
		someFalsehood.Z40K1
	);
	assert.strictEqual( findBooleanIdentity( someTruth ), someTruth.Z40K1 );
	assert.strictEqual(
		findBooleanIdentity( {
			Z1K1: Z9_( 'Z40' ),
			Z40K1: someTruth
		} ),
		someTruth.Z40K1
	);
} );

QUnit.test( 'findTypeDefinition', ( assert ) => {
	assert.strictEqual( findTypeDefinition( someFunctionCall ), null );
	assert.strictEqual( findTypeDefinition( someReference ), null );
	// Special case: called with a built-in reference, findTypeDefinition returns the Reference.
	assert.strictEqual( findTypeDefinition( builtInReference ), builtInReference );
	assert.strictEqual(
		findTypeDefinition( typeWithFunctionCallIdentity ), typeWithFunctionCallIdentity );
	assert.strictEqual(
		findTypeDefinition( typeWithBuiltInReferenceIdentity ), typeWithBuiltInReferenceIdentity );
	assert.strictEqual(
		findTypeDefinition( typeWithReferenceIdentity ), typeWithReferenceIdentity );
	assert.strictEqual(
		findTypeDefinition( typeWithTypeWithReferenceIdentityIdentity ),
		typeWithReferenceIdentity );
} );

QUnit.test( 'findTypeIdentity', ( assert ) => {
	assert.strictEqual( findTypeIdentity( typeWithFunctionCallIdentity ), someFunctionCall );
	assert.strictEqual( findTypeIdentity( typeWithBuiltInReferenceIdentity ), builtInReference );
	assert.strictEqual( findTypeIdentity( typeWithReferenceIdentity ), someReference );
	assert.strictEqual(
		findTypeIdentity( typeWithTypeWithReferenceIdentityIdentity ), someReference );
} );

QUnit.test( 'stableStringify', ( assert ) => {
	const someObject = {
		f: 'a string!',
		b: 1,
		a: 2,
		c: [
			{
				e: false,
				d: null
			}
		]
	};
	const someStringified = stableStringify( someObject );
	const otherObject = {
		a: 2,
		b: 1,
		c: [
			{
				d: null,
				e: false
			}
		],
		f: 'a string!'
	};
	const otherStringified = stableStringify( otherObject );
	assert.deepEqual( someStringified, otherStringified );
	assert.deepEqual( JSON.parse( someStringified ), someObject );
	assert.deepEqual( JSON.parse( someStringified ), otherObject );
	assert.deepEqual( JSON.parse( otherStringified ), someObject );
	assert.deepEqual( JSON.parse( otherStringified ), otherObject );

	const differentObject = {
		f: 'a string!',
		b: 1,
		a: 2,
		c: [
			{
				e: false
			}
		]
	};
	const differentStringified = stableStringify( differentObject );
	assert.notDeepEqual( someStringified, differentStringified );
	assert.deepEqual( JSON.parse( differentStringified ), differentObject );

	const differenterObject = {
		f: 'a string!',
		b: 1,
		a: 2,
		c: [
			{
				e: 0,
				d: null
			}
		]
	};
	const differenterStringified = stableStringify( differenterObject );
	assert.notDeepEqual( someStringified, differenterStringified );
	assert.deepEqual( JSON.parse( differenterStringified ), differenterObject );
} );

QUnit.test( 'findTypeConverterToCodeIdentity for non-Z46', ( assert ) => {
	assert.strictEqual( findTypeConverterToCodeIdentity( someFunctionCall ), null );
} );

const aToTypeConverter = {
	Z1K1: {
		Z1K1: 'Z9',
		Z9K1: 'Z46'
	},
	Z46K1: {
		Z1K1: 'Z9',
		Z9K1: 'Z10000'
	}
};
const anotherToTypeConverter = { ...aToTypeConverter };
anotherToTypeConverter.Z46K1 = { ...aToTypeConverter };

QUnit.test( 'findTypeConverterToCodeIdentity: one nesting level', ( assert ) => {
	assert.strictEqual(
		findTypeConverterToCodeIdentity( aToTypeConverter ),
		aToTypeConverter.Z46K1
	);
} );

QUnit.test( 'findToTypeConverterToCodeIdentity: two nesting levels', ( assert ) => {
	assert.strictEqual(
		findTypeConverterToCodeIdentity( anotherToTypeConverter ),
		aToTypeConverter.Z46K1
	);
} );

QUnit.test( 'findTypeConverterFromCodeIdentity for non-Z64', ( assert ) => {
	assert.strictEqual( findTypeConverterFromCodeIdentity( someFunctionCall ), null );
} );

const aFromTypeConverter = {
	Z1K1: {
		Z1K1: 'Z9',
		Z9K1: 'Z64'
	},
	Z64K1: {
		Z1K1: 'Z9',
		Z9K1: 'Z10000'
	}
};
const anotherFromTypeConverter = { ...aFromTypeConverter };
anotherFromTypeConverter.Z64K1 = { ...aFromTypeConverter };

QUnit.test( 'findTypeConverterFromCodeIdentity: one nesting level', ( assert ) => {
	assert.strictEqual(
		findTypeConverterFromCodeIdentity( aFromTypeConverter ),
		aFromTypeConverter.Z64K1
	);
} );

QUnit.test( 'findTypeConverterFromCodeIdentity: two nesting levels', ( assert ) => {
	assert.strictEqual(
		findTypeConverterFromCodeIdentity( anotherFromTypeConverter ),
		aFromTypeConverter.Z64K1
	);
} );

QUnit.test( 'createZObjectKey with Z7K1 & Z4s as references', ( assert ) => {
	const Z7 = {
		Z1K1: 'Z7',
		Z7K1: 'Z420',
		Z420K1: 'Z14',
		Z420K2: 'Z17'
	};
	assert.deepEqual( createZObjectKey( normalize( Z7 ).Z22K1 ), ':Z1K1|Z9K1|Z7,Z420K1|Z9K1|Z14,Z420K2|Z9K1|Z17,Z7K1|Z9K1|Z420,,' );
} );

QUnit.test( 'createZObjectKey with Z7K1 & Z4s as reified types', ( assert ) => {
	const Z4 = {
		Z1K1: 'Z4',
		Z4K1: {
			Z1K1: 'Z7',
			Z7K1: 'Z420',
			Z420K1: {
				Z1K1: 'Z4',
				Z4K1: 'Z14',
				Z4K2: [
					'Z3'
				],
				Z4K3: 'Z400'
			},
			Z420K2: {
				Z1K1: 'Z4',
				Z4K1: 'Z17',
				Z4K2: [
					'Z3'
				],
				Z4K3: 'Z400'
			}
		},
		Z4K2: [
			'Z3'
		],
		Z4K3: {
			Z1K1: 'Z9',
			Z9K1: 'Z402'
		}
	};
	assert.deepEqual( createZObjectKey( normalize( Z4 ).Z22K1 ), ':Z1K1|Z9K1|Z7,Z420K1|Z9K1|Z14,Z420K2|Z9K1|Z17,Z7K1|Z9K1|Z420,,' );
} );

QUnit.test( 'createZObjectKey for a user-defined type', ( assert ) => {
	const Z4 = {
		Z1K1: 'Z4',
		Z4K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z400'
		},
		Z4K2: [
			'Z3',
			{
				Z1K1: 'Z3',
				Z3K1: 'Z6',
				Z3K2: {
					Z1K1: 'Z6',
					Z6K1: 'K1'
				},
				Z3K3: {
					Z1K1: 'Z9',
					Z9K1: 'Z400'
				}
			},
			{
				Z1K1: 'Z3',
				Z3K1: {
					Z1K1: 'Z7',
					Z7K1: 'Z931',
					Z931K1: 'Z6',
					Z931K2: 'Z12'
				},
				Z3K2: {
					Z1K1: 'Z6',
					Z6K1: 'K2'
				},
				Z3K3: {
					Z1K1: 'Z9',
					Z9K1: 'Z400'
				}
			}
		],
		Z4K3: {
			Z1K1: 'Z9',
			Z9K1: 'Z401'
		}
	};
	assert.deepEqual( createZObjectKey( normalize( Z4 ).Z22K1 ), ':K1|Z9K1|Z6,K2|:Z1K1|Z9K1|Z7,Z7K1|Z9K1|Z931,Z931K1|Z9K1|Z6,Z931K2|Z9K1|Z12,,,Z1K1|Z9K1|Z4,,' );
} );

QUnit.test( 'createZObjectKey with generic type parameterized by object', ( assert ) => {
	const Z1 = {
		Z1K1: 'Z4',
		Z4K1: {
			Z1K1: 'Z7',
			Z7K1: 'Z420',
			Z420K1: {
				Z1K1: 'Z6',
				Z6K1: 'Smörgåsbord'
			},
			Z420K2: {
				Z1K1: 'Z4',
				Z4K1: 'Z17',
				Z4K2: [
					'Z3'
				],
				Z4K3: 'Z400'
			}
		},
		Z4K2: [
			'Z3'
		],
		Z4K3: {
			Z1K1: 'Z9',
			Z9K1: 'Z401'
		}
	};
	assert.deepEqual( createZObjectKey( normalize( Z1 ).Z22K1 ), ':Z1K1|Z9K1|Z7,Z420K1|Z6K1|Smörgåsbord,Z420K2|Z9K1|Z17,Z7K1|Z9K1|Z420,,' );
} );

QUnit.test( 'createZObjectKey with invalid object', ( assert ) => {
	const invalidZObject = {
		Hello: 'Molly',
		This: 'is Louis, Molly'
	};
	let failedResponse;
	try {
		failedResponse = createZObjectKey( invalidZObject );
	} catch ( error ) {
		// These are implementation details and not guaranteed-stable
		assert.strictEqual( error.name, 'Error' );
		assert.strictEqual( error.message, 'Invalid ZObject input' );
		assert.strictEqual( error.errorZObjectPayload.Z1K1.Z9K1, 'Z99' );
		// Note that the top-level object isn't visible here;
		// currently, a key can be created for a ZObject that is invalid
		// because its keys do not cleave to the correct format.
		assert.strictEqual( error.errorZObjectPayload.Z99K1, 'Molly' );
	}
	// If we've set a response then something went wrong in going wrong.
	assert.deepEqual( failedResponse, undefined );
} );

QUnit.test( 'createZObjectKey for Z6', ( assert ) => {
	const key = createZObjectKey( {
		Z1K1: 'Z6',
		Z6K1: 'Smörgåsbord'
	} );
	assert.deepEqual( key, 'Z6K1|Smörgåsbord' );
} );

QUnit.test( 'createZObjectKey for generic type', ( assert ) => {
	const Z1 = {
		Z1K1: 'Z4',
		Z4K1: {
			Z1K1: 'Z7',
			Z7K1: 'Z420',
			Z420K1: {
				Z1K1: 'Z6',
				Z6K1: 'Smörgåsbord'
			},
			Z420K2: {
				Z1K1: 'Z4',
				Z4K1: 'Z17',
				Z4K2: [
					'Z3'
				],
				Z4K3: 'Z400'
			}
		},
		Z4K2: [
			'Z3'
		],
		Z4K3: {
			Z1K1: 'Z9',
			Z9K1: 'Z401'
		}
	};
	const key = createZObjectKey( normalize( Z1 ).Z22K1 );
	assert.deepEqual( key, ':Z1K1|Z9K1|Z7,Z420K1|Z6K1|Smörgåsbord,Z420K2|Z9K1|Z17,Z7K1|Z9K1|Z420,,' );
} );

QUnit.test( 'createZObjectKey for Z9', ( assert ) => {
	const key = createZObjectKey( {
		Z1K1: 'Z9',
		Z9K1: 'Z9'
	} );
	assert.deepEqual( key, 'Z9K1|Z9' );
} );

QUnit.test( 'createZObjectKey for Z40', ( assert ) => {
	const key = createZObjectKey( {
		Z1K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z40'
		},
		Z40K1: {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z40'
			},
			Z40K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z41'
			}
		}
	} );
	assert.deepEqual( key, 'Z40K1|Z41' );
} );

QUnit.test( 'createZObjectKey for Z8', ( assert ) => {
	const key = createZObjectKey( {
		Z1K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z8'
		},
		Z8K1: {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z40'
			},
			Z40K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z41'
			}
		},
		Z8K2: {
			Z1K1: 'Z9',
			Z9K1: 'Z10000'
		},
		Z8K3: {
			Z1K1: 'Z9',
			Z9K1: 'Z10000'
		},
		Z8K4: {
			Z1K1: 'Z9',
			Z9K1: 'Z10000'
		},
		Z8K5: {
			Z1K1: 'Z9',
			Z9K1: 'Z10000'
		}
	} );
	assert.deepEqual( key, 'Z9K1|Z10000' );
} );

QUnit.test( 'createZObjectKey for weird Z8', ( assert ) => {
	const key = createZObjectKey( {
		Z1K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z8'
		},
		Z8K1: {
			Z1K1: {
				Z1K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z7'
				},
				Z7K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z881'
				},
				Z881K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z17'
				}
			}
		},
		Z8K2: {
			Z1K1: 'Z9',
			Z9K1: 'Z10000'
		},
		Z8K3: {
			Z1K1: 'Z9',
			Z9K1: 'Z10000'
		},
		Z8K4: {
			Z1K1: 'Z9',
			Z9K1: 'Z10000'
		},
		Z8K5: {
			Z1K1: 'Z6',
			Z6K1: 'Z10000'
		}
	} );
	assert.deepEqual(
		key,
		':Z1K1|Z9K1|Z8,Z8K1|:Z1K1|:Z1K1|Z9K1|Z7,Z7K1|Z9K1|Z881,Z881K1|Z9K1|Z17,,,,,Z8K2|Z9K1|Z10000,Z8K3|,Z8K4|,Z8K5|,,'
	);
} );

QUnit.test( 'createZObjectKey for weird Z7', ( assert ) => {
	const key = createZObjectKey( {
		Z1K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z7'
		},
		Z7K1: {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z8'
			},
			Z8K1: {
				Z1K1: {
					Z1K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z7'
					},
					Z7K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z881'
					},
					Z881K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z17'
					}
				}
			},
			Z8K2: {
				Z1K1: 'Z9',
				Z9K1: 'Z10000'
			},
			Z8K3: {
				Z1K1: 'Z9',
				Z9K1: 'Z10000'
			},
			Z8K4: {
				Z1K1: 'Z9',
				Z9K1: 'Z10000'
			},
			Z8K5: {
				Z1K1: 'Z6',
				Z6K1: 'Z10000'
			}
		},
		K1: {
			Z1K1: 'Z6',
			Z6K1: 'theargument'
		}
	} );
	assert.deepEqual(
		key,
		':K1|Z6K1|theargument,Z1K1|Z9K1|Z7,Z7K1|:Z1K1|Z9K1|Z8,Z8K1|:Z1K1|:Z1K1|Z9K1|Z7,Z7K1|Z9K1|Z881,Z881K1|Z9K1|Z17,,,,,Z8K2|Z9K1|Z10000,Z8K3|,Z8K4|,Z8K5|,,,,'
	);
} );

QUnit.test( 'createZObjectKey for Z99', ( assert ) => {
	const key = createZObjectKey( {
		Z1K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z99'
		},
		Z99K1: {
			thisIsJson: true
		}
	} );
	assert.deepEqual( key, ':Z1K1|Z9K1|Z99,Z99K1|{"thisIsJson":true},,' );
} );

{
	const goodObject = {
		Z1K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z10000'
		},
		Z10000K1: {
			Z1K1: 'Z6',
			Z6K1: 'Z6' // haaaaa
		},
		Z10000K2: {
			Z1K1: {
				Z1K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z7'
				},
				Z7K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z10001'
				},
				K1: {
					Z1K1: 'Z6',
					Z6K1: 'Z7' // HAAAA
				}
			},
			K2: {
				Z1K1: 'Z6',
				Z6K1: 'HA'
			}
		}
	};

	QUnit.test( 'findFirstBadPath for good object is null', ( assert ) => {
		const thePath = findFirstBadPath( goodObject );
		assert.deepEqual( thePath, null );
	} );

	QUnit.test( 'isZObject for good object', ( assert ) => {
		const isGoodObject = isZObject( goodObject );
		assert.deepEqual( isGoodObject, true );
	} );

	// Make a cheap clone and give it a bad member.
	const badObjectBadString = JSON.parse( JSON.stringify( goodObject ) );
	badObjectBadString.Z10000K2.K2.Z6K1 = 2;

	QUnit.test( 'findFirstBadPath for bad member object', ( assert ) => {
		const thePath = findFirstBadPath( badObjectBadString );
		assert.deepEqual( thePath, 'Z10000K2.K2.Z1K1' );
	} );

	QUnit.test( 'isZObject for bad member object', ( assert ) => {
		const isGoodObject = isZObject( badObjectBadString );
		assert.deepEqual( isGoodObject, false );
	} );

	// Make a cheap clone and give it a bad key.
	const badObjectBadKey = JSON.parse( JSON.stringify( goodObject ) );
	badObjectBadKey.Z10000K2.Z1K1.AAAA = { Z1K1: 'Z6', Z6K1: 'Z6' };

	QUnit.test( 'findFirstBadPath for bad key object', ( assert ) => {
		const thePath = findFirstBadPath( badObjectBadKey );
		assert.deepEqual( thePath, 'Z10000K2.Z1K1.AAAA' );
	} );

	QUnit.test( 'isZObject for bad key object', ( assert ) => {
		const isGoodObject = isZObject( badObjectBadKey );
		assert.deepEqual( isGoodObject, false );
	} );

}
