'use strict';

const fs = require( 'fs' );
const path = require( 'path' );
const {
	convertFormattedRequestToVersionedBinary,
	convertWrappedZObjectToVersionedBinary,
	convertZObjectToBinary,
	getWrappedZObjectFromVersionedBinary,
	getZObjectFromBinary,
	convertNormalForBinaryFormat,
	recoverNormalFromBinaryFormat
} = require( '../../../src/binaryFormatter.js' );
const { readJSON } = require( '../../../src/fileUtils.js' );

QUnit.module( 'Binary format' );

const hugeFunctionCall = readJSON( path.join( 'test_data', 'function_call', 'too_big.json' ) );

// Sense check: binary form should be smaller than the original object.
QUnit.test( 'binaries should be small', ( assert ) => {
	const binaryForm = convertZObjectToBinary( hugeFunctionCall, '0.0.1' );
	const compressedSize = binaryForm.length;
	const uncompressedSize = JSON.stringify( hugeFunctionCall ).length;
	// For objects of this size, we should achieve more than 2.5x compression.
	assert.true( compressedSize * 2.5 < uncompressedSize );
} );

// Idempotence check: conversion to binary and back should produce the
// original object.
QUnit.test( 'Binary format 0.0.1 should be invertible', ( assert ) => {
	const binaryForm = convertZObjectToBinary( hugeFunctionCall, '0.0.1' );
	const textForm = getZObjectFromBinary( binaryForm, '0.0.1' );
	assert.deepEqual( textForm, hugeFunctionCall );
} );

QUnit.test( 'recoverNormalFromBinaryFormat should throw error on bad input', ( assert ) => {
	const degenerate = { 'ztypes.Z2': 'impossible' };
	assert.throws( () => {
		recoverNormalFromBinaryFormat( degenerate );
	} );
} );

QUnit.test( 'convertNormalForBinaryFormat should throw an error on bad string input', ( assert ) => {
	const degenerate = { Z6K1: 'DEGENERATE' };
	assert.throws( () => {
		convertNormalForBinaryFormat( degenerate );
	} );
} );

QUnit.test( 'convertNormalForBinaryFormat should throw an error on bad Boolean input', ( assert ) => {
	const degenerate = { Z1K1: false };
	assert.throws( () => {
		convertNormalForBinaryFormat( degenerate );
	} );
} );

QUnit.test( 'convertNormalForBinaryFormat should throw an error on bad array input', ( assert ) => {
	const degenerate = [ { Z1K1: 'Z6', Z6K1: 'array!' } ];
	assert.throws( () => {
		convertNormalForBinaryFormat( degenerate );
	} );
} );

QUnit.test( 'convertNormalForBinaryFormat should throw an error on bad numeral input', ( assert ) => {
	const degenerate = 5;
	assert.throws( () => {
		convertNormalForBinaryFormat( degenerate );
	} );
} );

QUnit.test( 'Binary format 0.0.2 should be invertible', ( assert ) => {
	const original = {
		reentrant: true,
		zobject: hugeFunctionCall
	};
	const binaryForm = convertZObjectToBinary( original, '0.0.2' );
	const textForm = getZObjectFromBinary( binaryForm, '0.0.2' );
	assert.deepEqual( textForm, original );
} );

QUnit.test( 'wrapped binary conversion should be invertible with version 0.0.1', ( assert ) => {
	const original = hugeFunctionCall;
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '0.0.1' );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	assert.deepEqual( textForm, original );
} );

QUnit.test( 'wrapped binary conversion should be invertible with version 0.0.2', ( assert ) => {
	const original = {
		reentrant: true,
		zobject: hugeFunctionCall
	};
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '0.0.2' );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	assert.deepEqual( textForm, original );
} );

QUnit.test( 'convertNormalForBinaryFormat/recoverNormalFromBinaryFormat: quote', ( assert ) => {
	// FIXME: Why does validatesAsQuote() require normal form but the others require canonical?
	const quote = { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z99' }, Z99K1: 'Hello, this is dog' };
	const expected = { 'ztypes.Z99': { Z99K1: '"Hello, this is dog"' } };
	const normalFormQuote = convertNormalForBinaryFormat( quote );
	assert.deepEqual( normalFormQuote, expected );

	const recoveredForm = recoverNormalFromBinaryFormat( normalFormQuote );
	assert.deepEqual( recoveredForm, quote );
} );

QUnit.test( 'convertNormalForBinaryFormat/recoverNormalFromBinaryFormat: reference', ( assert ) => {
	const reference = { Z1K1: 'Z9', Z9K1: 'Z1' };
	const expected = { 'ztypes.Z9': { Z9K1: 'Z1' } };
	const normalFormReference = convertNormalForBinaryFormat( reference );
	assert.deepEqual( normalFormReference, expected );

	const recoveredForm = recoverNormalFromBinaryFormat( normalFormReference );
	assert.deepEqual( recoveredForm, reference );
} );

QUnit.test( 'convertNormalForBinaryFormat/recoverNormalFromBinaryFormat: string', ( assert ) => {
	const string = { Z1K1: 'Z6', Z6K1: 'String value' };
	const expected = { 'ztypes.Z6': { Z6K1: 'String value' } };
	const normalFormString = convertNormalForBinaryFormat( string );
	assert.deepEqual( normalFormString, expected );

	const recoveredForm = recoverNormalFromBinaryFormat( normalFormString );
	assert.deepEqual( recoveredForm, string );
} );

QUnit.test( 'convertNormalForBinaryFormat/recoverNormalFromBinaryFormat: custom type', ( assert ) => {
	const custom = { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z400' }, Z400K1: { Z1K1: 'Z6', Z6K1: 'Inner string value' } };
	const expected = { 'ztypes.Z1': { valueMap: {
		Z1K1: { 'ztypes.Z9': { Z9K1: 'Z400' } },
		Z400K1: { 'ztypes.Z6': { Z6K1: 'Inner string value' } }
	} } };
	const normalFormCustom = convertNormalForBinaryFormat( custom );
	assert.deepEqual( normalFormCustom, expected );

	const recoveredForm = recoverNormalFromBinaryFormat( normalFormCustom );
	assert.deepEqual( recoveredForm, custom );
} );

QUnit.test( 'Binary format 0.0.3', ( assert ) => {
	const bigFunctionCall = readJSON( path.join( 'test_data', 'function_call', 'decently_big.json' ) );
	const original = {
		reentrant: true,
		zobject: bigFunctionCall
	};
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '0.0.3' );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		reentrant: true,
		codingLanguage: 'python-3',
		codeString: 'def Z1802(Z1802K1, Z1802K2):\n    return {Z1802K2: str(Z1802K1[Z1802K2])}',
		functionName: 'Z1802',
		functionArguments: {
			Z1802K1: {
				Z1K1: 'Z6',
				Z6K1: 'true?'
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 0.0.4', ( assert ) => {
	const bigFunctionCall = readJSON( path.join( 'test_data', 'function_call', 'decently_big.json' ) );
	const e = 2.718281828459;
	const original = {
		reentrant: true,
		zobject: bigFunctionCall,
		remainingTime: e
	};
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '0.0.4' );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		reentrant: true,
		remainingTime: e,
		codingLanguage: 'python-3',
		codeString: 'def Z1802(Z1802K1, Z1802K2):\n    return {Z1802K2: str(Z1802K1[Z1802K2])}',
		functionName: 'Z1802',
		functionArguments: {
			Z1802K1: {
				Z1K1: 'Z6',
				Z6K1: 'true?'
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 0.0.5', ( assert ) => {
	const bigFunctionCall = readJSON( path.join( 'test_data', 'function_call', 'decently_big.json' ) );
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: bigFunctionCall,
		remainingTime: e,
		requestId: requestId
	};
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '0.0.5' );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'python-3',
		codeString: 'def Z1802(Z1802K1, Z1802K2):\n    return {Z1802K2: str(Z1802K1[Z1802K2])}',
		functionName: 'Z1802',
		functionArguments: {
			Z1802K1: {
				Z1K1: 'Z6',
				Z6K1: 'true?'
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 0.1.0 without typeConverters', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'decently_big.json' ) );
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '0.1.0', true );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'python-3',
		serializer: null,
		function: {
			codeString: 'def Z1802(Z1802K1, Z1802K2):\n    return {Z1802K2: str(Z1802K1[Z1802K2])}',
			functionName: 'Z1802'
		},
		functionArguments: {
			Z1802K1: {
				deserializer: null,
				object: {
					Z1K1: 'Z6',
					Z6K1: 'true?'
				}
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 0.1.0 with ZReference for programming language', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'decently_big.json' ) );
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	functionCall.Z7K1.Z8K4.K1.Z14K3.Z16K1 = { Z1K1: 'Z9', Z9K1: 'Z610' };
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '0.1.0', true );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'Z610',
		serializer: null,
		function: {
			codeString: 'def Z1802(Z1802K1, Z1802K2):\n    return {Z1802K2: str(Z1802K1[Z1802K2])}',
			functionName: 'Z1802'
		},
		functionArguments: {
			Z1802K1: {
				deserializer: null,
				object: {
					Z1K1: 'Z6',
					Z6K1: 'true?'
				}
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 0.1.0 with typeConverters but enableTypeConverters is false', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_type_converters.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );
	functionCall.Z7K1.Z8K2 = Z10001Type;
	functionCall.Z400K1.Z1K1 = Z10001Type;
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '0.1.0', /* enableTypeConverters= */ false );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'javascript',
		function: {
			codeString: 'function Z400( Z400K1 ) { return Z400K1 + 1; }',
			functionName: 'Z400'
		},
		serializer: null,
		functionArguments: {
			Z400K1: {
				deserializer: null,
				object: {
					Z1K1: Z10001Type.Z4K1,
					Z10001K1: {
						Z1K1: 'Z6',
						Z6K1: '439'
					}
				}
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 0.1.0 with typeConverters', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_type_converters.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );
	functionCall.Z7K1.Z8K2 = Z10001Type;
	functionCall.Z400K1.Z1K1 = Z10001Type;
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '0.1.0', /* enableTypeConverters= */ true );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'javascript',
		function: {
			codeString: 'function Z400( Z400K1 ) { return Z400K1 + 1; }',
			functionName: 'Z400'
		},
		serializer: {
			codeString: (
				'function Z10003( Z10001 ) { ' +
                'return { ' +
                'Z1K1: { Z1K1: \'Z9\', Z9K1: \'Z10001\' }, ' +
                'Z10001K1: { Z1K1: \'Z6\', Z6K1: Z10001.toString() } ' +
                '}; }' ),
			functionName: 'Z10003'
		},
		functionArguments: {
			Z400K1: {
				deserializer: {
					codeString: 'function Z10002( Z10001 ) { return parseInt( Z10001.Z10001K1.Z6K1 ); }',
					functionName: 'Z10002'
				},
				object: {
					Z1K1: Z10001Type.Z4K1,
					Z10001K1: {
						Z1K1: 'Z6',
						Z6K1: '439'
					}
				}
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 0.1.0 with non-trivial type converter IDs', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_type_converters.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );
	const oldFromTypeConverter = Z10001Type.Z4K7;
	const oldToTypeConverter = Z10001Type.Z4K8;

	// Now the Identity fields are full Z46/Z64 instead of references.
	oldFromTypeConverter.Z46K1 = { ...oldFromTypeConverter };
	oldToTypeConverter.Z64K1 = { ...oldToTypeConverter };
	functionCall.Z7K1.Z8K2 = Z10001Type;
	functionCall.Z400K1.Z1K1 = Z10001Type;
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '0.1.0', /* enableTypeConverters= */ true );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'javascript',
		function: {
			codeString: 'function Z400( Z400K1 ) { return Z400K1 + 1; }',
			functionName: 'Z400'
		},
		serializer: {
			codeString: (
				'function Z10003( Z10001 ) { ' +
                'return { ' +
                'Z1K1: { Z1K1: \'Z9\', Z9K1: \'Z10001\' }, ' +
                'Z10001K1: { Z1K1: \'Z6\', Z6K1: Z10001.toString() } ' +
                '}; }' ),
			functionName: 'Z10003'
		},
		functionArguments: {
			Z400K1: {
				deserializer: {
					codeString: 'function Z10002( Z10001 ) { return parseInt( Z10001.Z10001K1.Z6K1 ); }',
					functionName: 'Z10002'
				},
				object: {
					Z1K1: Z10001Type.Z4K1,
					Z10001K1: {
						Z1K1: 'Z6',
						Z6K1: '439'
					}
				}
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 0.1.0 with no programming language match', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_type_converters.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );

	// Now no type converter matches the desired programming language.
	Z10001Type.Z4K7.K1.Z46K3.Z16K1.Z61K1.Z6K1 = 'python-3';
	Z10001Type.Z4K8.K1.Z64K3.Z16K1.Z61K1.Z6K1 = 'python-3';
	functionCall.Z7K1.Z8K2 = Z10001Type;
	functionCall.Z400K1.Z1K1 = Z10001Type;
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '0.1.0', /* enableTypeConverters= */ true );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'javascript',
		serializer: null,
		function: {
			codeString: 'function Z400( Z400K1 ) { return Z400K1 + 1; }',
			functionName: 'Z400'
		},
		functionArguments: {
			Z400K1: {
				object: {
					Z1K1: Z10001Type.Z4K1,
					Z10001K1: {
						Z1K1: 'Z6',
						Z6K1: '439'
					}
				},
				deserializer: null
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 0.1.0 with bad Function Identity', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_type_converters.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );
	functionCall.Z7K1.Z8K2 = Z10001Type;
	functionCall.Z400K1.Z1K1 = Z10001Type;
	functionCall.Z7K1.Z8K5 = { Z1K1: 'Z6', Z6K1: 'Z400' };
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	assert.raises( () => {
		convertWrappedZObjectToVersionedBinary( original, '0.1.0' );
	} );
} );

QUnit.test( 'Binary format 0.1.1 without typeConverters', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'decently_big.json' ) );
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '0.1.1', true );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		schemaVersion: '0.1.1',
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'python-3',
		serializer: null,
		function: {
			codeString: 'def Z1802(Z1802K1, Z1802K2):\n    return {Z1802K2: str(Z1802K1[Z1802K2])}',
			functionName: 'Z1802'
		},
		functionArguments: {
			Z1802K1: {
				deserializer: null,
				object: {
					Z1K1: 'Z6',
					Z6K1: 'true?'
				}
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 0.1.1 with ZReference for programming language', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'decently_big.json' ) );
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	functionCall.Z7K1.Z8K4.K1.Z14K3.Z16K1 = { Z1K1: 'Z9', Z9K1: 'Z610' };
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '0.1.1', true );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		schemaVersion: '0.1.1',
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'Z610',
		serializer: null,
		function: {
			codeString: 'def Z1802(Z1802K1, Z1802K2):\n    return {Z1802K2: str(Z1802K1[Z1802K2])}',
			functionName: 'Z1802'
		},
		functionArguments: {
			Z1802K1: {
				deserializer: null,
				object: {
					Z1K1: 'Z6',
					Z6K1: 'true?'
				}
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 0.1.1 with typeConverters but enableTypeConverters is false', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_type_converters.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );
	functionCall.Z7K1.Z8K2 = Z10001Type;
	functionCall.Z400K1.Z1K1 = Z10001Type;
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '0.1.1', /* enableTypeConverters= */ false );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		schemaVersion: '0.1.1',
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'javascript',
		function: {
			codeString: 'function Z400( Z400K1 ) { return Z400K1 + 1; }',
			functionName: 'Z400'
		},
		serializer: null,
		functionArguments: {
			Z400K1: {
				deserializer: null,
				object: {
					Z1K1: Z10001Type.Z4K1,
					Z10001K1: {
						Z1K1: 'Z6',
						Z6K1: '439'
					}
				}
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 0.1.1 with typeConverters', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_type_converters.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );
	functionCall.Z7K1.Z8K2 = Z10001Type;
	functionCall.Z400K1.Z1K1 = Z10001Type;
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '0.1.1', /* enableTypeConverters= */ true );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		schemaVersion: '0.1.1',
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'javascript',
		function: {
			codeString: 'function Z400( Z400K1 ) { return Z400K1 + 1; }',
			functionName: 'Z400'
		},
		serializer: {
			code: {
				codeString: (
					'function Z10003( Z10001 ) { ' +
                    'return { ' +
                    'Z1K1: { Z1K1: \'Z9\', Z9K1: \'Z10001\' }, ' +
                    'Z10001K1: { Z1K1: \'Z6\', Z6K1: Z10001.toString() } ' +
                    '}; }' ),
				functionName: 'Z10003'
			},
			isList: false
		},
		functionArguments: {
			Z400K1: {
				deserializer: {
					code: {
						codeString: 'function Z10002( Z10001 ) { return parseInt( Z10001.Z10001K1.Z6K1 ); }',
						functionName: 'Z10002'
					},
					isList: false
				},
				object: {
					Z1K1: Z10001Type.Z4K1,
					Z10001K1: {
						Z1K1: 'Z6',
						Z6K1: '439'
					}
				}
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 0.1.1 with non-trivial type converter IDs', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_type_converters.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );
	const oldFromTypeConverter = Z10001Type.Z4K7;
	const oldToTypeConverter = Z10001Type.Z4K8;

	// Now the Identity fields are full Z46/Z64 instead of references.
	oldFromTypeConverter.Z46K1 = { ...oldFromTypeConverter };
	oldToTypeConverter.Z64K1 = { ...oldToTypeConverter };
	functionCall.Z7K1.Z8K2 = Z10001Type;
	functionCall.Z400K1.Z1K1 = Z10001Type;
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '0.1.1', /* enableTypeConverters= */ true );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		schemaVersion: '0.1.1',
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'javascript',
		function: {
			codeString: 'function Z400( Z400K1 ) { return Z400K1 + 1; }',
			functionName: 'Z400'
		},
		serializer: {
			code: {
				codeString: (
					'function Z10003( Z10001 ) { ' +
                    'return { ' +
                    'Z1K1: { Z1K1: \'Z9\', Z9K1: \'Z10001\' }, ' +
                    'Z10001K1: { Z1K1: \'Z6\', Z6K1: Z10001.toString() } ' +
                    '}; }' ),
				functionName: 'Z10003'
			},
			isList: false
		},
		functionArguments: {
			Z400K1: {
				deserializer: {
					code: {
						codeString: 'function Z10002( Z10001 ) { return parseInt( Z10001.Z10001K1.Z6K1 ); }',
						functionName: 'Z10002'
					},
					isList: false
				},
				object: {
					Z1K1: Z10001Type.Z4K1,
					Z10001K1: {
						Z1K1: 'Z6',
						Z6K1: '439'
					}
				}
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 0.1.1 with lists for input and output types', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_list_type_converters.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );
	const oldFromTypeConverter = Z10001Type.Z4K7;
	const oldToTypeConverter = Z10001Type.Z4K8;
	const listOfZ10001Type = readJSON( path.join( 'test_data', 'function_call', 'list-of-Z10001-type.json' ) );

	// Now the Identity fields are full Z46/Z64 instead of references.
	oldFromTypeConverter.Z46K1 = { ...oldFromTypeConverter };
	oldToTypeConverter.Z64K1 = { ...oldToTypeConverter };
	listOfZ10001Type.Z4K2.K1.Z3K1 = Z10001Type;
	functionCall.Z7K1.Z8K2 = listOfZ10001Type;
	functionCall.Z400K1.Z1K1 = listOfZ10001Type;
	functionCall.Z400K1.K1.Z1K1 = Z10001Type.Z4K1;
	functionCall.Z400K1.K2.Z1K1 = listOfZ10001Type;
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '0.1.1', /* enableTypeConverters= */ true );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		schemaVersion: '0.1.1',
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'javascript',
		function: {
			codeString: 'function Z400( Z400K1 ) { return Z400K1 + 1; }',
			functionName: 'Z400'
		},
		serializer: {
			code: {
				codeString: (
					'function Z10003( Z10001 ) { ' +
                    'return { ' +
                    'Z1K1: { Z1K1: \'Z9\', Z9K1: \'Z10001\' }, ' +
                    'Z10001K1: { Z1K1: \'Z6\', Z6K1: Z10001.toString() } ' +
                    '}; }' ),
				functionName: 'Z10003'
			},
			isList: true
		},
		functionArguments: {
			Z400K1: {
				deserializer: {
					code: {
						codeString: 'function Z10002( Z10001 ) { return parseInt( Z10001.Z10001K1.Z6K1 ); }',
						functionName: 'Z10002'
					},
					isList: true
				},
				object: {
					Z1K1: listOfZ10001Type.Z4K1,
					K1: {
						Z1K1: Z10001Type.Z4K1,
						Z10001K1: {
							Z1K1: 'Z6',
							Z6K1: '439'
						}
					},
					K2: {
						Z1K1: listOfZ10001Type.Z4K1
					}
				}
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 0.1.1 with no programming language match', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_type_converters.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );

	// Now no type converter matches the desired programming language.
	Z10001Type.Z4K7.K1.Z46K3.Z16K1.Z61K1.Z6K1 = 'python-3';
	Z10001Type.Z4K8.K1.Z64K3.Z16K1.Z61K1.Z6K1 = 'python-3';
	functionCall.Z7K1.Z8K2 = Z10001Type;
	functionCall.Z400K1.Z1K1 = Z10001Type;
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '0.1.1', /* enableTypeConverters= */ true );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		schemaVersion: '0.1.1',
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'javascript',
		serializer: null,
		function: {
			codeString: 'function Z400( Z400K1 ) { return Z400K1 + 1; }',
			functionName: 'Z400'
		},
		functionArguments: {
			Z400K1: {
				object: {
					Z1K1: Z10001Type.Z4K1,
					Z10001K1: {
						Z1K1: 'Z6',
						Z6K1: '439'
					}
				},
				deserializer: null
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 0.1.1 with bad Function Identity', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_type_converters.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );
	functionCall.Z7K1.Z8K2 = Z10001Type;
	functionCall.Z400K1.Z1K1 = Z10001Type;
	functionCall.Z7K1.Z8K5 = { Z1K1: 'Z6', Z6K1: 'Z400' };
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	assert.raises( () => {
		convertWrappedZObjectToVersionedBinary( original, '0.1.1' );
	} );
} );

QUnit.test( 'convert formatted request back to binary v0.1.0', ( assert ) => {
	const formattedRequest = readJSON( path.join( 'test_data', 'formatted_request', 'python3_add_with_type_converters.json' ) );
	const binaryForm = convertFormattedRequestToVersionedBinary( formattedRequest, '0.1.0' );
	const actual = getWrappedZObjectFromVersionedBinary( binaryForm );
	assert.deepEqual( actual, formattedRequest );
} );

QUnit.test( 'convert formatted request back to binary v0.1.0, no typeConverters', ( assert ) => {
	const formattedRequest = readJSON( path.join( 'test_data', 'formatted_request', 'python3_add_with_type_converters.json' ) );
	delete formattedRequest.serializer;
	for ( const key of Object.keys( formattedRequest.functionArguments ) ) {
		delete formattedRequest.functionArguments[ key ].deserializer;
	}
	const binaryForm = convertFormattedRequestToVersionedBinary( formattedRequest, '0.1.0' );
	const actual = getWrappedZObjectFromVersionedBinary( binaryForm );

	// Set type converters to "null."
	const expected = { ...formattedRequest };
	expected.serializer = null;
	for ( const key of Object.keys( expected.functionArguments ) ) {
		expected.functionArguments[ key ].deserializer = null;
	}
	assert.deepEqual( actual, expected );
} );

QUnit.test( 'convert formatted request back to binary v0.1.0, null typeConverters', ( assert ) => {
	const formattedRequest = readJSON( path.join( 'test_data', 'formatted_request', 'python3_add_with_type_converters.json' ) );
	formattedRequest.serializer = null;
	for ( const key of Object.keys( formattedRequest.functionArguments ) ) {
		formattedRequest.functionArguments[ key ].deserializer = null;
	}
	const binaryForm = convertFormattedRequestToVersionedBinary( formattedRequest, '0.1.0' );
	const actual = getWrappedZObjectFromVersionedBinary( binaryForm );
	assert.deepEqual( actual, formattedRequest );
} );

QUnit.test( 'convert formatted request back to binary v0.1.1', ( assert ) => {
	const formattedRequest = readJSON( path.join( 'test_data', 'formatted_request', 'python3_add_with_type_converters_011.json' ) );
	const binaryForm = convertFormattedRequestToVersionedBinary( formattedRequest, '0.1.1' );
	const actual = getWrappedZObjectFromVersionedBinary( binaryForm );
	assert.deepEqual( actual, formattedRequest );
} );

QUnit.test( 'convert formatted request back to binary v0.1.1, no typeConverters', ( assert ) => {
	const formattedRequest = readJSON( path.join( 'test_data', 'formatted_request', 'python3_add_with_type_converters_011.json' ) );
	delete formattedRequest.serializer;
	for ( const key of Object.keys( formattedRequest.functionArguments ) ) {
		delete formattedRequest.functionArguments[ key ].deserializer;
	}
	const binaryForm = convertFormattedRequestToVersionedBinary( formattedRequest, '0.1.1' );
	const actual = getWrappedZObjectFromVersionedBinary( binaryForm );

	// Set type converters to "null."
	const expected = { ...formattedRequest };
	expected.serializer = null;
	for ( const key of Object.keys( expected.functionArguments ) ) {
		expected.functionArguments[ key ].deserializer = null;
	}
	assert.deepEqual( actual, expected );
} );

QUnit.test( 'convert formatted request back to binary v0.1.1, null typeConverters', ( assert ) => {
	const formattedRequest = readJSON( path.join( 'test_data', 'formatted_request', 'python3_add_with_type_converters_011.json' ) );
	formattedRequest.serializer = null;
	for ( const key of Object.keys( formattedRequest.functionArguments ) ) {
		formattedRequest.functionArguments[ key ].deserializer = null;
	}
	const binaryForm = convertFormattedRequestToVersionedBinary( formattedRequest, '0.1.1' );
	const actual = getWrappedZObjectFromVersionedBinary( binaryForm );
	assert.deepEqual( actual, formattedRequest );
} );

QUnit.test( 'convert formatted request back to binary throws Error for semver < 0.1.0', ( assert ) => {
	const formattedRequest = readJSON( path.join( 'test_data', 'formatted_request', 'python3_add_with_type_converters.json' ) );
	assert.raises( () => {
		convertWrappedZObjectToVersionedBinary( formattedRequest, '0.0.5' );
	} );
} );

QUnit.test( 'Binary format 1.0.0 without typeConverters', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'decently_big.json' ) );
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '1.0.0', true );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		schemaVersion: '1.0.0',
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'python-3',
		serializer: null,
		function: {
			codeString: 'def Z1802(Z1802K1, Z1802K2):\n    return {Z1802K2: str(Z1802K1[Z1802K2])}',
			functionName: 'Z1802'
		},
		functionArguments: {
			Z1802K1: {
				deserializer: null,
				object: {
					Z1K1: 'Z6',
					Z6K1: 'true?'
				}
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 1.0.0 with ZReference for programming language', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'decently_big.json' ) );
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	functionCall.Z7K1.Z8K4.K1.Z14K3.Z16K1 = { Z1K1: 'Z9', Z9K1: 'Z610' };
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '1.0.0', true );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		schemaVersion: '1.0.0',
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'Z610',
		serializer: null,
		function: {
			codeString: 'def Z1802(Z1802K1, Z1802K2):\n    return {Z1802K2: str(Z1802K1[Z1802K2])}',
			functionName: 'Z1802'
		},
		functionArguments: {
			Z1802K1: {
				deserializer: null,
				object: {
					Z1K1: 'Z6',
					Z6K1: 'true?'
				}
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 1.0.0 with typeConverters but enableTypeConverters is false', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_type_converters.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );
	functionCall.Z7K1.Z8K2 = Z10001Type;
	functionCall.Z400K1.Z1K1 = Z10001Type;
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '1.0.0', /* enableTypeConverters= */ false );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		schemaVersion: '1.0.0',
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'javascript',
		function: {
			codeString: 'function Z400( Z400K1 ) { return Z400K1 + 1; }',
			functionName: 'Z400'
		},
		serializer: null,
		functionArguments: {
			Z400K1: {
				deserializer: null,
				object: {
					Z1K1: Z10001Type.Z4K1,
					Z10001K1: {
						Z1K1: 'Z6',
						Z6K1: '439'
					}
				}
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 1.0.0 with typeConverters', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_type_converters.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );
	functionCall.Z7K1.Z8K2 = Z10001Type;
	functionCall.Z400K1.Z1K1 = Z10001Type;
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '1.0.0', /* enableTypeConverters= */ true );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		schemaVersion: '1.0.0',
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'javascript',
		function: {
			codeString: 'function Z400( Z400K1 ) { return Z400K1 + 1; }',
			functionName: 'Z400'
		},
		serializer: {
			code: {
				codeString: (
					'function Z10003( Z10001 ) { ' +
                    'return { ' +
                    'Z1K1: { Z1K1: \'Z9\', Z9K1: \'Z10001\' }, ' +
                    'Z10001K1: { Z1K1: \'Z6\', Z6K1: Z10001.toString() } ' +
                    '}; }' ),
				functionName: 'Z10003'
			},
			isList: false
		},
		functionArguments: {
			Z400K1: {
				deserializer: {
					code: {
						codeString: 'function Z10002( Z10001 ) { return parseInt( Z10001.Z10001K1.Z6K1 ); }',
						functionName: 'Z10002'
					},
					isList: false
				},
				object: {
					Z1K1: Z10001Type.Z4K1,
					Z10001K1: {
						Z1K1: 'Z6',
						Z6K1: '439'
					}
				}
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 1.0.0 with non-trivial type converter IDs', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_type_converters.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );
	const oldFromTypeConverter = Z10001Type.Z4K7;
	const oldToTypeConverter = Z10001Type.Z4K8;

	// Now the Identity fields are full Z46/Z64 instead of references.
	oldFromTypeConverter.Z46K1 = { ...oldFromTypeConverter };
	oldToTypeConverter.Z64K1 = { ...oldToTypeConverter };
	functionCall.Z7K1.Z8K2 = Z10001Type;
	functionCall.Z400K1.Z1K1 = Z10001Type;
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '1.0.0', /* enableTypeConverters= */ true );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		schemaVersion: '1.0.0',
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'javascript',
		function: {
			codeString: 'function Z400( Z400K1 ) { return Z400K1 + 1; }',
			functionName: 'Z400'
		},
		serializer: {
			code: {
				codeString: (
					'function Z10003( Z10001 ) { ' +
                    'return { ' +
                    'Z1K1: { Z1K1: \'Z9\', Z9K1: \'Z10001\' }, ' +
                    'Z10001K1: { Z1K1: \'Z6\', Z6K1: Z10001.toString() } ' +
                    '}; }' ),
				functionName: 'Z10003'
			},
			isList: false
		},
		functionArguments: {
			Z400K1: {
				deserializer: {
					code: {
						codeString: 'function Z10002( Z10001 ) { return parseInt( Z10001.Z10001K1.Z6K1 ); }',
						functionName: 'Z10002'
					},
					isList: false
				},
				object: {
					Z1K1: Z10001Type.Z4K1,
					Z10001K1: {
						Z1K1: 'Z6',
						Z6K1: '439'
					}
				}
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 1.0.0 with lists for input and output types', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_list_type_converters.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );
	const oldFromTypeConverter = Z10001Type.Z4K7;
	const oldToTypeConverter = Z10001Type.Z4K8;
	const listOfZ10001Type = readJSON( path.join( 'test_data', 'function_call', 'list-of-Z10001-type.json' ) );

	// Now the Identity fields are full Z46/Z64 instead of references.
	oldFromTypeConverter.Z46K1 = { ...oldFromTypeConverter };
	oldToTypeConverter.Z64K1 = { ...oldToTypeConverter };
	listOfZ10001Type.Z4K2.K1.Z3K1 = Z10001Type;
	functionCall.Z7K1.Z8K2 = listOfZ10001Type;
	functionCall.Z400K1.Z1K1 = listOfZ10001Type;
	functionCall.Z400K1.K1.Z1K1 = Z10001Type.Z4K1;
	functionCall.Z400K1.K2.Z1K1 = listOfZ10001Type;
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '1.0.0', /* enableTypeConverters= */ true );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		schemaVersion: '1.0.0',
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'javascript',
		function: {
			codeString: 'function Z400( Z400K1 ) { return Z400K1 + 1; }',
			functionName: 'Z400'
		},
		serializer: {
			code: {
				codeString: (
					'function Z10003( Z10001 ) { ' +
                    'return { ' +
                    'Z1K1: { Z1K1: \'Z9\', Z9K1: \'Z10001\' }, ' +
                    'Z10001K1: { Z1K1: \'Z6\', Z6K1: Z10001.toString() } ' +
                    '}; }' ),
				functionName: 'Z10003'
			},
			isList: true
		},
		functionArguments: {
			Z400K1: {
				deserializer: {
					code: {
						codeString: 'function Z10002( Z10001 ) { return parseInt( Z10001.Z10001K1.Z6K1 ); }',
						functionName: 'Z10002'
					},
					isList: true
				},
				object: {
					Z1K1: listOfZ10001Type.Z4K1,
					K1: {
						Z1K1: Z10001Type.Z4K1,
						Z10001K1: {
							Z1K1: 'Z6',
							Z6K1: '439'
						}
					},
					K2: {
						Z1K1: listOfZ10001Type.Z4K1
					}
				}
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 1.0.0 with no programming language match', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_type_converters.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );

	// Now no type converter matches the desired programming language.
	Z10001Type.Z4K7.K1.Z46K3.Z16K1.Z61K1.Z6K1 = 'python-3';
	Z10001Type.Z4K8.K1.Z64K3.Z16K1.Z61K1.Z6K1 = 'python-3';
	functionCall.Z7K1.Z8K2 = Z10001Type;
	functionCall.Z400K1.Z1K1 = Z10001Type;
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '1.0.0', /* enableTypeConverters= */ true );
	const textForm = getWrappedZObjectFromVersionedBinary( binaryForm );
	const expected = {
		schemaVersion: '1.0.0',
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'javascript',
		serializer: null,
		function: {
			codeString: 'function Z400( Z400K1 ) { return Z400K1 + 1; }',
			functionName: 'Z400'
		},
		functionArguments: {
			Z400K1: {
				object: {
					Z1K1: Z10001Type.Z4K1,
					Z10001K1: {
						Z1K1: 'Z6',
						Z6K1: '439'
					}
				},
				deserializer: null
			}
		}
	};

	assert.deepEqual( textForm, expected );
} );

QUnit.test( 'Binary format 1.0.0 with bad Function Identity', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_type_converters.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );
	functionCall.Z7K1.Z8K2 = Z10001Type;
	functionCall.Z400K1.Z1K1 = Z10001Type;
	functionCall.Z7K1.Z8K5 = { Z1K1: 'Z6', Z6K1: 'Z400' };
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	assert.raises( () => {
		convertWrappedZObjectToVersionedBinary( original, '1.0.0' );
	} );
} );

QUnit.test( 'convert formatted request back to binary v1.0.0', ( assert ) => {
	const formattedRequest = readJSON( path.join( 'test_data', 'formatted_request', 'python3_add_with_type_converters_011.json' ) );
	const binaryForm = convertFormattedRequestToVersionedBinary( formattedRequest, '1.0.0' );
	const actual = getWrappedZObjectFromVersionedBinary( binaryForm );
	assert.deepEqual( actual, formattedRequest );
} );

QUnit.test( 'convert formatted request back to binary v1.0.0, no typeConverters', ( assert ) => {
	const formattedRequest = readJSON( path.join( 'test_data', 'formatted_request', 'python3_add_with_type_converters_011.json' ) );
	delete formattedRequest.serializer;
	for ( const key of Object.keys( formattedRequest.functionArguments ) ) {
		delete formattedRequest.functionArguments[ key ].deserializer;
	}
	const binaryForm = convertFormattedRequestToVersionedBinary( formattedRequest, '1.0.0' );
	const actual = getWrappedZObjectFromVersionedBinary( binaryForm );

	// Set type converters to "null."
	const expected = { ...formattedRequest };
	expected.serializer = null;
	for ( const key of Object.keys( expected.functionArguments ) ) {
		expected.functionArguments[ key ].deserializer = null;
	}
	assert.deepEqual( actual, expected );
} );

QUnit.test( 'convert formatted request back to binary v1.0.0, null typeConverters', ( assert ) => {
	const formattedRequest = readJSON( path.join( 'test_data', 'formatted_request', 'python3_add_with_type_converters_011.json' ) );
	formattedRequest.serializer = null;
	for ( const key of Object.keys( formattedRequest.functionArguments ) ) {
		formattedRequest.functionArguments[ key ].deserializer = null;
	}
	const binaryForm = convertFormattedRequestToVersionedBinary( formattedRequest, '1.0.0' );
	const actual = getWrappedZObjectFromVersionedBinary( binaryForm );
	assert.deepEqual( actual, formattedRequest );
} );

QUnit.test( 'v1.0.0 schema produces the expected Rust-compatible binary format', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_list_type_converters.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );
	const oldFromTypeConverter = Z10001Type.Z4K7;
	const oldToTypeConverter = Z10001Type.Z4K8;
	const listOfZ10001Type = readJSON( path.join( 'test_data', 'function_call', 'list-of-Z10001-type.json' ) );

	// Now the Identity fields are full Z46/Z64 instead of references.
	oldFromTypeConverter.Z46K1 = { ...oldFromTypeConverter };
	oldToTypeConverter.Z64K1 = { ...oldToTypeConverter };
	listOfZ10001Type.Z4K2.K1.Z3K1 = Z10001Type;
	functionCall.Z7K1.Z8K2 = listOfZ10001Type;
	functionCall.Z400K1.Z1K1 = listOfZ10001Type;
	functionCall.Z400K1.K1.Z1K1 = Z10001Type.Z4K1;
	functionCall.Z400K1.K2.Z1K1 = listOfZ10001Type;
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const binaryForm = convertWrappedZObjectToVersionedBinary( original, '1.0.0', /* enableTypeConverters= */ true );
	const versionlessBinaryForm = binaryForm.subarray( 6 );
	const actualAsHex = versionlessBinaryForm.toString( 'hex' );
	const expectedAsHex = fs.readFileSync(
		path.join( 'test_data', 'binary_format', 'hex.txt' )
	).toString().trim();
	assert.deepEqual( actualAsHex, expectedAsHex );
} );
