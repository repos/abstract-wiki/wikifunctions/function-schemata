'use strict';

const { compareTypes } = require( '../../../src/compareTypes.js' );
const normalize = require( '../../../src/normalize.js' );

QUnit.module( 'compareTypes.js' );

function makeZReference( ZID ) {
	return ZID;
}

function makeZKey( typeZ4, nameZ6 ) {
	const labelZ12 = {
		Z1K1: makeZReference( 'Z12' ),
		Z12K1: [ 'Z11' ]
	};
	return {
		Z1K1: makeZReference( 'Z3' ),
		Z3K1: typeZ4,
		Z3K2: nameZ6,
		Z3K3: labelZ12
	};
}

function makeZType( identity, arrayOfTypes = null ) {
	const Z4K2 = [ 'Z3' ];
	if ( arrayOfTypes !== null ) {
		for ( let i = 0; i < arrayOfTypes.length; ++i ) {
			const keyType = arrayOfTypes[ i ];
			Z4K2.push( makeZKey( keyType, 'Z10101K' + ( i + 1 ) ) );
		}
	}
	return {
		Z1K1: makeZReference( 'Z4' ),
		Z4K1: identity,
		Z4K2: Z4K2,
		Z4K3: makeZReference( 'Z831' )
	};
}

function boldlyNormalize( ZObject ) {
	return normalize( ZObject ).Z22K1;
}

QUnit.test( 'compareTypes is true when either type is a function call', ( assert ) => {
	const someGenericType = boldlyNormalize( {
		Z1K1: makeZReference( 'Z7' ),
		Z7K1: makeZReference( 'Z881' ),
		Z881K1: makeZReference( 'Z1' )
	} );
	const someScrub = boldlyNormalize( makeZType( makeZReference( 'Z4' ) ) );

	// Comparand is a function call.
	assert.true( compareTypes( someGenericType, someScrub ) );

	// Comparator is a function call.
	assert.true( compareTypes( someScrub, someGenericType ) );
} );

QUnit.test( 'compareTypes is true when comparator identity is Z1', ( assert ) => {
	// Try with a bare reference.
	const Z1Type = boldlyNormalize( makeZReference( 'Z1' ) );
	const someScrub = boldlyNormalize( makeZType( makeZReference( 'Z4' ) ) );
	assert.true( compareTypes( someScrub, Z1Type ) );

	// Try with a Z4 whose identity is a bare reference.
	const wrappedZ1 = boldlyNormalize( makeZType( Z1Type ) );
	assert.true( compareTypes( someScrub, wrappedZ1 ) );

	// Just get fun with it.
	const doubleWrappedZ1 = boldlyNormalize( makeZType( wrappedZ1 ) );
	assert.true( compareTypes( someScrub, doubleWrappedZ1 ) );
} );

QUnit.test( 'compareTypes is true when identities are equal', ( assert ) => {
	const canonicalTypeA = makeZReference( 'Z6' );
	const typeA = boldlyNormalize( canonicalTypeA );
	const wrappedA = boldlyNormalize( makeZType( typeA ) );

	// Try with bare references.
	assert.true( compareTypes( typeA, typeA ) );

	// Try wrapping comparand.
	assert.true( compareTypes( wrappedA, typeA ) );

	// Try wrapping comparator.
	assert.true( compareTypes( typeA, wrappedA ) );

	// Try wrapping both.
	assert.true( compareTypes( wrappedA, wrappedA ) );
} );

QUnit.test( 'compareTypes is false when identities are not equal', ( assert ) => {
	const canonicalTypeA = makeZReference( 'Z6' );
	const canonicalTypeB = makeZReference( 'Z4' );
	const typeA = boldlyNormalize( canonicalTypeA );
	const typeB = boldlyNormalize( canonicalTypeB );

	// Try with bare references.
	assert.false( compareTypes( typeA, typeB ) );

	// Try wrapping type A.
	const wrappedA = boldlyNormalize( makeZType( typeA ) );
	assert.false( compareTypes( wrappedA, typeB ) );

	// Try wrapping type B.
	const wrappedB = boldlyNormalize( makeZType( typeB ) );
	assert.false( compareTypes( typeA, wrappedB ) );

	// Try wrapping both.
	assert.false( compareTypes( wrappedA, wrappedB ) );
} );

QUnit.test( 'compareTypes is false when comparandIdentity input identity is not valid', ( assert ) => {
	const canonicalTypeA = makeZReference( 'Hello' );
	const canonicalTypeB = makeZReference( 'Z2' );
	const typeA = boldlyNormalize( canonicalTypeA );
	const typeB = boldlyNormalize( canonicalTypeB );

	assert.false( compareTypes( typeA, typeB ) );
} );

QUnit.test( 'compareTypes is true when the keys of user-defined types type-compare', ( assert ) => {
	let typeA, typeB;
	const identityA = makeZReference( 'Z88888' );
	const identityB = makeZReference( 'Z99999' );

	// What if no keys?
	typeA = boldlyNormalize( makeZType( identityA ) );
	typeB = boldlyNormalize( makeZType( identityB ) );
	assert.true( compareTypes( typeA, typeB ) );

	// What if, actually, there are some keys?
	const typeKeys = [ makeZReference( 'Z6' ), makeZType( makeZReference( 'Z55555' ) ) ];
	typeA = boldlyNormalize( makeZType( identityA, typeKeys ) );
	typeB = boldlyNormalize( makeZType( identityB, typeKeys ) );
	assert.true( compareTypes( typeA, typeB ) );

	// What if we nest it a little bit?
	const keysA = typeKeys.concat( [ boldlyNormalize( makeZType( identityA, typeKeys ) ) ] );
	const keysB = typeKeys.concat( [ boldlyNormalize( makeZType( identityB, typeKeys ) ) ] );
	typeA = boldlyNormalize( makeZType( identityA, keysA ) );
	typeB = boldlyNormalize( makeZType( identityB, keysB ) );
	assert.true( compareTypes( typeA, typeB ) );
} );

QUnit.test( 'compareTypes is false when the keys of user-defined types do not type-compare', ( assert ) => {
	let typeA, typeB;
	const identityA = makeZReference( 'Z88888' );
	const identityB = makeZReference( 'Z99999' );

	// Try with one discrepancy.
	const shallowKeysA = [ makeZReference( 'Z6' ), makeZType( makeZReference( 'Z55555' ) ) ];
	const shallowKeysB = [ makeZReference( 'Z4' ), makeZType( makeZReference( 'Z55555' ) ) ];
	typeA = boldlyNormalize( makeZType( identityA, shallowKeysA ) );
	typeB = boldlyNormalize( makeZType( identityB, shallowKeysB ) );
	assert.false( compareTypes( typeA, typeB ) );

	// Maybe with different numbers of keys?
	const noKeysA = [];
	const oneKeyB = [ makeZReference( 'Z4' ) ];
	typeA = boldlyNormalize( makeZType( identityA, noKeysA ) );
	typeB = boldlyNormalize( makeZType( identityB, oneKeyB ) );
	assert.false( compareTypes( typeA, typeB ) );

	// What if we nest it a little bit?
	const deepKeysA = shallowKeysA.concat(
		[ boldlyNormalize( makeZType( identityA, shallowKeysA ) ) ]
	);
	const deepKeysB = shallowKeysB.concat(
		[ boldlyNormalize( makeZType( identityB, shallowKeysB ) ) ]
	);
	typeA = boldlyNormalize( makeZType( identityA, deepKeysA ) );
	typeB = boldlyNormalize( makeZType( identityB, deepKeysB ) );
	assert.false( compareTypes( typeA, typeB ) );
} );

QUnit.test( 'compareTypes is false when the input identities are not valid', ( assert ) => {
	// This is purely to capture the final 'return' in the code.
	const identity = makeZReference( 'Hello' );
	const shallowKeys = [ makeZReference( 'Z6' ), makeZType( makeZReference( 'Z55555' ) ) ];
	const typeA = boldlyNormalize( makeZType( identity, shallowKeys ) );
	const typeB = boldlyNormalize( makeZType( identity, shallowKeys ) );
	assert.false( compareTypes( typeA, typeB ) );
} );

QUnit.test( 'compareTypes is true when comparand and comparator are user-defined types to the same item with no keys', ( assert ) => {
	const identityA = makeZReference( 'Z88888' );
	const identityB = makeZReference( 'Z99991' );

	const typeA = boldlyNormalize( makeZType( identityA, false ) );
	const typeB = boldlyNormalize( makeZType( identityB, false ) );

	// Hacky way to drop the Z4K2 keys from the Types
	typeA.Z4K2 = false;
	typeB.Z4K2 = false;

	assert.true( compareTypes( typeA, typeB ) );
} );
